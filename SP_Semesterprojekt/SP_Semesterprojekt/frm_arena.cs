﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_arena : Form
    {
        List<cls_pokemon> spieler1liste;
        List<cls_pokemon> gegnerliste;

        bool goLeft, goRight, jumping, damage;
        int jumpSpeed, force, score = 0;
        int playerSpeed = 7;

        int duration = 300;

        public frm_arena()
        {
            InitializeComponent();
        }

        // Überladener Konstruktor
        public frm_arena(List<cls_pokemon> pokemonspieler1, List<cls_pokemon> gegner)
        {
            InitializeComponent();
            spieler1liste = pokemonspieler1;
            gegnerliste = gegner;
        }

        private void count_down(object sender, EventArgs e)
        {

            if (duration == 0)
            {
                timer1.Stop();
                gameTimer.Stop();

                MessageBox.Show("Unentschieden", "Kein Gewinner", MessageBoxButtons.OK, MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;
            }
            else if (duration > 0)
            {
                duration--;
                lbl_timer.Text = duration.ToString();
            }
        }

        private void frm_arena_Load(object sender, EventArgs e)
        {
            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(count_down);
            timer1.Interval = 1000;
            timer1.Start();

            gameTimer = new System.Windows.Forms.Timer();
            gameTimer.Tick += new EventHandler(MainGameTimerEvent);
            gameTimer.Interval = 20; // 20 milliseconds for smoother animation, adjust as needed
            gameTimer.Start();

            if (spieler1liste != null && spieler1liste.Count > 0)
            {
                lbl_spieler1.Text = spieler1liste[0].Name;
                lbl_lp1.Text = spieler1liste[0].Lp.ToString();
                lbl_wp1.Text = spieler1liste[0].Wp.ToString();
                lbl_spieler1typ1.Text = spieler1liste[0].Typ1;
                lbl_spieler1typ2.Text = spieler1liste[0].Typ2;

            }

            if (gegnerliste != null && gegnerliste.Count > 0)
            {
                lbl_gegner2.Text = gegnerliste[0].Name;
                lbl_lp2.Text = gegnerliste[0].Lp.ToString();
                lbl_wp2.Text = gegnerliste[0].Wp.ToString();
                lbl_gegnertyp1.Text = gegnerliste[0].Typ1;
                lbl_gegnertyp2.Text = gegnerliste[0].Typ2;
            }
        }

        

        private void Spieler1()
        {
            player1.Top += jumpSpeed;

            if (goLeft == true)
            {
                player1.Left -= playerSpeed;
            }
            if (goRight == true)
            {
                player1.Left += playerSpeed;
            }

            if (jumping == true && force < 0)
            {
                jumping = false;
            }

            if (jumping == true)
            {
                jumpSpeed = -8;
                force -= 1;
            }
            else
            {
                jumpSpeed = 10;
            }
        }
        private void Gegner()
        {
            //richtung von Spieler 2
            int moveDirection;
            if (player1.Left > player2.Left)
            {
                moveDirection = 1; // nach rechts
            }
            else
            {
                moveDirection = -1; //nach links
            }

            // Geschwindigkeit von Spieler 2
            player2.Left += moveDirection * (playerSpeed / 3);

            //ob Spieler 2 auf einer Plattform steht

            bool platform = false;

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "platform" && player2.Bounds.IntersectsWith(x.Bounds))
                {
                    platform = true;
                    break;
                }
            }

            //Wenn auf keiner Platform
            if (!platform)
            {
                player2.Top += jumpSpeed;

                //Wenn er runter gefalen ist
                if (player2.Top + player2.Height >= this.ClientSize.Height)
                {
  
                    gameTimer.Stop();
                    timer1.Stop();
                    MessageBox.Show("Spieler 1 hat gewonnen!", "Spielende", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    DialogResult = DialogResult.OK;
                    
                }
                else
                {
                    // Wenn noch nicht tot, Sprungeschwindigkeit
                    jumpSpeed += 2;
                }
            }
            else
            {
                //Auf einer Platform, Sprunggeschwindigkeit, null
                jumpSpeed = 0;
            }
        }




        private void MainGameTimerEvent(object sender, EventArgs e)
        {
            Spieler1();
            Gegner();

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox)
                {
                    if ((string)x.Tag == "platform")
                    {
                        if (player1.Bounds.IntersectsWith(x.Bounds))
                        {
                            force = 8;
                            player1.Top = x.Top - player1.Height;
                        }

                        if (player2.Bounds.IntersectsWith(x.Bounds))
                        {
                            force = 8;
                            player2.Top = x.Top - player2.Height;
                        }
                    }
                }
            }


            // Wenn Spieler 1 Spieler 2 berührt und die Taste W gedrückt wird
            if (player1.Bounds.IntersectsWith(player2.Bounds) && damage)
            {
                int schaden = Convert.ToInt32(lbl_wp1.Text) / 4;
                int lebenspunktegegner = Convert.ToInt32(lbl_lp2.Text);

                if (schaden >= lebenspunktegegner)
                {
                    gameTimer.Stop();

                    lbl_lp2.Text = "0";

                    MessageBox.Show("Spieler 1 gewinnt!", "Du hast gewonnen", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    DialogResult = DialogResult.OK;
                }

                else
                {
                    // Ziehe den Schaden von den aktuellen Lebenspunkten des Gegners ab
                    lbl_lp2.Text = (lebenspunktegegner - schaden).ToString();
                }

                // Setze damage auf false, um sicherzustellen, dass der Schaden nur einmal verursacht wird
                damage = false;
            }



            // Wenn Spieler 2 Spieler 1 berührt und Spieler 1 noch nicht verloren hat
            if (player2.Bounds.IntersectsWith(player1.Bounds))
            {
                Random rnd = new Random();
                bool causeDamage = rnd.Next(0, 2) == 1;

                if (causeDamage)
                {
                    int schaden = Convert.ToInt32(lbl_wp2.Text) / 8;
                    int Lebenspieler1 = Convert.ToInt32(lbl_lp1.Text); 

                    if (schaden >= Lebenspieler1)
                    {
                        gameTimer.Stop();
                        lbl_lp1.Text = "0";

                        MessageBox.Show("Spieler 1 hat verloren!", "Computer gewinnt", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        DialogResult= DialogResult.OK;

                    }
                    else
                    {
                        // Ziehe den Schaden von den aktuellen Lebenspunkten von Spieler 1 ab
                        lbl_lp1.Text = (Lebenspieler1 - schaden).ToString();
                    }
                }
            }

            //Wenn Spieler 1 runter fällt
            if (player1.Top >= this.ClientSize.Height - player1.Height)
            {
                lbl_lp1.Text = "0";
                gameTimer.Stop();
                MessageBox.Show("Spieler 1 ist heruntergefallen! Spieler 2 gewinnt!", "Spielende", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }

            //Wenn Spieler 2 runter fällt
            if (player2.Top >= this.ClientSize.Height - player2.Height)
            {
                lbl_lp2.Text = "0";
                gameTimer.Stop();
                MessageBox.Show("Spieler 2 ist heruntergefallen! Spieler 1 gewinnt!", "Spielende", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }



        }

        //Wenn eine Taste gedrückt wird
        private void frm_arena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = true;
            }
            if (e.KeyCode == Keys.Space && jumping == false)
            {
                jumping = true;
            }
            if(e.KeyCode == Keys.W)
            {
                damage = true;
            }
        }
       
        //Wenn eine Taste losgelassen wird
        private void frm_arena_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = false;
            }
            if (jumping == true)
            {
                jumping = false;
            }
            if(e.KeyCode == Keys.W)
            {
                damage = false;
            }
        }
    }
}
