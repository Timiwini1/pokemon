﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_hauptmenü : Form
    {
        public frm_hauptmenü()
        {
            InitializeComponent();
        }

        private void btn_quiz_Click(object sender, EventArgs e)
        {
            this.Close();

            frm_quiz q = new frm_quiz();
            q.ShowDialog();
        }

        private void btn_erfinden_Click(object sender, EventArgs e)
        {
            
             
        }

        private void btn_suchen_Click(object sender, EventArgs e)
        {
             
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frm_quiz q = new frm_quiz();
            q.ShowDialog();
        }

        private void pbx_neu_Click(object sender, EventArgs e)
        {

            frm_pokemonerstellen p = new frm_pokemonerstellen();
            p.ShowDialog();
        }

        private void pbx_suchen_Click(object sender, EventArgs e)
        {
            frm_suchen suchen = new frm_suchen();
            suchen.ShowDialog();
        }

        private void pbx_kampf_Click(object sender, EventArgs e)
        {
            frm_kampfauswahl k = new frm_kampfauswahl();
            k.ShowDialog();
        }
    }
}
