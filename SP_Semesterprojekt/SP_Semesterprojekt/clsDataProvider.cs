﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SP_Semesterprojekt
{
    internal class clsDataProvider
    {
        const string connectionString = "datasource=db4free.net;port=3306;username=timrad07;password=Konnmo007;database=semester_pokemon;";
        static public void InsertPokemon(cls_pokemon pokemon)
        {
            MySqlConnection conn = new MySqlConnection(connectionString);

            string query = "INSERT INTO tbl_alle (pokedex, name, typ1, typ2, attack, lebenspunkte, generation_name) VALUES (@pokedex, @name, @typ1, @typ2, @wp, @lp, @generation)";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            cmd.Parameters.Add("@pokedex", MySqlDbType.Int32).Value = pokemon.Pokedex;
            cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = pokemon.Name;
            cmd.Parameters.Add("@typ1", MySqlDbType.VarChar).Value = pokemon.Typ1;
            cmd.Parameters.Add("@typ2", MySqlDbType.VarChar).Value = pokemon.Typ2;
            cmd.Parameters.Add("@wp", MySqlDbType.Int32).Value = pokemon.Wp;
            cmd.Parameters.Add("@lp", MySqlDbType.Int32).Value = pokemon.Lp;
            cmd.Parameters.Add("@generation", MySqlDbType.VarChar).Value = pokemon.Generation;


            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Dein Pokemon wurde erfolgreich erstellt.", "Erfolgreiche Eintragung", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        static public void LoadData(List<cls_pokemon> pokemonliste)
        {
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);

            string query = "SELECT * FROM tbl_alle";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);

            try
            {
                databaseConnection.Open();

                MySqlDataReader reader = commandDatabase.ExecuteReader();

                while (reader.Read())
                {
                    cls_pokemon p = new cls_pokemon(Convert.ToInt32(reader[0]), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), reader[6].ToString());

                    pokemonliste.Add(p);

                }

                MessageBox.Show("Daten geladen!", "Erfolgreich geladen", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        static public void FilterData(List<cls_pokemon> pokemonliste, string name, string typ1, string typ2, string generation, int lp, int wp)
        {
            MySqlConnection conn = new MySqlConnection(connectionString);
            string query = "SELECT * FROM tbl_alle WHERE 1=1";

            if (!string.IsNullOrEmpty(name))
            {
                query += " AND name LIKE @name";
            }

            if (!string.IsNullOrEmpty(typ1))
            {
                query += " AND typ1 = @typ1";
            }

            if (!string.IsNullOrEmpty(typ2))
            {
                query += " AND typ2 = @typ2";
            }

            if (!string.IsNullOrEmpty(generation))
            {
                query += " AND generation_name = @generation";
            }

            if (lp > 0)
            {
                query += " AND lebenspunkte >= @lp";
            }

            if (wp > 0)
            {
                query += " AND attack >= @wp";
            }

            MySqlCommand cmd = new MySqlCommand(query, conn);
            cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = "%" + name + "%";
            cmd.Parameters.Add("@typ1", MySqlDbType.VarChar).Value = typ1;
            cmd.Parameters.Add("@typ2", MySqlDbType.VarChar).Value = typ2;
            cmd.Parameters.Add("@generation", MySqlDbType.VarChar).Value = generation;
            cmd.Parameters.Add("@lp", MySqlDbType.Int32).Value = lp;
            cmd.Parameters.Add("@wp", MySqlDbType.Int32).Value = wp;

            try
            {
                conn.Open();
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cls_pokemon p = new cls_pokemon(
                        Convert.ToInt32(reader["pokedex"]),
                        Convert.ToString(reader["name"]),
                        Convert.ToString(reader["typ1"]),
                        Convert.ToString(reader["typ2"]),
                        Convert.ToInt32(reader["attack"]),
                        Convert.ToInt32(reader["lebenspunkte"]),
                        Convert.ToString(reader["generation_name"])
                    );
                    pokemonliste.Add(p);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Es ist ein Fehler aufgetreten: " + ex.Message);
                conn.Close();
            }
        }
    }
    }
