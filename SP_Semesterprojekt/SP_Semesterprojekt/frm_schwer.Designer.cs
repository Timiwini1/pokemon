﻿namespace SP_Semesterprojekt
{
    partial class frm_schwer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_schwer));
            this.btn_antwort4 = new System.Windows.Forms.Button();
            this.btn_antwort3 = new System.Windows.Forms.Button();
            this.btn_antwort2 = new System.Windows.Forms.Button();
            this.btn_antwort1 = new System.Windows.Forms.Button();
            this.lbl_frage = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbx_pokemon = new System.Windows.Forms.PictureBox();
            this.btn_zurueck = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pokemon)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_antwort4
            // 
            this.btn_antwort4.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_antwort4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort4.Location = new System.Drawing.Point(385, 456);
            this.btn_antwort4.Name = "btn_antwort4";
            this.btn_antwort4.Size = new System.Drawing.Size(180, 71);
            this.btn_antwort4.TabIndex = 17;
            this.btn_antwort4.Tag = "4";
            this.btn_antwort4.Text = "4";
            this.btn_antwort4.UseVisualStyleBackColor = false;
            this.btn_antwort4.Click += new System.EventHandler(this.checkAnswerEvent);
            // 
            // btn_antwort3
            // 
            this.btn_antwort3.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_antwort3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort3.Location = new System.Drawing.Point(81, 456);
            this.btn_antwort3.Name = "btn_antwort3";
            this.btn_antwort3.Size = new System.Drawing.Size(180, 71);
            this.btn_antwort3.TabIndex = 16;
            this.btn_antwort3.Tag = "3";
            this.btn_antwort3.Text = "3";
            this.btn_antwort3.UseVisualStyleBackColor = false;
            this.btn_antwort3.Click += new System.EventHandler(this.checkAnswerEvent);
            // 
            // btn_antwort2
            // 
            this.btn_antwort2.BackColor = System.Drawing.Color.Orange;
            this.btn_antwort2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort2.Location = new System.Drawing.Point(385, 343);
            this.btn_antwort2.Name = "btn_antwort2";
            this.btn_antwort2.Size = new System.Drawing.Size(180, 71);
            this.btn_antwort2.TabIndex = 15;
            this.btn_antwort2.Tag = "2";
            this.btn_antwort2.Text = "2";
            this.btn_antwort2.UseVisualStyleBackColor = false;
            this.btn_antwort2.Click += new System.EventHandler(this.checkAnswerEvent);
            // 
            // btn_antwort1
            // 
            this.btn_antwort1.BackColor = System.Drawing.Color.Tomato;
            this.btn_antwort1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort1.Location = new System.Drawing.Point(81, 343);
            this.btn_antwort1.Name = "btn_antwort1";
            this.btn_antwort1.Size = new System.Drawing.Size(180, 71);
            this.btn_antwort1.TabIndex = 13;
            this.btn_antwort1.Tag = "1";
            this.btn_antwort1.Text = "1";
            this.btn_antwort1.UseVisualStyleBackColor = false;
            this.btn_antwort1.Click += new System.EventHandler(this.checkAnswerEvent);
            // 
            // lbl_frage
            // 
            this.lbl_frage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_frage.Location = new System.Drawing.Point(134, 296);
            this.lbl_frage.Name = "lbl_frage";
            this.lbl_frage.Size = new System.Drawing.Size(359, 56);
            this.lbl_frage.TabIndex = 21;
            this.lbl_frage.Text = "Frage";
            this.lbl_frage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(578, 455);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(68, 72);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 20;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(578, 11);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(68, 72);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 267);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // pbx_pokemon
            // 
            this.pbx_pokemon.Image = global::SP_Semesterprojekt.Properties.Resources.brutalanda;
            this.pbx_pokemon.Location = new System.Drawing.Point(93, 12);
            this.pbx_pokemon.Name = "pbx_pokemon";
            this.pbx_pokemon.Size = new System.Drawing.Size(472, 258);
            this.pbx_pokemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_pokemon.TabIndex = 12;
            this.pbx_pokemon.TabStop = false;
            // 
            // btn_zurueck
            // 
            this.btn_zurueck.BackColor = System.Drawing.Color.BlueViolet;
            this.btn_zurueck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_zurueck.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_zurueck.Location = new System.Drawing.Point(274, 419);
            this.btn_zurueck.Name = "btn_zurueck";
            this.btn_zurueck.Size = new System.Drawing.Size(95, 33);
            this.btn_zurueck.TabIndex = 22;
            this.btn_zurueck.Text = "Zurück";
            this.btn_zurueck.UseVisualStyleBackColor = false;
            this.btn_zurueck.Click += new System.EventHandler(this.btn_zurueck_Click);
            // 
            // frm_schwer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(658, 539);
            this.ControlBox = false;
            this.Controls.Add(this.btn_zurueck);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_antwort4);
            this.Controls.Add(this.btn_antwort3);
            this.Controls.Add(this.btn_antwort2);
            this.Controls.Add(this.btn_antwort1);
            this.Controls.Add(this.pbx_pokemon);
            this.Controls.Add(this.lbl_frage);
            this.Name = "frm_schwer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quiz schwer";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pokemon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_antwort4;
        private System.Windows.Forms.Button btn_antwort3;
        private System.Windows.Forms.Button btn_antwort2;
        private System.Windows.Forms.Button btn_antwort1;
        private System.Windows.Forms.PictureBox pbx_pokemon;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_frage;
        private System.Windows.Forms.Button btn_zurueck;
    }
}