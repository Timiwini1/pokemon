﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_suchen : Form
    {
        List<cls_pokemon> hilfsliste = new List<cls_pokemon>();
        public frm_suchen()
        {
            InitializeComponent();
            lbx_suchen.DisplayMember = "Ausgabe";
        }

        public frm_suchen(List<cls_pokemon> pokelist)
        {
            InitializeComponent();
           
        }

        private void frm_suchen_Load(object sender, EventArgs e)
        {
            //Name
            lbl_name.Hide();
            txt_name.Hide();
            //Typ1
            lbl_typ1.Hide();
            com_typ1.Hide();
            //Typ2
            lbl_typ2.Hide();
            com_typ2.Hide();
            //Generation
            lbl_generation.Hide();
            com_generation.Hide();
            //Lebenspunkte
            lbl_lp.Hide();
            lbl_lp_zwischen.Hide();
            num_lp_min.Hide();
            //Wttkampfpunkte
            lbl_wp.Hide();
            lbl_wp_zwischen.Hide();
            num_wp_min.Hide();
        }

        private void cb_name_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_name.Checked)
            {
                lbl_name.Show();
                txt_name.Show();
            }
            else
            {
                lbl_name.Hide();
                txt_name.Hide();
                txt_name.Text = "";
            }
        }

        private void cb_typ1_CheckedChanged(object sender, EventArgs e)
        {
            if(cb_typ1.Checked)
            {
                lbl_typ1.Show();
                com_typ1.Show();
                lbl_typ2.Show();
                com_typ2.Show();
            }
            else
            {
                lbl_typ1.Hide();
                com_typ1.Hide();
                lbl_typ2.Hide();
                com_typ2.Hide();
                com_typ1.Text = "";
                com_typ2.Text = "";
            }
        }


        

        private void cb_generation_CheckedChanged(object sender, EventArgs e)
        {
            if(cb_generation.Checked)
            {
                lbl_generation.Show();
                com_generation.Show();
            }
            else
            {
                lbl_generation.Hide();
                com_generation.Hide();
                com_generation.Text = "";
            }
        }

        private void cb_lp_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_lp.Checked)
            {
                lbl_lp.Show();
                lbl_lp_zwischen.Show();
                num_lp_min.Show();
            }
            else
            {
                lbl_lp.Hide();
                lbl_lp_zwischen.Hide();
                num_lp_min.Hide();
                num_lp_min.Value= 0;
                
            }
        }

        private void cb_wp_CheckedChanged(object sender, EventArgs e)
        {
            if(cb_wp.Checked)
            {
                lbl_wp.Show();
                lbl_wp_zwischen.Show();
                num_wp_min.Show();
            }
            else
            {
                lbl_wp.Hide();
                lbl_wp_zwischen.Hide();
                num_wp_min.Hide();
                num_wp_min.Value = 0;
            }
        }

        private void com_typ1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (com_typ1.Text == "Boden")
            {
                this.BackColor = System.Drawing.Color.Chocolate;
            }
            else if (com_typ1.Text == "Drache")
            {
                this.BackColor = System.Drawing.Color.Teal;
            }
            else if (com_typ1.Text == "Eis")
            {
                this.BackColor = System.Drawing.Color.PaleTurquoise;
            }
            else if (com_typ1.Text == "Elektro")
            {
                this.BackColor = System.Drawing.Color.Gold;
            }
            else if (com_typ1.Text == "Fee")
            {
                this.BackColor = System.Drawing.Color.LightPink;
            }
            else if (com_typ1.Text == "Feuer")
            {
                this.BackColor = System.Drawing.Color.Tomato;
            }
            else if (com_typ1.Text == "Flug")
            {
                this.BackColor = System.Drawing.Color.Azure;
            }
            else if (com_typ1.Text == "Geist")
            {
                this.BackColor = System.Drawing.Color.MediumPurple;
            }
            else if (com_typ1.Text == "Gestein")
            {
                this.BackColor = System.Drawing.Color.BurlyWood;
            }
            else if (com_typ1.Text == "Gift")
            {
                this.BackColor = System.Drawing.Color.Plum;
            }
            else if (com_typ1.Text == "Kampf")
            {
                this.BackColor = System.Drawing.Color.IndianRed;
            }
            else if (com_typ1.Text == "Käfer")
            {
                this.BackColor = System.Drawing.Color.Olive;
            }
            else if (com_typ1.Text == "Normal")
            {
                this.BackColor = System.Drawing.Color.Silver;
            }
            else if (com_typ1.Text == "Pflanze")
            {
                this.BackColor = System.Drawing.Color.YellowGreen;
            }
            else if (com_typ1.Text == "Psycho")
            {
                this.BackColor = System.Drawing.Color.PaleVioletRed;
            }
            else if (com_typ1.Text == "Stahl")
            {
                this.BackColor = System.Drawing.Color.Gray;
            }
            else if (com_typ1.Text == "Unlicht")
            {
                this.BackColor = System.Drawing.Color.DimGray;
            }
            else if (com_typ1.Text == "Wasser")
            {
                this.BackColor = System.Drawing.Color.RoyalBlue;
            }
            else if (com_typ1.Text == "Alle")
            {
                this.BackColor = System.Drawing.Color.Beige;
            }
        }

        private void btn_filtern_Click(object sender, EventArgs e)
        {
            
            try
            {
                hilfsliste.Clear();
                lbx_suchen.Items.Clear();



                string name = txt_name.Text;
                string typ1 = com_typ1.Text;
                string typ2 = com_typ2.Text;
                string generation = com_generation.Text;
                int lp = (int)num_lp_min.Value; 
                int wp = (int)num_wp_min.Value; 

               
                clsDataProvider.FilterData(hilfsliste, name, typ1, typ2, generation, lp, wp);

               
                foreach (cls_pokemon pokemon in hilfsliste)
                {
  
                        lbx_suchen.Items.Add(pokemon);
                    
                }

                MessageBox.Show("Daten wurden gefiltert und aktualisiert.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler beim Filtern: " + ex.Message, "Fehler beim Filtern", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_filtern_Click_1(object sender, EventArgs e)
        {

            try
            {
                hilfsliste.Clear();
                lbx_suchen.Items.Clear();



                string name = txt_name.Text;
                string typ1 = com_typ1.Text;
                string typ2 = com_typ2.Text;
                string generation = com_generation.Text;
                int lp = (int)num_lp_min.Value;
                int wp = (int)num_wp_min.Value;


                clsDataProvider.FilterData(hilfsliste, name, typ1, typ2, generation, lp, wp);


                foreach (cls_pokemon pokemon in hilfsliste)
                {

                    lbx_suchen.Items.Add(pokemon);

                }

                MessageBox.Show("Daten wurden gefiltert und aktualisiert.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fehler beim Filtern: " + ex.Message, "Fehler beim Filtern", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
