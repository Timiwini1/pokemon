﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_quiz : Form
    {
        public frm_quiz()
        {
            InitializeComponent();
        }

        private void btn_leicht_Click(object sender, EventArgs e)
        {
            frm_leicht l = new frm_leicht();
            l.ShowDialog();
        }
       
        private void btn_mittel_Click(object sender, EventArgs e)
        {
            frm_mittel m = new frm_mittel();
            m.ShowDialog();
        }

        private void btn_schwer_Click(object sender, EventArgs e)
        {
            frm_schwer s = new frm_schwer();
            s.ShowDialog();
        }

        private void btn_profi_Click(object sender, EventArgs e)
        {
            frm_profi p = new frm_profi();
            p.ShowDialog();
        }

        private void btn_hauptmenü_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
