﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SP_Semesterprojekt
{
    public class cls_pokemon
    {
        int m_pokedex;
        string m_name;
        string m_typ1;
        string m_typ2;
        double m_wp;
        double m_lp;
        string m_generation;

        public int Pokedex { get => m_pokedex; set => m_pokedex = value; }
        public string Name { get => m_name; set => m_name = value; }
        public string Typ1 { get => m_typ1; set => m_typ1 = value; }
        public string Typ2 { get => m_typ2; set => m_typ2 = value; }
        public double Wp { get => m_wp; set => m_wp = value; }
        public double Lp { get => m_lp; set => m_lp = value; }
        public string Generation { get => m_generation; set => m_generation = value; }

        public cls_pokemon()
        {

        }

        public cls_pokemon(string name, string typ1, string typ2, int wp, int lp, string generation)
        {
            m_name = name;
            m_typ1 = typ1;
            m_typ2 = typ2;
            m_wp = wp;
            m_lp = lp;
            m_generation = generation;

        }

        public cls_pokemon(int pokedex, string name, string typ1, string typ2, int wp, int lp, string generation)
        {
            m_pokedex = pokedex;
            m_name = name;
            m_typ1 = typ1;
            m_typ2 = typ2;
            m_wp = wp;
            m_lp = lp;
            m_generation = generation;
            
        }

        public string Ausgabe
        {
            get
            {
                if (!string.IsNullOrEmpty(m_typ2))
                {
                    return string.Format("#{0} | {1} | {2} / {3} | WP: {4} | LP: {5} | Region: {6} ", m_pokedex, m_name, m_typ1, m_typ2, m_wp, m_lp, m_generation);
                }
                else
                {
                    return string.Format("#{0} | {1} | {2} | WP: {3} | LP: {4} | Region: {5} ", m_pokedex, m_name, m_typ1, m_wp, m_lp, m_generation);
                }
            }
        }

        public string Kämpfen
        {
            get
            {
                if (!string.IsNullOrEmpty(m_typ2))
                {
                    return string.Format("#{0} | {1} | {2} / {3} | WP: {4} | LP: {5} | Region: {6} ", m_pokedex, m_name, m_typ1, m_typ2, m_wp, m_lp, m_generation);
                }
                else
                {
                    return string.Format("#{0} | {1} | {2} | WP: {3} | LP: {4} | Region: {5} ", m_pokedex, m_name, m_typ1, m_wp, m_lp, m_generation);
                }
            }
        }



    }

}
