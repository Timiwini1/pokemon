﻿namespace SP_Semesterprojekt
{
    partial class frm_quiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_überschrift = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_leicht = new System.Windows.Forms.Button();
            this.btn_mittel = new System.Windows.Forms.Button();
            this.btn_schwer = new System.Windows.Forms.Button();
            this.btn_profi = new System.Windows.Forms.Button();
            this.btn_hauptmenü = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_überschrift
            // 
            this.lbl_überschrift.AutoSize = true;
            this.lbl_überschrift.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_überschrift.Location = new System.Drawing.Point(58, 21);
            this.lbl_überschrift.Name = "lbl_überschrift";
            this.lbl_überschrift.Size = new System.Drawing.Size(628, 46);
            this.lbl_überschrift.TabIndex = 1;
            this.lbl_überschrift.Text = "Willkommen beim Pokemonquiz!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(476, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Wähle deinen Schwierigkeitsgrad!";
            // 
            // btn_leicht
            // 
            this.btn_leicht.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_leicht.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_leicht.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_leicht.Location = new System.Drawing.Point(78, 185);
            this.btn_leicht.Name = "btn_leicht";
            this.btn_leicht.Size = new System.Drawing.Size(104, 160);
            this.btn_leicht.TabIndex = 3;
            this.btn_leicht.Text = "Leicht";
            this.btn_leicht.UseVisualStyleBackColor = false;
            this.btn_leicht.Click += new System.EventHandler(this.btn_leicht_Click);
            // 
            // btn_mittel
            // 
            this.btn_mittel.BackColor = System.Drawing.Color.Gold;
            this.btn_mittel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_mittel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_mittel.Location = new System.Drawing.Point(259, 185);
            this.btn_mittel.Name = "btn_mittel";
            this.btn_mittel.Size = new System.Drawing.Size(104, 160);
            this.btn_mittel.TabIndex = 4;
            this.btn_mittel.Text = "Mittel";
            this.btn_mittel.UseVisualStyleBackColor = false;
            this.btn_mittel.Click += new System.EventHandler(this.btn_mittel_Click);
            // 
            // btn_schwer
            // 
            this.btn_schwer.BackColor = System.Drawing.Color.Tomato;
            this.btn_schwer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_schwer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_schwer.Location = new System.Drawing.Point(445, 185);
            this.btn_schwer.Name = "btn_schwer";
            this.btn_schwer.Size = new System.Drawing.Size(104, 160);
            this.btn_schwer.TabIndex = 5;
            this.btn_schwer.Text = "Schwer";
            this.btn_schwer.UseVisualStyleBackColor = false;
            this.btn_schwer.Click += new System.EventHandler(this.btn_schwer_Click);
            // 
            // btn_profi
            // 
            this.btn_profi.BackColor = System.Drawing.Color.Brown;
            this.btn_profi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_profi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_profi.Location = new System.Drawing.Point(623, 185);
            this.btn_profi.Name = "btn_profi";
            this.btn_profi.Size = new System.Drawing.Size(104, 160);
            this.btn_profi.TabIndex = 6;
            this.btn_profi.Text = "Profi";
            this.btn_profi.UseVisualStyleBackColor = false;
            this.btn_profi.Click += new System.EventHandler(this.btn_profi_Click);
            // 
            // btn_hauptmenü
            // 
            this.btn_hauptmenü.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_hauptmenü.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_hauptmenü.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_hauptmenü.Location = new System.Drawing.Point(273, 367);
            this.btn_hauptmenü.Name = "btn_hauptmenü";
            this.btn_hauptmenü.Size = new System.Drawing.Size(257, 71);
            this.btn_hauptmenü.TabIndex = 7;
            this.btn_hauptmenü.Text = "Zurück zum Hauptmenü";
            this.btn_hauptmenü.UseVisualStyleBackColor = false;
            this.btn_hauptmenü.Click += new System.EventHandler(this.btn_hauptmenü_Click);
            // 
            // frm_quiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_hauptmenü);
            this.Controls.Add(this.btn_profi);
            this.Controls.Add(this.btn_schwer);
            this.Controls.Add(this.btn_mittel);
            this.Controls.Add(this.btn_leicht);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_überschrift);
            this.Name = "frm_quiz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quizauswahl";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_überschrift;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_leicht;
        private System.Windows.Forms.Button btn_mittel;
        private System.Windows.Forms.Button btn_schwer;
        private System.Windows.Forms.Button btn_profi;
        private System.Windows.Forms.Button btn_hauptmenü;
    }
}