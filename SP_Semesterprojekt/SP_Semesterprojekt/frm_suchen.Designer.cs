﻿namespace SP_Semesterprojekt
{
    partial class frm_suchen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_name = new System.Windows.Forms.TextBox();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_lp = new System.Windows.Forms.Label();
            this.lbl_wp = new System.Windows.Forms.Label();
            this.lbl_generation = new System.Windows.Forms.Label();
            this.lbl_typ2 = new System.Windows.Forms.Label();
            this.cb_name = new System.Windows.Forms.CheckBox();
            this.cb_typ1 = new System.Windows.Forms.CheckBox();
            this.cb_generation = new System.Windows.Forms.CheckBox();
            this.cb_lp = new System.Windows.Forms.CheckBox();
            this.cb_wp = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_typ1 = new System.Windows.Forms.Label();
            this.com_typ1 = new System.Windows.Forms.ComboBox();
            this.com_typ2 = new System.Windows.Forms.ComboBox();
            this.com_generation = new System.Windows.Forms.ComboBox();
            this.num_lp_min = new System.Windows.Forms.NumericUpDown();
            this.lbl_lp_zwischen = new System.Windows.Forms.Label();
            this.lbl_wp_zwischen = new System.Windows.Forms.Label();
            this.num_wp_min = new System.Windows.Forms.NumericUpDown();
            this.lbx_suchen = new System.Windows.Forms.ListBox();
            this.btn_filtern = new System.Windows.Forms.Button();
            this.btn_zurueck = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_lp_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_wp_min)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(317, 60);
            this.txt_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 22);
            this.txt_name.TabIndex = 0;
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(221, 63);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(44, 16);
            this.lbl_name.TabIndex = 1;
            this.lbl_name.Text = "Name";
            // 
            // lbl_lp
            // 
            this.lbl_lp.AutoSize = true;
            this.lbl_lp.Location = new System.Drawing.Point(839, 119);
            this.lbl_lp.Name = "lbl_lp";
            this.lbl_lp.Size = new System.Drawing.Size(92, 16);
            this.lbl_lp.TabIndex = 2;
            this.lbl_lp.Text = "Lebenspunkte";
            // 
            // lbl_wp
            // 
            this.lbl_wp.AutoSize = true;
            this.lbl_wp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_wp.Location = new System.Drawing.Point(816, 15);
            this.lbl_wp.Name = "lbl_wp";
            this.lbl_wp.Size = new System.Drawing.Size(111, 16);
            this.lbl_wp.TabIndex = 3;
            this.lbl_wp.Text = "Wettkampfpunkte";
            // 
            // lbl_generation
            // 
            this.lbl_generation.AutoSize = true;
            this.lbl_generation.Location = new System.Drawing.Point(221, 169);
            this.lbl_generation.Name = "lbl_generation";
            this.lbl_generation.Size = new System.Drawing.Size(73, 16);
            this.lbl_generation.TabIndex = 4;
            this.lbl_generation.Text = "Generation";
            // 
            // lbl_typ2
            // 
            this.lbl_typ2.AutoSize = true;
            this.lbl_typ2.Location = new System.Drawing.Point(520, 167);
            this.lbl_typ2.Name = "lbl_typ2";
            this.lbl_typ2.Size = new System.Drawing.Size(41, 16);
            this.lbl_typ2.TabIndex = 5;
            this.lbl_typ2.Text = "Typ 2";
            // 
            // cb_name
            // 
            this.cb_name.AutoSize = true;
            this.cb_name.Location = new System.Drawing.Point(15, 53);
            this.cb_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_name.Name = "cb_name";
            this.cb_name.Size = new System.Drawing.Size(66, 20);
            this.cb_name.TabIndex = 6;
            this.cb_name.Text = "Name";
            this.cb_name.UseVisualStyleBackColor = true;
            this.cb_name.CheckedChanged += new System.EventHandler(this.cb_name_CheckedChanged);
            // 
            // cb_typ1
            // 
            this.cb_typ1.AutoSize = true;
            this.cb_typ1.Location = new System.Drawing.Point(15, 81);
            this.cb_typ1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_typ1.Name = "cb_typ1";
            this.cb_typ1.Size = new System.Drawing.Size(56, 20);
            this.cb_typ1.TabIndex = 7;
            this.cb_typ1.Text = "Typ ";
            this.cb_typ1.UseVisualStyleBackColor = true;
            this.cb_typ1.CheckedChanged += new System.EventHandler(this.cb_typ1_CheckedChanged);
            // 
            // cb_generation
            // 
            this.cb_generation.AutoSize = true;
            this.cb_generation.Location = new System.Drawing.Point(15, 110);
            this.cb_generation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_generation.Name = "cb_generation";
            this.cb_generation.Size = new System.Drawing.Size(95, 20);
            this.cb_generation.TabIndex = 8;
            this.cb_generation.Text = "Generation";
            this.cb_generation.UseVisualStyleBackColor = true;
            this.cb_generation.CheckedChanged += new System.EventHandler(this.cb_generation_CheckedChanged);
            // 
            // cb_lp
            // 
            this.cb_lp.AutoSize = true;
            this.cb_lp.Location = new System.Drawing.Point(15, 138);
            this.cb_lp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_lp.Name = "cb_lp";
            this.cb_lp.Size = new System.Drawing.Size(114, 20);
            this.cb_lp.TabIndex = 9;
            this.cb_lp.Text = "Lebenspunkte";
            this.cb_lp.UseVisualStyleBackColor = true;
            this.cb_lp.CheckedChanged += new System.EventHandler(this.cb_lp_CheckedChanged);
            // 
            // cb_wp
            // 
            this.cb_wp.AutoSize = true;
            this.cb_wp.Location = new System.Drawing.Point(15, 166);
            this.cb_wp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_wp.Name = "cb_wp";
            this.cb_wp.Size = new System.Drawing.Size(133, 20);
            this.cb_wp.TabIndex = 10;
            this.cb_wp.Text = "Wettkampfpunkte";
            this.cb_wp.UseVisualStyleBackColor = true;
            this.cb_wp.CheckedChanged += new System.EventHandler(this.cb_wp_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Haettenschweiler", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label1.Location = new System.Drawing.Point(9, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nach was willst du filtern?";
            // 
            // lbl_typ1
            // 
            this.lbl_typ1.AutoSize = true;
            this.lbl_typ1.Location = new System.Drawing.Point(520, 63);
            this.lbl_typ1.Name = "lbl_typ1";
            this.lbl_typ1.Size = new System.Drawing.Size(41, 16);
            this.lbl_typ1.TabIndex = 13;
            this.lbl_typ1.Text = "Typ 1";
            // 
            // com_typ1
            // 
            this.com_typ1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.com_typ1.FormattingEnabled = true;
            this.com_typ1.Items.AddRange(new object[] {
            "Boden",
            "Drache",
            "Eis",
            "Elektro",
            "Fee",
            "Feuer",
            "Flug",
            "Geist",
            "Gestein",
            "Gift",
            "Kampf",
            "Käfer",
            "Normal",
            "Pflanze",
            "Psycho",
            "Stahl",
            "Unlicht",
            "Wasser"});
            this.com_typ1.Location = new System.Drawing.Point(577, 59);
            this.com_typ1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.com_typ1.Name = "com_typ1";
            this.com_typ1.Size = new System.Drawing.Size(121, 24);
            this.com_typ1.TabIndex = 14;
            this.com_typ1.SelectedIndexChanged += new System.EventHandler(this.com_typ1_SelectedIndexChanged);
            // 
            // com_typ2
            // 
            this.com_typ2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.com_typ2.FormattingEnabled = true;
            this.com_typ2.Items.AddRange(new object[] {
            "",
            "Boden",
            "Drache",
            "Eis",
            "Elektro",
            "Fee",
            "Feuer",
            "Flug",
            "Geist",
            "Gestein",
            "Gift",
            "Kampf",
            "Käfer",
            "Normal",
            "Pflanze",
            "Psycho",
            "Stahl",
            "Unlicht",
            "Wasser"});
            this.com_typ2.Location = new System.Drawing.Point(577, 165);
            this.com_typ2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.com_typ2.Name = "com_typ2";
            this.com_typ2.Size = new System.Drawing.Size(121, 24);
            this.com_typ2.TabIndex = 15;
            // 
            // com_generation
            // 
            this.com_generation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.com_generation.FormattingEnabled = true;
            this.com_generation.Items.AddRange(new object[] {
            "Alola",
            "Einall",
            "Galar",
            "Hoenn",
            "Johto",
            "Kalos",
            "Kanto",
            "Paldea",
            "Sinnoh",
            "Spectra"});
            this.com_generation.Location = new System.Drawing.Point(317, 164);
            this.com_generation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.com_generation.Name = "com_generation";
            this.com_generation.Size = new System.Drawing.Size(121, 24);
            this.com_generation.TabIndex = 16;
            // 
            // num_lp_min
            // 
            this.num_lp_min.Location = new System.Drawing.Point(832, 165);
            this.num_lp_min.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num_lp_min.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.num_lp_min.Name = "num_lp_min";
            this.num_lp_min.Size = new System.Drawing.Size(120, 22);
            this.num_lp_min.TabIndex = 17;
            // 
            // lbl_lp_zwischen
            // 
            this.lbl_lp_zwischen.AutoSize = true;
            this.lbl_lp_zwischen.Location = new System.Drawing.Point(857, 144);
            this.lbl_lp_zwischen.Name = "lbl_lp_zwischen";
            this.lbl_lp_zwischen.Size = new System.Drawing.Size(76, 16);
            this.lbl_lp_zwischen.TabIndex = 18;
            this.lbl_lp_zwischen.Text = "Mindestens";
            // 
            // lbl_wp_zwischen
            // 
            this.lbl_wp_zwischen.AutoSize = true;
            this.lbl_wp_zwischen.Location = new System.Drawing.Point(857, 39);
            this.lbl_wp_zwischen.Name = "lbl_wp_zwischen";
            this.lbl_wp_zwischen.Size = new System.Drawing.Size(76, 16);
            this.lbl_wp_zwischen.TabIndex = 22;
            this.lbl_wp_zwischen.Text = "Mindestens";
            // 
            // num_wp_min
            // 
            this.num_wp_min.Location = new System.Drawing.Point(832, 60);
            this.num_wp_min.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num_wp_min.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.num_wp_min.Name = "num_wp_min";
            this.num_wp_min.Size = new System.Drawing.Size(120, 22);
            this.num_wp_min.TabIndex = 21;
            // 
            // lbx_suchen
            // 
            this.lbx_suchen.FormattingEnabled = true;
            this.lbx_suchen.ItemHeight = 16;
            this.lbx_suchen.Location = new System.Drawing.Point(29, 226);
            this.lbx_suchen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbx_suchen.Name = "lbx_suchen";
            this.lbx_suchen.Size = new System.Drawing.Size(1151, 468);
            this.lbx_suchen.TabIndex = 25;
            // 
            // btn_filtern
            // 
            this.btn_filtern.Location = new System.Drawing.Point(1004, 159);
            this.btn_filtern.Margin = new System.Windows.Forms.Padding(4);
            this.btn_filtern.Name = "btn_filtern";
            this.btn_filtern.Size = new System.Drawing.Size(177, 62);
            this.btn_filtern.TabIndex = 26;
            this.btn_filtern.Text = "Filtern";
            this.btn_filtern.UseVisualStyleBackColor = true;
            this.btn_filtern.Click += new System.EventHandler(this.btn_filtern_Click_1);
            // 
            // btn_zurueck
            // 
            this.btn_zurueck.Location = new System.Drawing.Point(1004, 73);
            this.btn_zurueck.Margin = new System.Windows.Forms.Padding(4);
            this.btn_zurueck.Name = "btn_zurueck";
            this.btn_zurueck.Size = new System.Drawing.Size(177, 62);
            this.btn_zurueck.TabIndex = 27;
            this.btn_zurueck.Text = "Zurück zum Hauptmenü";
            this.btn_zurueck.UseVisualStyleBackColor = true;
            this.btn_zurueck.Click += new System.EventHandler(this.btn_zurueck_Click);
            // 
            // frm_suchen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 711);
            this.Controls.Add(this.btn_zurueck);
            this.Controls.Add(this.btn_filtern);
            this.Controls.Add(this.lbx_suchen);
            this.Controls.Add(this.lbl_wp_zwischen);
            this.Controls.Add(this.num_wp_min);
            this.Controls.Add(this.lbl_lp_zwischen);
            this.Controls.Add(this.num_lp_min);
            this.Controls.Add(this.com_generation);
            this.Controls.Add(this.com_typ2);
            this.Controls.Add(this.com_typ1);
            this.Controls.Add(this.lbl_typ1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_wp);
            this.Controls.Add(this.cb_lp);
            this.Controls.Add(this.cb_generation);
            this.Controls.Add(this.cb_typ1);
            this.Controls.Add(this.cb_name);
            this.Controls.Add(this.lbl_typ2);
            this.Controls.Add(this.lbl_generation);
            this.Controls.Add(this.lbl_wp);
            this.Controls.Add(this.lbl_lp);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.txt_name);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm_suchen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frm_suchen";
            this.Load += new System.EventHandler(this.frm_suchen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.num_lp_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_wp_min)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_lp;
        private System.Windows.Forms.Label lbl_wp;
        private System.Windows.Forms.Label lbl_generation;
        private System.Windows.Forms.Label lbl_typ2;
        private System.Windows.Forms.CheckBox cb_name;
        private System.Windows.Forms.CheckBox cb_typ1;
        private System.Windows.Forms.CheckBox cb_generation;
        private System.Windows.Forms.CheckBox cb_lp;
        private System.Windows.Forms.CheckBox cb_wp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_typ1;
        private System.Windows.Forms.ComboBox com_typ1;
        private System.Windows.Forms.ComboBox com_typ2;
        private System.Windows.Forms.ComboBox com_generation;
        private System.Windows.Forms.NumericUpDown num_lp_min;
        private System.Windows.Forms.Label lbl_lp_zwischen;
        private System.Windows.Forms.Label lbl_wp_zwischen;
        private System.Windows.Forms.NumericUpDown num_wp_min;
        private System.Windows.Forms.ListBox lbx_suchen;
        private System.Windows.Forms.Button btn_filtern;
        private System.Windows.Forms.Button btn_zurueck;
    }
}