﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_mittel : Form
    {
        //Variablen für das Quiz
        int correctAnswer;
        int questionNumber = 1;
        int score;
        int percentage;
        int totalQuestions;
        public frm_mittel()
        {
            InitializeComponent();
            askQuestion(questionNumber);
            totalQuestions = 10;
        }

        private void frm_mittel_Load(object sender, EventArgs e)
        {

        }

        private void checkAnswerEvent(object sender, EventArgs e)
        {
            var senderObject = (Button)sender;
            int buttonTag = Convert.ToInt32(senderObject.Tag);

            if (buttonTag == correctAnswer)
            {
                score++;
            }
            if (questionNumber == totalQuestions)
            {
                // work out the percentage
                percentage = (int)Math.Round((double)(score * 100) / totalQuestions);
                MessageBox.Show(
                    "Fertig!" + Environment.NewLine +
                    "Du hast " + score + "/10 Fragen richtig beantwortet.");

                DialogResult = DialogResult.OK;
            }
            questionNumber++;
            askQuestion(questionNumber);
        }

        private void askQuestion(int qnum)
        {
            switch (qnum)
            {
                case 1:
                    pbx_pokemon.Image = Properties.Resources.karpador;
                    lbl_frage.Text = "Wie heißt die Vorentwicklung von Garados?";
                    btn_antwort1.Text = "Hypno";
                    btn_antwort2.Text = "Abra";
                    btn_antwort3.Text = "Barschwa";
                    btn_antwort4.Text = "Karpador";
                    correctAnswer = 4;
                    break;
                case 2:
                    pbx_pokemon.Image = Properties.Resources.johto;
                    lbl_frage.Text = "Wie heißt die zweite Region in Pokemon?";
                    btn_antwort1.Text = "Kanto";
                    btn_antwort2.Text = "Alola";
                    btn_antwort3.Text = "Einall";
                    btn_antwort4.Text = "Johto";
                    correctAnswer = 4;
                    break;
                case 3:
                    pbx_pokemon.Image = Properties.Resources.pokeball;
                    lbl_frage.Text = "Welche Farben umfassen den Hyperball?";
                    btn_antwort1.Text = "Schwarz, Gelb und Weiß";
                    btn_antwort2.Text = "Orange und Weiß";
                    btn_antwort3.Text = "Blau, Rot und Weiß";
                    btn_antwort4.Text = "Grün, Violett und Weiß";
                    correctAnswer = 1;
                    break;
                case 4:
                    pbx_pokemon.Image = Properties.Resources.igamaro;
                    lbl_frage.Text = "Welches Pokémon ist ein Starterpokemon?";
                    btn_antwort1.Text = "Zickzack";
                    btn_antwort2.Text = "Larvitar";
                    btn_antwort3.Text = "Igamaro";
                    btn_antwort4.Text = "Vulpix"; ;
                    correctAnswer = 3;
                    break;
                case 5:
                    pbx_pokemon.Image = Properties.Resources.typen;
                    lbl_frage.Text = "Welchen Typ gibt es wirklich?";
                    btn_antwort1.Text = "Keramik";
                    btn_antwort2.Text = "Hagel";
                    btn_antwort3.Text = "Gold";
                    btn_antwort4.Text = "Stahl";
                    correctAnswer = 4;
                    break;
                case 6:
                    pbx_pokemon.Image = Properties.Resources.nachtara;
                    lbl_frage.Text = "Welches Evoli Entwicklung ist das?";
                    btn_antwort1.Text = "Nachtara";
                    btn_antwort2.Text = "Flamara";
                    btn_antwort3.Text = "Psiana";
                    btn_antwort4.Text = "Felinara";
                    correctAnswer = 1;
                    break;
                case 7:
                    pbx_pokemon.Image = Properties.Resources.lapras;
                    lbl_frage.Text = "Welchen Typ hat Lapras außer Wasser noch?";
                    btn_antwort1.Text = "Boden";
                    btn_antwort2.Text = "Flug";
                    btn_antwort3.Text = "Eis";
                    btn_antwort4.Text = "Pflanze";
                    correctAnswer = 3;
                    break;
                case 8:
                    pbx_pokemon.Image = Properties.Resources.niantic;
                    lbl_frage.Text = "Welche Firma hat Pokemon Go erfunden?";
                    btn_antwort1.Text = "Apple";
                    btn_antwort2.Text = "Supercell";
                    btn_antwort3.Text = "Epic Games";
                    btn_antwort4.Text = "Niantic";
                    correctAnswer = 4;
                    break;

                case 9:
                    pbx_pokemon.Image = Properties.Resources.pokemon;
                    lbl_frage.Text = "Wie viele Pokemon gibt es?";
                    btn_antwort1.Text = "ca. 500 ";
                    btn_antwort2.Text = "ca. 750 ";
                    btn_antwort3.Text = "ca. 900";
                    btn_antwort4.Text = "über 1000";
                    correctAnswer = 4;
                    break;

                case 10:
                    pbx_pokemon.Image = Properties.Resources.ash;
                    lbl_frage.Text = "Wie heißt der Hauptcharakter im Anime 'Pokemon'?";
                    btn_antwort1.Text = "Nico";
                    btn_antwort2.Text = "Bob";
                    btn_antwort3.Text = "Ash";
                    btn_antwort4.Text = "Simon";
                    correctAnswer = 3;
                    break;

            }
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
