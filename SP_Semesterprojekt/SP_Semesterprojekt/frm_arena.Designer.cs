﻿namespace SP_Semesterprojekt
{
    partial class frm_arena
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.lbl_spieler1 = new System.Windows.Forms.Label();
            this.lbl_spieler1wp = new System.Windows.Forms.Label();
            this.lbl_wp1 = new System.Windows.Forms.Label();
            this.lbl_lp1 = new System.Windows.Forms.Label();
            this.lbl_spieler1lp = new System.Windows.Forms.Label();
            this.lbl_gegnerlp = new System.Windows.Forms.Label();
            this.lbl_lp2 = new System.Windows.Forms.Label();
            this.lbl_wp2 = new System.Windows.Forms.Label();
            this.lbl_gegnerwp = new System.Windows.Forms.Label();
            this.lbl_gegner2 = new System.Windows.Forms.Label();
            this.lbl_Schadenspieler1 = new System.Windows.Forms.Label();
            this.lbl_schadengegner = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.player1 = new System.Windows.Forms.PictureBox();
            this.player2 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbl_timer = new System.Windows.Forms.Label();
            this.lbl_spieler1typ1 = new System.Windows.Forms.Label();
            this.lbl_spieler1typ2 = new System.Windows.Forms.Label();
            this.lbl_gegnertyp1 = new System.Windows.Forms.Label();
            this.lbl_gegnertyp2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Red;
            this.pictureBox1.Location = new System.Drawing.Point(82, 443);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1160, 70);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "platform";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Red;
            this.pictureBox2.Location = new System.Drawing.Point(125, 499);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1068, 70);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "platform";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Red;
            this.pictureBox3.Location = new System.Drawing.Point(176, 559);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(972, 70);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "platform";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Red;
            this.pictureBox4.Location = new System.Drawing.Point(219, 615);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(865, 70);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "platform";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Red;
            this.pictureBox5.Location = new System.Drawing.Point(271, 665);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(772, 70);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Tag = "platform";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.Red;
            this.pictureBox6.Location = new System.Drawing.Point(202, 300);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(224, 38);
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "platform";
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.Red;
            this.pictureBox7.Location = new System.Drawing.Point(889, 300);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(224, 38);
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "platform";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Red;
            this.pictureBox8.Location = new System.Drawing.Point(492, 230);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(333, 38);
            this.pictureBox8.TabIndex = 7;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "platform";
            // 
            // lbl_spieler1
            // 
            this.lbl_spieler1.BackColor = System.Drawing.Color.Blue;
            this.lbl_spieler1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1.ForeColor = System.Drawing.Color.White;
            this.lbl_spieler1.Location = new System.Drawing.Point(40, 9);
            this.lbl_spieler1.Name = "lbl_spieler1";
            this.lbl_spieler1.Size = new System.Drawing.Size(221, 25);
            this.lbl_spieler1.TabIndex = 8;
            this.lbl_spieler1.Text = "Spieler";
            this.lbl_spieler1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_spieler1wp
            // 
            this.lbl_spieler1wp.AutoSize = true;
            this.lbl_spieler1wp.BackColor = System.Drawing.Color.Blue;
            this.lbl_spieler1wp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1wp.ForeColor = System.Drawing.Color.White;
            this.lbl_spieler1wp.Location = new System.Drawing.Point(12, 43);
            this.lbl_spieler1wp.Name = "lbl_spieler1wp";
            this.lbl_spieler1wp.Size = new System.Drawing.Size(40, 18);
            this.lbl_spieler1wp.TabIndex = 10;
            this.lbl_spieler1wp.Text = "WP:";
            // 
            // lbl_wp1
            // 
            this.lbl_wp1.AutoSize = true;
            this.lbl_wp1.BackColor = System.Drawing.Color.Blue;
            this.lbl_wp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_wp1.ForeColor = System.Drawing.Color.White;
            this.lbl_wp1.Location = new System.Drawing.Point(65, 45);
            this.lbl_wp1.Name = "lbl_wp1";
            this.lbl_wp1.Size = new System.Drawing.Size(31, 16);
            this.lbl_wp1.TabIndex = 11;
            this.lbl_wp1.Text = "100";
            // 
            // lbl_lp1
            // 
            this.lbl_lp1.AutoSize = true;
            this.lbl_lp1.BackColor = System.Drawing.Color.Blue;
            this.lbl_lp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lp1.ForeColor = System.Drawing.Color.White;
            this.lbl_lp1.Location = new System.Drawing.Point(65, 76);
            this.lbl_lp1.Name = "lbl_lp1";
            this.lbl_lp1.Size = new System.Drawing.Size(31, 16);
            this.lbl_lp1.TabIndex = 15;
            this.lbl_lp1.Text = "120";
            // 
            // lbl_spieler1lp
            // 
            this.lbl_spieler1lp.AutoSize = true;
            this.lbl_spieler1lp.BackColor = System.Drawing.Color.Blue;
            this.lbl_spieler1lp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1lp.ForeColor = System.Drawing.Color.White;
            this.lbl_spieler1lp.Location = new System.Drawing.Point(14, 74);
            this.lbl_spieler1lp.Name = "lbl_spieler1lp";
            this.lbl_spieler1lp.Size = new System.Drawing.Size(33, 18);
            this.lbl_spieler1lp.TabIndex = 16;
            this.lbl_spieler1lp.Text = "LP:";
            // 
            // lbl_gegnerlp
            // 
            this.lbl_gegnerlp.AutoSize = true;
            this.lbl_gegnerlp.BackColor = System.Drawing.Color.Green;
            this.lbl_gegnerlp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegnerlp.Location = new System.Drawing.Point(1199, 74);
            this.lbl_gegnerlp.Name = "lbl_gegnerlp";
            this.lbl_gegnerlp.Size = new System.Drawing.Size(33, 18);
            this.lbl_gegnerlp.TabIndex = 21;
            this.lbl_gegnerlp.Text = "LP:";
            // 
            // lbl_lp2
            // 
            this.lbl_lp2.AutoSize = true;
            this.lbl_lp2.BackColor = System.Drawing.Color.Green;
            this.lbl_lp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lp2.Location = new System.Drawing.Point(1250, 76);
            this.lbl_lp2.Name = "lbl_lp2";
            this.lbl_lp2.Size = new System.Drawing.Size(23, 16);
            this.lbl_lp2.TabIndex = 20;
            this.lbl_lp2.Text = "90";
            // 
            // lbl_wp2
            // 
            this.lbl_wp2.AutoSize = true;
            this.lbl_wp2.BackColor = System.Drawing.Color.Green;
            this.lbl_wp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_wp2.Location = new System.Drawing.Point(1250, 45);
            this.lbl_wp2.Name = "lbl_wp2";
            this.lbl_wp2.Size = new System.Drawing.Size(23, 16);
            this.lbl_wp2.TabIndex = 19;
            this.lbl_wp2.Text = "50";
            // 
            // lbl_gegnerwp
            // 
            this.lbl_gegnerwp.AutoSize = true;
            this.lbl_gegnerwp.BackColor = System.Drawing.Color.Green;
            this.lbl_gegnerwp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegnerwp.Location = new System.Drawing.Point(1197, 43);
            this.lbl_gegnerwp.Name = "lbl_gegnerwp";
            this.lbl_gegnerwp.Size = new System.Drawing.Size(40, 18);
            this.lbl_gegnerwp.TabIndex = 18;
            this.lbl_gegnerwp.Text = "WP:";
            // 
            // lbl_gegner2
            // 
            this.lbl_gegner2.BackColor = System.Drawing.Color.Green;
            this.lbl_gegner2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegner2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbl_gegner2.Location = new System.Drawing.Point(1078, 9);
            this.lbl_gegner2.Name = "lbl_gegner2";
            this.lbl_gegner2.Size = new System.Drawing.Size(217, 25);
            this.lbl_gegner2.TabIndex = 17;
            this.lbl_gegner2.Text = "Gegner";
            this.lbl_gegner2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_Schadenspieler1
            // 
            this.lbl_Schadenspieler1.AutoSize = true;
            this.lbl_Schadenspieler1.BackColor = System.Drawing.Color.Blue;
            this.lbl_Schadenspieler1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Schadenspieler1.ForeColor = System.Drawing.Color.White;
            this.lbl_Schadenspieler1.Location = new System.Drawing.Point(159, 43);
            this.lbl_Schadenspieler1.Name = "lbl_Schadenspieler1";
            this.lbl_Schadenspieler1.Size = new System.Drawing.Size(31, 16);
            this.lbl_Schadenspieler1.TabIndex = 22;
            this.lbl_Schadenspieler1.Text = "100";
            // 
            // lbl_schadengegner
            // 
            this.lbl_schadengegner.AutoSize = true;
            this.lbl_schadengegner.BackColor = System.Drawing.Color.Green;
            this.lbl_schadengegner.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_schadengegner.Location = new System.Drawing.Point(1099, 34);
            this.lbl_schadengegner.Name = "lbl_schadengegner";
            this.lbl_schadengegner.Size = new System.Drawing.Size(31, 16);
            this.lbl_schadengegner.TabIndex = 23;
            this.lbl_schadengegner.Text = "100";
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.Red;
            this.pictureBox9.Image = global::SP_Semesterprojekt.Properties.Resources.poke;
            this.pictureBox9.Location = new System.Drawing.Point(431, 464);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(432, 235);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 24;
            this.pictureBox9.TabStop = false;
            // 
            // player1
            // 
            this.player1.BackColor = System.Drawing.Color.Blue;
            this.player1.Location = new System.Drawing.Point(146, 387);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(39, 60);
            this.player1.TabIndex = 25;
            this.player1.TabStop = false;
            // 
            // player2
            // 
            this.player2.BackColor = System.Drawing.Color.Green;
            this.player2.Location = new System.Drawing.Point(1137, 387);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(39, 60);
            this.player2.TabIndex = 26;
            this.player2.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Blue;
            this.pictureBox10.Location = new System.Drawing.Point(-2, -1);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(282, 105);
            this.pictureBox10.TabIndex = 27;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Green;
            this.pictureBox11.Location = new System.Drawing.Point(1027, -1);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(277, 105);
            this.pictureBox11.TabIndex = 28;
            this.pictureBox11.TabStop = false;
            // 
            // gameTimer
            // 
            this.gameTimer.Interval = 2000;
            this.gameTimer.Tick += new System.EventHandler(this.MainGameTimerEvent);
            // 
            // lbl_timer
            // 
            this.lbl_timer.AutoSize = true;
            this.lbl_timer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_timer.Location = new System.Drawing.Point(628, 9);
            this.lbl_timer.Name = "lbl_timer";
            this.lbl_timer.Size = new System.Drawing.Size(55, 29);
            this.lbl_timer.TabIndex = 29;
            this.lbl_timer.Text = "300";
            // 
            // lbl_spieler1typ1
            // 
            this.lbl_spieler1typ1.AutoSize = true;
            this.lbl_spieler1typ1.BackColor = System.Drawing.Color.Blue;
            this.lbl_spieler1typ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1typ1.ForeColor = System.Drawing.Color.White;
            this.lbl_spieler1typ1.Location = new System.Drawing.Point(118, 76);
            this.lbl_spieler1typ1.Name = "lbl_spieler1typ1";
            this.lbl_spieler1typ1.Size = new System.Drawing.Size(31, 16);
            this.lbl_spieler1typ1.TabIndex = 30;
            this.lbl_spieler1typ1.Text = "100";
            // 
            // lbl_spieler1typ2
            // 
            this.lbl_spieler1typ2.AutoSize = true;
            this.lbl_spieler1typ2.BackColor = System.Drawing.Color.Blue;
            this.lbl_spieler1typ2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1typ2.ForeColor = System.Drawing.Color.White;
            this.lbl_spieler1typ2.Location = new System.Drawing.Point(206, 76);
            this.lbl_spieler1typ2.Name = "lbl_spieler1typ2";
            this.lbl_spieler1typ2.Size = new System.Drawing.Size(31, 16);
            this.lbl_spieler1typ2.TabIndex = 31;
            this.lbl_spieler1typ2.Text = "100";
            // 
            // lbl_gegnertyp1
            // 
            this.lbl_gegnertyp1.AutoSize = true;
            this.lbl_gegnertyp1.BackColor = System.Drawing.Color.Green;
            this.lbl_gegnertyp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegnertyp1.Location = new System.Drawing.Point(1043, 76);
            this.lbl_gegnertyp1.Name = "lbl_gegnertyp1";
            this.lbl_gegnertyp1.Size = new System.Drawing.Size(31, 16);
            this.lbl_gegnertyp1.TabIndex = 32;
            this.lbl_gegnertyp1.Text = "100";
            // 
            // lbl_gegnertyp2
            // 
            this.lbl_gegnertyp2.AutoSize = true;
            this.lbl_gegnertyp2.BackColor = System.Drawing.Color.Green;
            this.lbl_gegnertyp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegnertyp2.Location = new System.Drawing.Point(1134, 76);
            this.lbl_gegnertyp2.Name = "lbl_gegnertyp2";
            this.lbl_gegnertyp2.Size = new System.Drawing.Size(31, 16);
            this.lbl_gegnertyp2.TabIndex = 33;
            this.lbl_gegnertyp2.Text = "100";
            // 
            // frm_arena
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1316, 711);
            this.Controls.Add(this.lbl_gegnertyp2);
            this.Controls.Add(this.lbl_gegnertyp1);
            this.Controls.Add(this.lbl_spieler1typ2);
            this.Controls.Add(this.lbl_spieler1typ1);
            this.Controls.Add(this.lbl_timer);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.lbl_schadengegner);
            this.Controls.Add(this.lbl_Schadenspieler1);
            this.Controls.Add(this.lbl_gegnerlp);
            this.Controls.Add(this.lbl_lp2);
            this.Controls.Add(this.lbl_wp2);
            this.Controls.Add(this.lbl_gegnerwp);
            this.Controls.Add(this.lbl_gegner2);
            this.Controls.Add(this.lbl_spieler1lp);
            this.Controls.Add(this.lbl_lp1);
            this.Controls.Add(this.lbl_wp1);
            this.Controls.Add(this.lbl_spieler1wp);
            this.Controls.Add(this.lbl_spieler1);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Name = "frm_arena";
            this.Text = "frm_arena";
            this.Load += new System.EventHandler(this.frm_arena_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_arena_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frm_arena_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label lbl_spieler1;
        private System.Windows.Forms.Label lbl_spieler1wp;
        private System.Windows.Forms.Label lbl_wp1;
        private System.Windows.Forms.Label lbl_lp1;
        private System.Windows.Forms.Label lbl_spieler1lp;
        private System.Windows.Forms.Label lbl_gegnerlp;
        private System.Windows.Forms.Label lbl_lp2;
        private System.Windows.Forms.Label lbl_wp2;
        private System.Windows.Forms.Label lbl_gegnerwp;
        private System.Windows.Forms.Label lbl_gegner2;
        private System.Windows.Forms.Label lbl_Schadenspieler1;
        private System.Windows.Forms.Label lbl_schadengegner;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox player1;
        private System.Windows.Forms.PictureBox player2;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbl_timer;
        private System.Windows.Forms.Label lbl_spieler1typ1;
        private System.Windows.Forms.Label lbl_spieler1typ2;
        private System.Windows.Forms.Label lbl_gegnertyp1;
        private System.Windows.Forms.Label lbl_gegnertyp2;
    }
}