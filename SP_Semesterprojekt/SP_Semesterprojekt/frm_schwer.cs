﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_schwer : Form
    {
        //Variablen für das Quiz
        int correctAnswer;
        int questionNumber = 1;
        int score;
        int percentage;
        int totalQuestions;

        public frm_schwer()
        {
            InitializeComponent();
            askQuestion(questionNumber);
            totalQuestions = 10;
        }

        private void checkAnswerEvent(object sender, EventArgs e)
        {
            var senderObject = (Button)sender;
            int buttonTag = Convert.ToInt32(senderObject.Tag);

            if (buttonTag == correctAnswer)
            {
                score++;
            }
            if (questionNumber == totalQuestions)
            {
                // work out the percentage
                percentage = (int)Math.Round((double)(score * 100) / totalQuestions);
                MessageBox.Show(
                    "Fertig!" + Environment.NewLine +
                    "Du hast " + score + "/10 Fragen richtig beantwortet.");

                DialogResult = DialogResult.OK;
            }
            questionNumber++;
            askQuestion(questionNumber);
        }

        private void askQuestion(int qnum)
        {
            switch (qnum)
            {
                case 1:
                    pbx_pokemon.Image = Properties.Resources.Megaentwicklung;
                    lbl_frage.Text = "In welcher Region wurde die Mega-Entwicklung erstmals eingeführt?";
                    btn_antwort1.Text = "Kalos";
                    btn_antwort2.Text = "Johto";
                    btn_antwort3.Text = "Einall";
                    btn_antwort4.Text = "Galar";
                    correctAnswer = 1;
                    break;
                case 2:
                    pbx_pokemon.Image = Properties.Resources.bauz;
                    lbl_frage.Text = "Wie heißt der Pflanzenstarter in der Region Alola?";
                    btn_antwort1.Text = "Igamaro";
                    btn_antwort2.Text = "Bauz";
                    btn_antwort3.Text = "Chelast";
                    btn_antwort4.Text = "Endivie";
                    correctAnswer = 2;
                    break;
                case 3:
                    pbx_pokemon.Image = Properties.Resources.typen;
                    lbl_frage.Text = "Wie viele Pokemon Typen gibt es?";
                    btn_antwort1.Text = "12";
                    btn_antwort2.Text = "14";
                    btn_antwort3.Text = "16";
                    btn_antwort4.Text = "18";
                    correctAnswer = 4;
                    break;
                case 4:
                    pbx_pokemon.Image = Properties.Resources.karte;
                    lbl_frage.Text = "Aus wie vielen Pokemon besteht die Region Kanto?";
                    btn_antwort1.Text = "142";
                    btn_antwort2.Text = "145";
                    btn_antwort3.Text = "148";
                    btn_antwort4.Text = "151"; ;
                    correctAnswer = 4;
                    break;
                case 5:
                    pbx_pokemon.Image = Properties.Resources.hippo;
                    lbl_frage.Text = "Welchen Typ das Pokemon Hippoterus?";
                    btn_antwort1.Text = "Gestein";
                    btn_antwort2.Text = "Stahl";
                    btn_antwort3.Text = "Boden";
                    btn_antwort4.Text = "Feuer";
                    correctAnswer = 3;
                    break;
                case 6:
                    pbx_pokemon.Image = Properties.Resources.Klicklack;
                    lbl_frage.Text = "Wie heißt dieses Pokemon?";
                    btn_antwort1.Text = "Bronzel";
                    btn_antwort2.Text = "KliKlak";
                    btn_antwort3.Text = "Flunkifer";
                    btn_antwort4.Text = "Magneton";
                    correctAnswer = 2;
                    break;
                case 7:
                    pbx_pokemon.Image = Properties.Resources.go;
                    lbl_frage.Text = "Wer ist Ashs bester Freund in der Galar Region?";
                    btn_antwort1.Text = "Rocko";
                    btn_antwort2.Text = "Benni";
                    btn_antwort3.Text = "Go";
                    btn_antwort4.Text = "Michael";
                    correctAnswer = 3;
                    break;
                case 8:
                    pbx_pokemon.Image = Properties.Resources.arceus;
                    lbl_frage.Text = "Welches Pokemon gilt als Erschaffer des Pokemon Universum?";
                    btn_antwort1.Text = "Giratina";
                    btn_antwort2.Text = "Palkia";
                    btn_antwort3.Text = "Arceus";
                    btn_antwort4.Text = "Dialga";
                    correctAnswer = 3;
                    break;

                case 9:
                    pbx_pokemon.Image = Properties.Resources.brutalanda;
                    lbl_frage.Text = "Wie heißt man die erste Entwicklung von Brutalanda?";
                    btn_antwort1.Text = "Kindwurm";
                    btn_antwort2.Text = "Larvitar";
                    btn_antwort3.Text = "Knacklion";
                    btn_antwort4.Text = "Dratini";
                    correctAnswer = 1;
                    break;

                case 10:
                    pbx_pokemon.Image = Properties.Resources.quajutsu;
                    lbl_frage.Text = "Welches Pokemon hat die Typen Unlicht und Wasser?";
                    btn_antwort1.Text = "Garados";
                    btn_antwort2.Text = "Sumpex";
                    btn_antwort3.Text = "Quajutsu";
                    btn_antwort4.Text = "Turtok";
                    correctAnswer = 3;
                    break;

            }
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
