﻿namespace SP_Semesterprojekt
{
    partial class frm_pokemonerstellen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_name = new System.Windows.Forms.TextBox();
            this.lbl_typ2 = new System.Windows.Forms.Label();
            this.lbl_typ1 = new System.Windows.Forms.Label();
            this.com_typ1 = new System.Windows.Forms.ComboBox();
            this.com_typ2 = new System.Windows.Forms.ComboBox();
            this.lbl_lebenspunkte = new System.Windows.Forms.Label();
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_angriffspunkte = new System.Windows.Forms.Label();
            this.lbl_generation = new System.Windows.Forms.Label();
            this.btn_eintragen = new System.Windows.Forms.Button();
            this.lbl_ueberschrift = new System.Windows.Forms.Label();
            this.num_angriff = new System.Windows.Forms.NumericUpDown();
            this.lbl_spectra = new System.Windows.Forms.Label();
            this.num_lebenspunkte = new System.Windows.Forms.NumericUpDown();
            this.btn_randompokemon = new System.Windows.Forms.Button();
            this.btn_abbrechen = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_angriff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_lebenspunkte)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(227, 85);
            this.txt_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(141, 22);
            this.txt_name.TabIndex = 0;
            // 
            // lbl_typ2
            // 
            this.lbl_typ2.AutoSize = true;
            this.lbl_typ2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_typ2.Location = new System.Drawing.Point(16, 178);
            this.lbl_typ2.Name = "lbl_typ2";
            this.lbl_typ2.Size = new System.Drawing.Size(74, 25);
            this.lbl_typ2.TabIndex = 1;
            this.lbl_typ2.Text = "Typ 2:";
            // 
            // lbl_typ1
            // 
            this.lbl_typ1.AutoSize = true;
            this.lbl_typ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_typ1.Location = new System.Drawing.Point(13, 129);
            this.lbl_typ1.Name = "lbl_typ1";
            this.lbl_typ1.Size = new System.Drawing.Size(74, 25);
            this.lbl_typ1.TabIndex = 2;
            this.lbl_typ1.Text = "Typ 1:";
            // 
            // com_typ1
            // 
            this.com_typ1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.com_typ1.FormattingEnabled = true;
            this.com_typ1.Items.AddRange(new object[] {
            "Boden",
            "Drache",
            "Eis",
            "Elektro",
            "Fee",
            "Feuer",
            "Flug",
            "Geist",
            "Gestein",
            "Gift",
            "Kampf",
            "Käfer",
            "Normal",
            "Pflanze",
            "Psycho",
            "Stahl",
            "Unlicht",
            "Wasser"});
            this.com_typ1.Location = new System.Drawing.Point(227, 133);
            this.com_typ1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.com_typ1.Name = "com_typ1";
            this.com_typ1.Size = new System.Drawing.Size(141, 24);
            this.com_typ1.TabIndex = 4;
            this.com_typ1.SelectedIndexChanged += new System.EventHandler(this.com_typ1_SelectedIndexChanged);
            // 
            // com_typ2
            // 
            this.com_typ2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.com_typ2.FormattingEnabled = true;
            this.com_typ2.Items.AddRange(new object[] {
            "",
            "Boden",
            "Drache",
            "Eis",
            "Elektro",
            "Fee",
            "Feuer",
            "Flug",
            "Gestein",
            "Gift",
            "Kampf",
            "Käfer",
            "Normal",
            "Pflanze",
            "Psycho",
            "Stahl",
            "Unlicht",
            "Wasser",
            "Geist"});
            this.com_typ2.Location = new System.Drawing.Point(227, 178);
            this.com_typ2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.com_typ2.Name = "com_typ2";
            this.com_typ2.Size = new System.Drawing.Size(141, 24);
            this.com_typ2.TabIndex = 5;
            // 
            // lbl_lebenspunkte
            // 
            this.lbl_lebenspunkte.AutoSize = true;
            this.lbl_lebenspunkte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lebenspunkte.Location = new System.Drawing.Point(16, 277);
            this.lbl_lebenspunkte.Name = "lbl_lebenspunkte";
            this.lbl_lebenspunkte.Size = new System.Drawing.Size(155, 25);
            this.lbl_lebenspunkte.TabIndex = 6;
            this.lbl_lebenspunkte.Text = "Lebenspunkte:";
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.Location = new System.Drawing.Point(13, 82);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(75, 25);
            this.lbl_name.TabIndex = 7;
            this.lbl_name.Text = "Name:";
            // 
            // lbl_angriffspunkte
            // 
            this.lbl_angriffspunkte.AutoSize = true;
            this.lbl_angriffspunkte.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_angriffspunkte.Location = new System.Drawing.Point(16, 223);
            this.lbl_angriffspunkte.Name = "lbl_angriffspunkte";
            this.lbl_angriffspunkte.Size = new System.Drawing.Size(158, 25);
            this.lbl_angriffspunkte.TabIndex = 8;
            this.lbl_angriffspunkte.Text = "Angriffspunkte:";
            // 
            // lbl_generation
            // 
            this.lbl_generation.AutoSize = true;
            this.lbl_generation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_generation.Location = new System.Drawing.Point(16, 330);
            this.lbl_generation.Name = "lbl_generation";
            this.lbl_generation.Size = new System.Drawing.Size(125, 25);
            this.lbl_generation.TabIndex = 9;
            this.lbl_generation.Text = "Generation:";
            // 
            // btn_eintragen
            // 
            this.btn_eintragen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_eintragen.Location = new System.Drawing.Point(8, 377);
            this.btn_eintragen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_eintragen.Name = "btn_eintragen";
            this.btn_eintragen.Size = new System.Drawing.Size(140, 50);
            this.btn_eintragen.TabIndex = 10;
            this.btn_eintragen.Text = "Eintragen der Werte";
            this.btn_eintragen.UseVisualStyleBackColor = true;
            this.btn_eintragen.Click += new System.EventHandler(this.btn_eintragen_Click);
            // 
            // lbl_ueberschrift
            // 
            this.lbl_ueberschrift.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ueberschrift.Location = new System.Drawing.Point(3, 9);
            this.lbl_ueberschrift.Name = "lbl_ueberschrift";
            this.lbl_ueberschrift.Size = new System.Drawing.Size(395, 60);
            this.lbl_ueberschrift.TabIndex = 11;
            this.lbl_ueberschrift.Text = "Erstelle dein eigenes Pokemon";
            this.lbl_ueberschrift.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // num_angriff
            // 
            this.num_angriff.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.num_angriff.Location = new System.Drawing.Point(227, 228);
            this.num_angriff.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num_angriff.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.num_angriff.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.num_angriff.Name = "num_angriff";
            this.num_angriff.Size = new System.Drawing.Size(141, 22);
            this.num_angriff.TabIndex = 12;
            this.num_angriff.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lbl_spectra
            // 
            this.lbl_spectra.AutoSize = true;
            this.lbl_spectra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spectra.Location = new System.Drawing.Point(245, 330);
            this.lbl_spectra.Name = "lbl_spectra";
            this.lbl_spectra.Size = new System.Drawing.Size(87, 25);
            this.lbl_spectra.TabIndex = 15;
            this.lbl_spectra.Text = "Spectra";
            // 
            // num_lebenspunkte
            // 
            this.num_lebenspunkte.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.num_lebenspunkte.Location = new System.Drawing.Point(227, 282);
            this.num_lebenspunkte.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.num_lebenspunkte.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.num_lebenspunkte.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.num_lebenspunkte.Name = "num_lebenspunkte";
            this.num_lebenspunkte.Size = new System.Drawing.Size(141, 22);
            this.num_lebenspunkte.TabIndex = 16;
            this.num_lebenspunkte.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btn_randompokemon
            // 
            this.btn_randompokemon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_randompokemon.Location = new System.Drawing.Point(258, 377);
            this.btn_randompokemon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_randompokemon.Name = "btn_randompokemon";
            this.btn_randompokemon.Size = new System.Drawing.Size(140, 50);
            this.btn_randompokemon.TabIndex = 17;
            this.btn_randompokemon.Text = "Random Pokemon generieren";
            this.btn_randompokemon.UseVisualStyleBackColor = true;
            this.btn_randompokemon.Click += new System.EventHandler(this.btn_randompokemon_Click);
            // 
            // btn_abbrechen
            // 
            this.btn_abbrechen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_abbrechen.Location = new System.Drawing.Point(153, 384);
            this.btn_abbrechen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_abbrechen.Name = "btn_abbrechen";
            this.btn_abbrechen.Size = new System.Drawing.Size(99, 37);
            this.btn_abbrechen.TabIndex = 18;
            this.btn_abbrechen.Text = "Abbrechen";
            this.btn_abbrechen.UseVisualStyleBackColor = true;
            this.btn_abbrechen.Click += new System.EventHandler(this.btn_abbrechen_Click);
            // 
            // frm_pokemonerstellen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(402, 441);
            this.Controls.Add(this.btn_abbrechen);
            this.Controls.Add(this.btn_randompokemon);
            this.Controls.Add(this.num_lebenspunkte);
            this.Controls.Add(this.lbl_spectra);
            this.Controls.Add(this.num_angriff);
            this.Controls.Add(this.lbl_ueberschrift);
            this.Controls.Add(this.btn_eintragen);
            this.Controls.Add(this.lbl_generation);
            this.Controls.Add(this.lbl_angriffspunkte);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.lbl_lebenspunkte);
            this.Controls.Add(this.com_typ2);
            this.Controls.Add(this.com_typ1);
            this.Controls.Add(this.lbl_typ1);
            this.Controls.Add(this.lbl_typ2);
            this.Controls.Add(this.txt_name);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm_pokemonerstellen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Neues Pokemon erfinden";
            ((System.ComponentModel.ISupportInitialize)(this.num_angriff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_lebenspunkte)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label lbl_typ2;
        private System.Windows.Forms.Label lbl_typ1;
        private System.Windows.Forms.ComboBox com_typ1;
        private System.Windows.Forms.ComboBox com_typ2;
        private System.Windows.Forms.Label lbl_lebenspunkte;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_angriffspunkte;
        private System.Windows.Forms.Label lbl_generation;
        private System.Windows.Forms.Button btn_eintragen;
        private System.Windows.Forms.Label lbl_ueberschrift;
        private System.Windows.Forms.NumericUpDown num_angriff;
        private System.Windows.Forms.Label lbl_spectra;
        private System.Windows.Forms.NumericUpDown num_lebenspunkte;
        private System.Windows.Forms.Button btn_randompokemon;
        private System.Windows.Forms.Button btn_abbrechen;
    }
}