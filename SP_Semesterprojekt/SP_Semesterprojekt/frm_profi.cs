﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_profi : Form
    {
        //Variablen für das Quiz
        int correctAnswer;
        int questionNumber = 1;
        int score;
        int totalQuestions;
        public frm_profi()
        {
            InitializeComponent();
            askQuestion(questionNumber);
            totalQuestions = 10;
        }

        private void checkAnswerEvent(object sender, EventArgs e)
        {
            var senderObject = (Button)sender;
            int buttonTag = Convert.ToInt32(senderObject.Tag);

            if (buttonTag == correctAnswer)
            {
                score++;
            }
            if (questionNumber == totalQuestions)
            {
                MessageBox.Show(
                    "Fertig!" + Environment.NewLine +
                    "Du hast " + score + "/10 Fragen richtig beantwortet.");

                DialogResult = DialogResult.OK;
            }
            questionNumber++;
            askQuestion(questionNumber);
        }

        private void askQuestion(int qnum)
        {
            switch (qnum)
            {
                case 1:
                    pbx_pokemon.Image = Properties.Resources.kukui;
                    lbl_frage.Text = "Wie heißt der Pokemon Professor in der Region Alola?";
                    btn_antwort1.Text = "Birk";
                    btn_antwort2.Text = "Esche";
                    btn_antwort3.Text = "Rowan";
                    btn_antwort4.Text = "Kukui";
                    correctAnswer = 4;
                    break;
                case 2:
                    pbx_pokemon.Image = Properties.Resources.mega;
                    lbl_frage.Text = "Welches Pokemon besitzt keine Megaentwicklung?";
                    btn_antwort1.Text = "Nidoking";
                    btn_antwort2.Text = "Pinsir";
                    btn_antwort3.Text = "Schherox";
                    btn_antwort4.Text = "Bibor";
                    correctAnswer = 1;
                    break;
                case 3:
                    pbx_pokemon.Image = Properties.Resources.kalos;
                    lbl_frage.Text = "Welcher Typ ist in der Region Kalos eingeführt worden?";
                    btn_antwort1.Text = "Stahl";
                    btn_antwort2.Text = "Eis";
                    btn_antwort3.Text = "Fee";
                    btn_antwort4.Text = "Unlicht";
                    correctAnswer = 3;
                    break;
                case 4:
                    pbx_pokemon.Image = Properties.Resources.alle;
                    lbl_frage.Text = "Welches Pokémon hat die Fähigkeit, sich in jedes andere Pokémon zu verwandeln?";
                    btn_antwort1.Text = "Ditto";
                    btn_antwort2.Text = "Mew";
                    btn_antwort3.Text = "Zoroak";
                    btn_antwort4.Text = "Smeargle"; ;
                    correctAnswer = 1;
                    break;
                case 5:
                    pbx_pokemon.Image = Properties.Resources.wulaosu;
                    lbl_frage.Text = "Welches Pokemon ist das?";
                    btn_antwort1.Text = "Zarude";
                    btn_antwort2.Text = "Lextremo";
                    btn_antwort3.Text = "Wulaosu";
                    btn_antwort4.Text = "Tengulist";
                    correctAnswer = 3;
                    break;
                case 6:
                    pbx_pokemon.Image = Properties.Resources.meloetta;
                    lbl_frage.Text = "Welches Pokémon kann durch Singen die Schwerkraft aufheben und in der Luft schweben?";
                    btn_antwort1.Text = "Meloetta";
                    btn_antwort2.Text = "Gravitynote";
                    btn_antwort3.Text = "Levitune";
                    btn_antwort4.Text = "Sonarise";
                    correctAnswer = 1;
                    break;
                case 7:
                    pbx_pokemon.Image = Properties.Resources.ultra;
                    lbl_frage.Text = "Welches Pokemon ist keine Ultrabestie?";
                    btn_antwort1.Text = "Nihilego";
                    btn_antwort2.Text = "Guzzlord";
                    btn_antwort3.Text = "Zygarde";
                    btn_antwort4.Text = "Celesteela";
                    correctAnswer = 3;
                    break;
                case 8:
                    pbx_pokemon.Image = Properties.Resources.chore;
                    lbl_frage.Text = "Wie viele verschiedene Formen kann sich das Pokémon Choreogel annehmen?";
                    btn_antwort1.Text = "2";
                    btn_antwort2.Text = "3";
                    btn_antwort3.Text = "4";
                    btn_antwort4.Text = "5";
                    correctAnswer = 3;
                    break;

                case 9:
                    pbx_pokemon.Image = Properties.Resources.Megaentwicklung;
                    lbl_frage.Text = "Wie viele verschiedene Mega-Entwicklungen gibt es insgesamt bis zur siebten Generation?";
                    btn_antwort1.Text = "36";
                    btn_antwort2.Text = "32";
                    btn_antwort3.Text = "28";
                    btn_antwort4.Text = "24";
                    correctAnswer = 2;
                    break;

                case 10:
                    pbx_pokemon.Image = Properties.Resources.giga;
                    lbl_frage.Text = "Wie viele Pokémon können sich in der Galar-Region dynamaximieren?";
                    btn_antwort1.Text = "12";
                    btn_antwort2.Text = "18";
                    btn_antwort3.Text = "24";
                    btn_antwort4.Text = "Alle";
                    correctAnswer = 3;
                    break;

            }
        }
        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        
    }
}
