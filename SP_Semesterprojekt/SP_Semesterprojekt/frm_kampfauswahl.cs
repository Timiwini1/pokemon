﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_kampfauswahl : Form
    {
        List<cls_pokemon> spieler1= new List<cls_pokemon>();
        List<cls_pokemon> spieler2 = new List<cls_pokemon>();
        List<cls_pokemon> pokemonliste = new List<cls_pokemon>();

        public frm_kampfauswahl()
        {
            InitializeComponent();
            lbx_gegner.DisplayMember = "Ausgabe";
            lbx_spieler1.DisplayMember = "Ausgabe";
        }

        private void frm_kampfauswahl_Load(object sender, EventArgs e)
        {
            pokemonliste.Clear();
            lbx_spieler1.Items.Clear();
            lbx_gegner.Items.Clear();

            clsDataProvider.LoadData(pokemonliste);

            foreach (cls_pokemon pokemon in pokemonliste)
            {
                lbx_spieler1.Items.Add(pokemon);
                lbx_gegner.Items.Add(pokemon);
            }
        }

        private void btn_hauptmenue_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btn_arena_Click(object sender, EventArgs e)
        {
            if(spieler1.Count > 0 && spieler2.Count > 0)
            {
                frm_arena a = new frm_arena(spieler1, spieler2);
                a.ShowDialog();
            }
            else
            {
                MessageBox.Show("Beide Spieler müssen ein Pokemon ausgewählt haben!", "Fehler bei der Auswahl", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txt_spieler1_TextChanged(object sender, EventArgs e)
        {
            string spielerpokemon = txt_spieler1.Text.ToLower(); //kleinbuchstaben

            lbx_spieler1.Items.Clear();

            foreach (cls_pokemon pokemon in pokemonliste)
            {
                if (pokemon.Name.ToLower().StartsWith(spielerpokemon)) //filter
                {
                    lbx_spieler1.Items.Add(pokemon);
                }
            }
        }

        private void txt_spieler2_TextChanged(object sender, EventArgs e)
        {
            string gegnerpokemon = txt_gegner.Text.ToLower(); // Kleinbuchstaben
            lbx_gegner.Items.Clear();

            foreach (cls_pokemon pokemon in pokemonliste)
            {
                if (pokemon.Name.ToLower().StartsWith(gegnerpokemon)) //filter
                {
                    lbx_gegner.Items.Add(pokemon);
                }
            }
        }

        private void btn_wechselnspieler1_Click(object sender, EventArgs e)
        {
            spieler1.Clear();
            lbx_spieler1.Enabled = true;
            txt_spieler1.Enabled = true;
            lbx_spieler1.ClearSelected();
            txt_spieler1.Clear();
            btn_wechselnspieler1.Visible = false;
            btn_randomspieler1.Visible = true;

        }

        private void btn_wechselnspieler2_Click(object sender, EventArgs e)
        {
            spieler2.Clear();
            lbx_gegner.Enabled = true;
            txt_gegner.Enabled = true;
            lbx_gegner.ClearSelected();
            txt_gegner.Clear();
            btn_wechselngegner.Visible = false;
            btn_randomgegner.Visible = true;

        }

        private void btn_randomspieler1_Click(object sender, EventArgs e)
        {
            if (lbx_spieler1.Items.Count > 0)
            {
                Random r = new Random();
                int zahl1 = r.Next(0, lbx_spieler1.Items.Count);
                lbx_spieler1.SelectedIndex = zahl1;

                cls_pokemon selectedPokemon = lbx_spieler1.SelectedItem as cls_pokemon;
                spieler1.Add(selectedPokemon);
                MessageBox.Show("Ein zufälliges Pokemon wurde ausgewählt!", "Erfolgreiche Auswahl Spieler 1", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lbx_spieler1.Enabled = false;
                txt_spieler1.Enabled = false;
                btn_randomspieler1.Visible = false;
                btn_wechselnspieler1.Visible = true;

            }
            else
            {
                MessageBox.Show("Die Liste der Gegner ist leer.", "Fehler bei der Auswahl Spieler 1", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_randomspieler2_Click(object sender, EventArgs e)
        {
            if (lbx_gegner.Items.Count > 0)
            {
                Random r = new Random();
                int zahl2 = r.Next(0, lbx_gegner.Items.Count);
                lbx_gegner.SelectedIndex = zahl2;

                cls_pokemon selectedPokemon = lbx_gegner.SelectedItem as cls_pokemon;
                spieler2.Add(selectedPokemon);
                MessageBox.Show("Ein zufälliges Pokemon wurde ausgewählt!", "Erfolgreiche Auswahl Spieler 2", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lbx_gegner.Enabled = false;
                txt_gegner.Enabled = false;
                btn_randomgegner.Visible = false;
                btn_wechselngegner.Visible = true;

            }
            else
            {
                MessageBox.Show("Die Liste der Gegner ist leer.", "Fehler bei der Auswahl Spieler 2", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void lbx_spieler1_DoubleClick(object sender, EventArgs e)
        {
            if (lbx_spieler1.SelectedItem != null)
            {
                cls_pokemon selectedPokemon = lbx_spieler1.SelectedItem as cls_pokemon;
                spieler1.Add(selectedPokemon);
                MessageBox.Show("Dein Pokemon wurde ausgewählt!", "Erfolgreiche Auswahl Spieler 1", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lbx_spieler1.Enabled = false;
                txt_spieler1.Enabled = false;
                btn_wechselnspieler1.Visible = true;
                btn_randomspieler1.Visible=false;

            }
            else
            {
                MessageBox.Show("Du hast kein Pokemon ausgewählt.", "Fehler bei der Auswahl Spieler 1", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lbx_spieler2_DoubleClick(object sender, EventArgs e)
        {
            if (lbx_gegner.SelectedItem != null)
            {
                cls_pokemon selectedPokemon = lbx_gegner.SelectedItem as cls_pokemon;
                spieler2.Add(selectedPokemon);
                MessageBox.Show("Das Pokemon deines Gegners wurde ausgewählt!", "Erfolgreiche Auswahl Spieler 2", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lbx_gegner.Enabled = false;
                txt_gegner.Enabled = false;
                btn_wechselngegner.Visible = true;
                btn_randomgegner.Visible=false;
            }
            else
            {
                MessageBox.Show("Du hast kein Pokemon ausgewählt.", "Fehler bei der Auswahl Spieler 2", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

