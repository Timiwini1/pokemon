﻿namespace SP_Semesterprojekt
{
    partial class frm_kampfauswahl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbx_spieler1 = new System.Windows.Forms.ListBox();
            this.lbl_spieler1ü = new System.Windows.Forms.Label();
            this.txt_spieler1 = new System.Windows.Forms.TextBox();
            this.lbl_spieler1 = new System.Windows.Forms.Label();
            this.lbl_gegner = new System.Windows.Forms.Label();
            this.txt_gegner = new System.Windows.Forms.TextBox();
            this.lbl_gegnerü = new System.Windows.Forms.Label();
            this.lbx_gegner = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_wechselngegner = new System.Windows.Forms.Button();
            this.btn_randomgegner = new System.Windows.Forms.Button();
            this.btn_wechselnspieler1 = new System.Windows.Forms.Button();
            this.btn_hauptmenue = new System.Windows.Forms.Button();
            this.btn_arena = new System.Windows.Forms.Button();
            this.btn_randomspieler1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbx_spieler1
            // 
            this.lbx_spieler1.FormattingEnabled = true;
            this.lbx_spieler1.ItemHeight = 16;
            this.lbx_spieler1.Location = new System.Drawing.Point(29, 227);
            this.lbx_spieler1.Name = "lbx_spieler1";
            this.lbx_spieler1.Size = new System.Drawing.Size(496, 276);
            this.lbx_spieler1.TabIndex = 1;
            this.lbx_spieler1.DoubleClick += new System.EventHandler(this.lbx_spieler1_DoubleClick);
            // 
            // lbl_spieler1ü
            // 
            this.lbl_spieler1ü.AutoSize = true;
            this.lbl_spieler1ü.Font = new System.Drawing.Font("MS PGothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1ü.Location = new System.Drawing.Point(87, 128);
            this.lbl_spieler1ü.Name = "lbl_spieler1ü";
            this.lbl_spieler1ü.Size = new System.Drawing.Size(401, 28);
            this.lbl_spieler1ü.TabIndex = 2;
            this.lbl_spieler1ü.Text = "Wähle dein Pokemon Spieler 1!";
            // 
            // txt_spieler1
            // 
            this.txt_spieler1.Location = new System.Drawing.Point(155, 193);
            this.txt_spieler1.Name = "txt_spieler1";
            this.txt_spieler1.Size = new System.Drawing.Size(207, 22);
            this.txt_spieler1.TabIndex = 3;
            this.txt_spieler1.TextChanged += new System.EventHandler(this.txt_spieler1_TextChanged);
            // 
            // lbl_spieler1
            // 
            this.lbl_spieler1.AutoSize = true;
            this.lbl_spieler1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_spieler1.Location = new System.Drawing.Point(54, 193);
            this.lbl_spieler1.Name = "lbl_spieler1";
            this.lbl_spieler1.Size = new System.Drawing.Size(63, 20);
            this.lbl_spieler1.TabIndex = 4;
            this.lbl_spieler1.Text = "Name:";
            // 
            // lbl_gegner
            // 
            this.lbl_gegner.AutoSize = true;
            this.lbl_gegner.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegner.Location = new System.Drawing.Point(634, 193);
            this.lbl_gegner.Name = "lbl_gegner";
            this.lbl_gegner.Size = new System.Drawing.Size(63, 20);
            this.lbl_gegner.TabIndex = 8;
            this.lbl_gegner.Text = "Name:";
            // 
            // txt_gegner
            // 
            this.txt_gegner.Location = new System.Drawing.Point(735, 193);
            this.txt_gegner.Name = "txt_gegner";
            this.txt_gegner.Size = new System.Drawing.Size(227, 22);
            this.txt_gegner.TabIndex = 7;
            this.txt_gegner.TextChanged += new System.EventHandler(this.txt_spieler2_TextChanged);
            // 
            // lbl_gegnerü
            // 
            this.lbl_gegnerü.Font = new System.Drawing.Font("MS PGothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_gegnerü.Location = new System.Drawing.Point(651, 115);
            this.lbl_gegnerü.Name = "lbl_gegnerü";
            this.lbl_gegnerü.Size = new System.Drawing.Size(468, 75);
            this.lbl_gegnerü.TabIndex = 6;
            this.lbl_gegnerü.Text = "Wähle das Pokemon für deinen Gegner!";
            this.lbl_gegnerü.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbx_gegner
            // 
            this.lbx_gegner.FormattingEnabled = true;
            this.lbx_gegner.ItemHeight = 16;
            this.lbx_gegner.Location = new System.Drawing.Point(623, 227);
            this.lbx_gegner.Name = "lbx_gegner";
            this.lbx_gegner.Size = new System.Drawing.Size(496, 276);
            this.lbx_gegner.TabIndex = 5;
            this.lbx_gegner.DoubleClick += new System.EventHandler(this.lbx_spieler2_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS PGothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(322, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(510, 28);
            this.label1.TabIndex = 9;
            this.label1.Text = "Willkommen bei den Pokemon Kämpfen!";
            // 
            // btn_wechselngegner
            // 
            this.btn_wechselngegner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_wechselngegner.Location = new System.Drawing.Point(791, 618);
            this.btn_wechselngegner.Name = "btn_wechselngegner";
            this.btn_wechselngegner.Size = new System.Drawing.Size(193, 64);
            this.btn_wechselngegner.TabIndex = 10;
            this.btn_wechselngegner.Text = "Pokemon wechseln";
            this.btn_wechselngegner.UseVisualStyleBackColor = true;
            this.btn_wechselngegner.Visible = false;
            this.btn_wechselngegner.Click += new System.EventHandler(this.btn_wechselnspieler2_Click);
            // 
            // btn_randomgegner
            // 
            this.btn_randomgegner.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_randomgegner.Location = new System.Drawing.Point(791, 519);
            this.btn_randomgegner.Name = "btn_randomgegner";
            this.btn_randomgegner.Size = new System.Drawing.Size(193, 64);
            this.btn_randomgegner.TabIndex = 11;
            this.btn_randomgegner.Text = "Zufälliges Pokemon wählen";
            this.btn_randomgegner.UseVisualStyleBackColor = true;
            this.btn_randomgegner.Click += new System.EventHandler(this.btn_randomspieler2_Click);
            // 
            // btn_wechselnspieler1
            // 
            this.btn_wechselnspieler1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_wechselnspieler1.Location = new System.Drawing.Point(155, 618);
            this.btn_wechselnspieler1.Name = "btn_wechselnspieler1";
            this.btn_wechselnspieler1.Size = new System.Drawing.Size(193, 64);
            this.btn_wechselnspieler1.TabIndex = 12;
            this.btn_wechselnspieler1.Text = "Pokemon wechseln";
            this.btn_wechselnspieler1.UseVisualStyleBackColor = true;
            this.btn_wechselnspieler1.Visible = false;
            this.btn_wechselnspieler1.Click += new System.EventHandler(this.btn_wechselnspieler1_Click);
            // 
            // btn_hauptmenue
            // 
            this.btn_hauptmenue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_hauptmenue.Location = new System.Drawing.Point(502, 533);
            this.btn_hauptmenue.Name = "btn_hauptmenue";
            this.btn_hauptmenue.Size = new System.Drawing.Size(158, 64);
            this.btn_hauptmenue.TabIndex = 13;
            this.btn_hauptmenue.Text = "Zurück zum Hauptmenü";
            this.btn_hauptmenue.UseVisualStyleBackColor = true;
            this.btn_hauptmenue.Click += new System.EventHandler(this.btn_hauptmenue_Click);
            // 
            // btn_arena
            // 
            this.btn_arena.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_arena.Location = new System.Drawing.Point(481, 618);
            this.btn_arena.Name = "btn_arena";
            this.btn_arena.Size = new System.Drawing.Size(193, 64);
            this.btn_arena.TabIndex = 14;
            this.btn_arena.Text = "Weiter zur Arena";
            this.btn_arena.UseVisualStyleBackColor = true;
            this.btn_arena.Click += new System.EventHandler(this.btn_arena_Click);
            // 
            // btn_randomspieler1
            // 
            this.btn_randomspieler1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_randomspieler1.Location = new System.Drawing.Point(155, 519);
            this.btn_randomspieler1.Name = "btn_randomspieler1";
            this.btn_randomspieler1.Size = new System.Drawing.Size(193, 64);
            this.btn_randomspieler1.TabIndex = 15;
            this.btn_randomspieler1.Text = "Zufälliges Pokemon wählen";
            this.btn_randomspieler1.UseVisualStyleBackColor = true;
            this.btn_randomspieler1.Click += new System.EventHandler(this.btn_randomspieler1_Click);
            // 
            // frm_kampfauswahl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1156, 694);
            this.Controls.Add(this.btn_randomspieler1);
            this.Controls.Add(this.btn_arena);
            this.Controls.Add(this.btn_hauptmenue);
            this.Controls.Add(this.btn_wechselnspieler1);
            this.Controls.Add(this.btn_randomgegner);
            this.Controls.Add(this.btn_wechselngegner);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_gegner);
            this.Controls.Add(this.txt_gegner);
            this.Controls.Add(this.lbl_gegnerü);
            this.Controls.Add(this.lbx_gegner);
            this.Controls.Add(this.lbl_spieler1);
            this.Controls.Add(this.txt_spieler1);
            this.Controls.Add(this.lbl_spieler1ü);
            this.Controls.Add(this.lbx_spieler1);
            this.Name = "frm_kampfauswahl";
            this.Text = "Auswahl der Pokemon";
            this.Load += new System.EventHandler(this.frm_kampfauswahl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lbx_spieler1;
        private System.Windows.Forms.Label lbl_spieler1ü;
        private System.Windows.Forms.TextBox txt_spieler1;
        private System.Windows.Forms.Label lbl_spieler1;
        private System.Windows.Forms.Label lbl_gegner;
        private System.Windows.Forms.TextBox txt_gegner;
        private System.Windows.Forms.Label lbl_gegnerü;
        private System.Windows.Forms.ListBox lbx_gegner;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_wechselngegner;
        private System.Windows.Forms.Button btn_randomgegner;
        private System.Windows.Forms.Button btn_wechselnspieler1;
        private System.Windows.Forms.Button btn_hauptmenue;
        private System.Windows.Forms.Button btn_arena;
        private System.Windows.Forms.Button btn_randomspieler1;
    }
}