﻿namespace SP_Semesterprojekt
{
    partial class frm_hauptmenü
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_hauptmenü));
            this.label1 = new System.Windows.Forms.Label();
            this.pbx_quiz = new System.Windows.Forms.PictureBox();
            this.pbx_kampf = new System.Windows.Forms.PictureBox();
            this.pbx_neu = new System.Windows.Forms.PictureBox();
            this.pbx_suchen = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_quiz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_kampf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_neu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_suchen)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(517, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Willkommen bei Pokemon!";
            // 
            // pbx_quiz
            // 
            this.pbx_quiz.Image = ((System.Drawing.Image)(resources.GetObject("pbx_quiz.Image")));
            this.pbx_quiz.Location = new System.Drawing.Point(72, 101);
            this.pbx_quiz.Margin = new System.Windows.Forms.Padding(4);
            this.pbx_quiz.Name = "pbx_quiz";
            this.pbx_quiz.Size = new System.Drawing.Size(212, 123);
            this.pbx_quiz.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_quiz.TabIndex = 7;
            this.pbx_quiz.TabStop = false;
            this.pbx_quiz.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pbx_kampf
            // 
            this.pbx_kampf.Image = ((System.Drawing.Image)(resources.GetObject("pbx_kampf.Image")));
            this.pbx_kampf.Location = new System.Drawing.Point(368, 101);
            this.pbx_kampf.Margin = new System.Windows.Forms.Padding(4);
            this.pbx_kampf.Name = "pbx_kampf";
            this.pbx_kampf.Size = new System.Drawing.Size(212, 123);
            this.pbx_kampf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_kampf.TabIndex = 8;
            this.pbx_kampf.TabStop = false;
            this.pbx_kampf.Click += new System.EventHandler(this.pbx_kampf_Click);
            // 
            // pbx_neu
            // 
            this.pbx_neu.Image = ((System.Drawing.Image)(resources.GetObject("pbx_neu.Image")));
            this.pbx_neu.Location = new System.Drawing.Point(72, 277);
            this.pbx_neu.Margin = new System.Windows.Forms.Padding(4);
            this.pbx_neu.Name = "pbx_neu";
            this.pbx_neu.Size = new System.Drawing.Size(212, 123);
            this.pbx_neu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_neu.TabIndex = 9;
            this.pbx_neu.TabStop = false;
            this.pbx_neu.Click += new System.EventHandler(this.pbx_neu_Click);
            // 
            // pbx_suchen
            // 
            this.pbx_suchen.Image = ((System.Drawing.Image)(resources.GetObject("pbx_suchen.Image")));
            this.pbx_suchen.Location = new System.Drawing.Point(368, 277);
            this.pbx_suchen.Margin = new System.Windows.Forms.Padding(4);
            this.pbx_suchen.Name = "pbx_suchen";
            this.pbx_suchen.Size = new System.Drawing.Size(212, 123);
            this.pbx_suchen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbx_suchen.TabIndex = 10;
            this.pbx_suchen.TabStop = false;
            this.pbx_suchen.Click += new System.EventHandler(this.pbx_suchen_Click);
            // 
            // frm_hauptmenü
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(638, 424);
            this.Controls.Add(this.pbx_suchen);
            this.Controls.Add(this.pbx_neu);
            this.Controls.Add(this.pbx_kampf);
            this.Controls.Add(this.pbx_quiz);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frm_hauptmenü";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Hauptmenü";
            ((System.ComponentModel.ISupportInitialize)(this.pbx_quiz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_kampf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_neu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_suchen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbx_quiz;
        private System.Windows.Forms.PictureBox pbx_kampf;
        private System.Windows.Forms.PictureBox pbx_neu;
        private System.Windows.Forms.PictureBox pbx_suchen;
    }
}