﻿using MySql.Data.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_pokemonerstellen : Form
    {
        private List<string> verfügbareNamen;

        public frm_pokemonerstellen()
        {
            InitializeComponent();
        }

        private void com_typ1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(com_typ1.Text == "Boden")
            {
                this.BackColor = System.Drawing.Color.Chocolate;
            }
            else if (com_typ1.Text == "Drache")
            {
                this.BackColor = System.Drawing.Color.Teal;
            }
            else if (com_typ1.Text == "Eis")
            {
                this.BackColor = System.Drawing.Color.PaleTurquoise;
            }
            else if (com_typ1.Text == "Elektro")
            {
                this.BackColor = System.Drawing.Color.Gold;
            }
            else if (com_typ1.Text == "Fee")
            {
                this.BackColor = System.Drawing.Color.LightPink;
            }
            else if (com_typ1.Text == "Feuer")
            {
                this.BackColor = System.Drawing.Color.Tomato;
            }
            else if (com_typ1.Text == "Flug")
            {
                this.BackColor = System.Drawing.Color.Azure;
            }
            else if (com_typ1.Text == "Geist")
            {
                this.BackColor = System.Drawing.Color.MediumPurple;
            }
            else if (com_typ1.Text == "Gestein")
            {
                this.BackColor = System.Drawing.Color.BurlyWood;
            }
            else if (com_typ1.Text == "Gift")
            {
                this.BackColor = System.Drawing.Color.Plum;
            }
            else if (com_typ1.Text == "Kampf")
            {
                this.BackColor = System.Drawing.Color.IndianRed;
            }
            else if (com_typ1.Text == "Käfer")
            {
                this.BackColor = System.Drawing.Color.Olive;
            }
            else if (com_typ1.Text == "Normal")
            {
                this.BackColor = System.Drawing.Color.Silver;
            }
            else if (com_typ1.Text == "Pflanze")
            {
                this.BackColor = System.Drawing.Color.YellowGreen;
            }
            else if (com_typ1.Text == "Psycho")
            {
                this.BackColor = System.Drawing.Color.PaleVioletRed;
            }
            else if (com_typ1.Text == "Stahl")
            {
                this.BackColor = System.Drawing.Color.Gray;
            }
            else if (com_typ1.Text == "Unlicht")
            {
                this.BackColor = System.Drawing.Color.DimGray;
            }
            else if (com_typ1.Text == "Wasser")
            {
                this.BackColor = System.Drawing.Color.RoyalBlue;
            }
        }

        private void InitialisiereVerfügbareNamen()
        {
            verfügbareNamen = new List<string>
            {
            "Flareon", "Aquilox", "Zephyreon", "Thundrax", "Venomoth", "Blitzara", "Terraflare", "Mystigon", "Florashade", "Aeroflare", "Psyblaze", "Aquaphant", "Geistorm", "Ignitionix", "Leafsorcery", "Galvaplant", "Venomystic", "Lunarflare", "Pyroshade", "Aquanox",
            "Aquavortex", "Pyrospirit", "Lunafury", "Leafwhisper", "Thunderquill", "Phantasmaw", "Venomstrike", "Mysticorb", "Ignisurge", "Terrablitz", "Aerothorn", "Blazewind", "Floraluna", "Psyquiver", "Dracoshade", "Abysspulse", "Solarwhisper", "Venomshadow", "Blizzara", "Gaiaspectre",
            "Mystiwave", "Vaporflare", "Venomwhisper", "Spectrospark", "Blossomshade", "Zephyrquake", "Abyssbreeze", "Fluffyclaw", "Ignisurge", "Stardusthowl", "Razorblaze", "Lunarfrost", "Thunderbloom", "Pyrothorn", "Aerogloom", "Dracoquill", "Floralight", "Psychosurge", "Aquanox", "Vortexshadow",
            "Lunawing", "Blastquake", "Aerospirit", "Blizzardflare", "Floralshade", "Thunderleaf", "Venomglow", "Mysticflame", "Aquanox", "Pyroscorch", "Dracowhisper", "Psybreeze", "Stellarsurge", "Geistblade", "Vortexglare", "Terrawisp", "Venomblade", "Flarewhisper", "Zephyrgaze", "Frostquill",
            "Lunashock", "Blitzflare", "Mysticquill", "Aerogrove", "Venomwhirl", "Aquanight", "Psyquiver", "Flarestorm", "Gaiaspectre", "Venomshade", "Lunarflare", "Dracowisp", "Terraspark", "Mysticsurge", "Ignitioflare", "Leafwhisper", "Aeroshadow", "Blizzara", "Psychotide", "Floralight",
            "Voltalrausch","Frostgeist","Flammenwirbel","Aquaphantom","Gaiablitz","Schattengale","Giftsturm","Psychowirbel","Donnerschleier","Flammarion","Gischtgeist","Wolkenschauer","Nebelnymphe","Vulkanflare","Quasarzephyr","Frostglanz","Psymagnet","Flammenmeer","Hydroschleier","Erdensog",
            "Blitznova", "Frostaura", "Flammenfeder", "Aquaflare", "Gaiablitz", "Schattenschleier", "Giftsturm", "Psychozephyr", "Donnergeist", "Flammenherz", "Gischtphantom", "Wolkenfänger", "Nebelschimmer", "Vulkanigma", "Quasarschleier", "Frostglanz", "Psycharm", "Flammenwoge", "Hydrovision", "Erdenschild",

            };
        }

        private void btn_randompokemon_Click(object sender, EventArgs e)
        {
            try
            {
                Random random = new Random();

                if (verfügbareNamen == null || verfügbareNamen.Count == 0)
                {
                    InitialisiereVerfügbareNamen();
                }

                // Typen in einer liste
                string[] möglicheTypen1 = { "Boden", "Drache", "Eis", "Elektro", "Fee", "Feuer", "Flug", "Geist", "Gestein", "Gift", "Kampf", "Käfer", "Normal", "Pflanze", "Psycho", "Stahl", "Unlicht", "Wasser" };
                string[] möglicheTypen2 = { "", "Boden", "Drache", "Eis", "Elektro", "Fee", "Feuer", "Flug", "Geist", "Gestein", "Gift", "Kampf", "Käfer", "Normal", "Pflanze", "Psycho", "Stahl", "Unlicht", "Wasser" };

                // Zufällige Auswahl von Typ und Name
                com_typ1.Text = möglicheTypen1[random.Next(möglicheTypen1.Length)];
                com_typ2.Text = möglicheTypen2[random.Next(möglicheTypen2.Length)];

                // Namen von Listen entfernen
                int index = random.Next(verfügbareNamen.Count);
                txt_name.Text = verfügbareNamen[index];
                verfügbareNamen.RemoveAt(index);

                // Zufällige Werte zuweisen
                num_angriff.Value = random.Next(1, 150);
                num_lebenspunkte.Value = random.Next(1, 200);
                lbl_spectra.Text = "Spectra";
            }
            catch
            {
                MessageBox.Show("Probiere es erneut!", "Fehler bei der Erstellung", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


            private void btn_eintragen_Click(object sender, EventArgs e)
        {
            if(txt_name.Text == "" || com_typ1.Text == "")
            {
                MessageBox.Show("Du musst überall etwas eintragen!", "Fehler", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            else
            {
               cls_pokemon hilfsobjekt = new cls_pokemon(txt_name.Text, com_typ1.Text, com_typ2.Text, Convert.ToInt32(num_angriff.Value), Convert.ToInt32(num_lebenspunkte.Value), lbl_spectra.Text);
               clsDataProvider.InsertPokemon(hilfsobjekt);

                DialogResult = DialogResult.OK;
            }
        }

        private void btn_abbrechen_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
