﻿namespace SP_Semesterprojekt
{
    partial class frm_pokemon
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_antwort2 = new System.Windows.Forms.Button();
            this.lbl_frage = new System.Windows.Forms.Label();
            this.btn_antwort1 = new System.Windows.Forms.Button();
            this.btn_antwort3 = new System.Windows.Forms.Button();
            this.btn_antwort4 = new System.Windows.Forms.Button();
            this.pbx_pokemon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pokemon)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_antwort2
            // 
            this.btn_antwort2.BackColor = System.Drawing.Color.DarkOrange;
            this.btn_antwort2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_antwort2.Location = new System.Drawing.Point(376, 478);
            this.btn_antwort2.Name = "btn_antwort2";
            this.btn_antwort2.Size = new System.Drawing.Size(209, 70);
            this.btn_antwort2.TabIndex = 3;
            this.btn_antwort2.UseVisualStyleBackColor = false;
            this.btn_antwort2.Click += new System.EventHandler(this.btn_antwort2_Click_1);
            // 
            // lbl_frage
            // 
            this.lbl_frage.AutoSize = true;
            this.lbl_frage.BackColor = System.Drawing.Color.Transparent;
            this.lbl_frage.Font = new System.Drawing.Font("Javanese Text", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_frage.Location = new System.Drawing.Point(29, 9);
            this.lbl_frage.Name = "lbl_frage";
            this.lbl_frage.Size = new System.Drawing.Size(556, 98);
            this.lbl_frage.TabIndex = 5;
            this.lbl_frage.Text = "Welches Pokemon ist das?";
            this.lbl_frage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn_antwort1
            // 
            this.btn_antwort1.BackColor = System.Drawing.Color.Red;
            this.btn_antwort1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_antwort1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_antwort1.Location = new System.Drawing.Point(75, 478);
            this.btn_antwort1.Margin = new System.Windows.Forms.Padding(0);
            this.btn_antwort1.Name = "btn_antwort1";
            this.btn_antwort1.Size = new System.Drawing.Size(209, 70);
            this.btn_antwort1.TabIndex = 6;
            this.btn_antwort1.UseVisualStyleBackColor = false;
            this.btn_antwort1.Click += new System.EventHandler(this.btn_antwort1_Click_1);
            // 
            // btn_antwort3
            // 
            this.btn_antwort3.BackColor = System.Drawing.Color.ForestGreen;
            this.btn_antwort3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_antwort3.Location = new System.Drawing.Point(75, 597);
            this.btn_antwort3.Name = "btn_antwort3";
            this.btn_antwort3.Size = new System.Drawing.Size(209, 70);
            this.btn_antwort3.TabIndex = 7;
            this.btn_antwort3.UseVisualStyleBackColor = false;
            this.btn_antwort3.Click += new System.EventHandler(this.btn_antwort3_Click_1);
            // 
            // btn_antwort4
            // 
            this.btn_antwort4.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_antwort4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_antwort4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_antwort4.Location = new System.Drawing.Point(376, 597);
            this.btn_antwort4.Name = "btn_antwort4";
            this.btn_antwort4.Size = new System.Drawing.Size(209, 70);
            this.btn_antwort4.TabIndex = 8;
            this.btn_antwort4.UseVisualStyleBackColor = false;
            this.btn_antwort4.Click += new System.EventHandler(this.btn_antwort4_Click_1);
            // 
            // pbx_pokemon
            // 
            this.pbx_pokemon.Image = global::SP_Semesterprojekt.Properties.Resources.schiggy;
            this.pbx_pokemon.Location = new System.Drawing.Point(136, 90);
            this.pbx_pokemon.Name = "pbx_pokemon";
            this.pbx_pokemon.Size = new System.Drawing.Size(385, 359);
            this.pbx_pokemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_pokemon.TabIndex = 4;
            this.pbx_pokemon.TabStop = false;
            // 
            // frm_pokemon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(639, 695);
            this.Controls.Add(this.btn_antwort4);
            this.Controls.Add(this.btn_antwort3);
            this.Controls.Add(this.btn_antwort1);
            this.Controls.Add(this.pbx_pokemon);
            this.Controls.Add(this.btn_antwort2);
            this.Controls.Add(this.lbl_frage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_pokemon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokemon";
            this.Load += new System.EventHandler(this.frm_pokemon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_pokemon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_antwort2;
        private System.Windows.Forms.PictureBox pbx_pokemon;
        private System.Windows.Forms.Label lbl_frage;
        private System.Windows.Forms.Button btn_antwort1;
        private System.Windows.Forms.Button btn_antwort3;
        private System.Windows.Forms.Button btn_antwort4;
    }
}

