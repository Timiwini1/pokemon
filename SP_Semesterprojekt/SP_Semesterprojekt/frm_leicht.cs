﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_leicht : Form
    {
        //Variablen für das Quiz
        int correctAnswer;
        int questionNumber = 1;
        int score;
        int percentage;
        int totalQuestions;
        public frm_leicht()
        {
            InitializeComponent();
            askQuestion(questionNumber);
            totalQuestions = 10;
        }
        private void checkAnswerEvent(object sender, EventArgs e)
        {
            var senderObject = (Button)sender;
            int buttonTag = Convert.ToInt32(senderObject.Tag);

            if (buttonTag == correctAnswer)
            {
                score++;
            }
            if (questionNumber == totalQuestions)
            {
                // work out the percentage
                percentage = (int)Math.Round((double)(score * 100) / totalQuestions);
                MessageBox.Show(
                    "Fertig!" + Environment.NewLine +
                    "Du hast " + score + "/10 Fragen richtig beantwortet.");

                DialogResult = DialogResult.OK;
            }
            questionNumber++;
            askQuestion(questionNumber);
        }

        private void askQuestion(int qnum)
        {
            switch (qnum)
            {
                case 1:
                    pbx_pokemon.Image = Properties.Resources.abra;
                    lbl_frage.Text = "Welches Pokémon hat Teleportfähigkeiten?";
                    btn_antwort1.Text = "Hypno";
                    btn_antwort2.Text = "Lapras";
                    btn_antwort3.Text = "Abra";
                    btn_antwort4.Text = "Rosana";
                    correctAnswer = 3;
                    break;
                case 2:
                    pbx_pokemon.Image = Properties.Resources.turtok;
                    lbl_frage.Text = "Welches Pokémon ist auf diesem Bild abgebildet?";
                    btn_antwort1.Text = "Hornliu";
                    btn_antwort2.Text = "Nidoran";
                    btn_antwort3.Text = "Turtok";
                    btn_antwort4.Text = "Paras";
                    correctAnswer = 3;
                    break;
                case 3:
                    pbx_pokemon.Image = Properties.Resources.glutexo;
                    lbl_frage.Text = "Welches Pokémon entwickelt sich aus Glumanda?";
                    btn_antwort1.Text = "Schillok";
                    btn_antwort2.Text = "Geckabor";
                    btn_antwort3.Text = "Milotic";
                    btn_antwort4.Text = "Glutexo";
                    correctAnswer = 4;
                    break;
                case 4:
                    pbx_pokemon.Image = Properties.Resources.relaxo;
                    lbl_frage.Text = "Welches Pokémon ist für seinen starken Schlaf bekannt und liegt oft faul herum?";
                    btn_antwort1.Text = "Mew";
                    btn_antwort2.Text = "Arktos";
                    btn_antwort3.Text = "Relaxo";
                    btn_antwort4.Text = "Evoli"; ;
                    correctAnswer = 3;
                    break;
                case 5:
                    pbx_pokemon.Image = Properties.Resources.arktos;
                    lbl_frage.Text = "Welches Pokémon hat den Typ Eis und Flug?";
                    btn_antwort1.Text = "Arktos";
                    btn_antwort2.Text = "Zapdos";
                    btn_antwort3.Text = "Lavados";
                    btn_antwort4.Text = "Entei";
                    correctAnswer = 1;
                    break;
                case 6:
                    pbx_pokemon.Image = Properties.Resources.evoli;
                    lbl_frage.Text = "Welches Pokemon hat mehr als 3 verschiedene Entwicklungen!";
                    btn_antwort1.Text = "Rattfratz";
                    btn_antwort2.Text = "Evoli";
                    btn_antwort3.Text = "Flemli";
                    btn_antwort4.Text = "Simsala";
                    correctAnswer = 2;
                    break;
                case 7:
                    pbx_pokemon .Image = Properties.Resources.muschas1;
                    lbl_frage.Text = "Welches Pokémon schaut wie eine Muschel aus?";
                    btn_antwort1.Text = "Pummeluff";
                    btn_antwort2.Text = "Robbal";
                    btn_antwort3.Text = "Jurob";
                    btn_antwort4.Text = "Muschas";
                    correctAnswer = 4;
                    break;
                case 8:
                    pbx_pokemon .Image = Properties.Resources.pokeball;
                    lbl_frage.Text = "Welche Frabe hat ein ganz normaler Pokeball?";
                    btn_antwort1.Text = "Rot und Weiß";
                    btn_antwort2.Text = "Gelb und Schwarz";
                    btn_antwort3.Text = "Blau und Weiß";
                    btn_antwort4.Text = "Lila und Gelb";
                    correctAnswer = 1;
                    break;

                case 9:
                    pbx_pokemon.Image = Properties.Resources.donnerblitz;
                    lbl_frage.Text = "Welches Typ hat die Attacke Donnerblitz?";
                    btn_antwort1.Text = "Boden";
                    btn_antwort2.Text = "Wasser";
                    btn_antwort3.Text = "Gestein";
                    btn_antwort4.Text = "Elektro";
                    correctAnswer = 4;
                    break;

                case 10:
                    pbx_pokemon.Image = Properties.Resources.bisasam;
                    lbl_frage.Text = "Welches Pokemon ist das erste im Pokedex!";
                    btn_antwort1.Text = "Bisasam";
                    btn_antwort2.Text = "Pikachu";
                    btn_antwort3.Text = "Nidoran";
                    btn_antwort4.Text = "Glumanda";
                    correctAnswer = 1;
                    break;

            }
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
