﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SP_Semesterprojekt
{
    public partial class frm_pokemon : Form
    {
        public frm_pokemon()
        {
            InitializeComponent();
        }

        private int richtigeAntwort;

        private void frm_pokemon_Load(object sender, EventArgs e)
        {
            StartQuiz();
        }

        private void StartQuiz()
        {
            this.FormBorderStyle = FormBorderStyle.Sizable;

            Random r = new Random();
            int zahl1 = r.Next(0, 5); // Ändere die Obergrenze auf 5, um alle 5 Fälle abzudecken.

            if (zahl1 == 0)
            {
                SetQuestion(Properties.Resources.pikachu, "Pipi", "Turtok", "Zapdos", "Pikachu", 4);
            }
            else if (zahl1 == 1)
            {
                SetQuestion(Properties.Resources.relaxo, "Mauzi", "Auqana", "Relaxo", "Evoli", 3);
            }
            else if (zahl1 == 2)
            {
                SetQuestion(Properties.Resources.schiggy, "Entei", "Schiggy", "Raichu", "Slurp", 2);
            }
            else if (zahl1 == 3)
            {
                SetQuestion(Properties.Resources.bisasam, "Bisasam", "Bauz", "Meganie", "Kangama", 1);
            }
            else if (zahl1 == 4)
            {
                SetQuestion(Properties.Resources.Glurak, "Glurak", "Ho-Oh", "Mew", "Tauros", 1);
            }
        }
        //Methode für die andere methode oben, dass es besser ausschaut wie konstruktor so
        private void SetQuestion(Image image, string answer1, string answer2, string answer3, string answer4, int correctAnswer)
        {
            pbx_pokemon.Image = image;
            btn_antwort1.Text = answer1;
            btn_antwort2.Text = answer2;
            btn_antwort3.Text = answer3;
            btn_antwort4.Text = answer4;
            richtigeAntwort = correctAnswer;
        }

        //Antworten werden geprüft, wenn richtig neues form, wenn falsch wiederholen
        private void CheckAnswer(int selectedAnswer)
        {
            if (selectedAnswer == richtigeAntwort)
            {
                MessageBox.Show("Richtig!", "Erfolg", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();

                //neues Form
                frm_hauptmenü h = new frm_hauptmenü();
                h.ShowDialog();
            }
            else
            {
                // Falsch beantwortet, starte das Quiz erneut.
                MessageBox.Show("Falsch! Versuche es erneut.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                StartQuiz();
            }
        }

        //Wird die Methode CheckAnswer verwendet um die antwort zu checken
        private void btn_antwort1_Click_1(object sender, EventArgs e)
        {
            CheckAnswer(1);
        }

        private void btn_antwort2_Click_1(object sender, EventArgs e)
        {
            CheckAnswer(2);
        }

        private void btn_antwort3_Click_1(object sender, EventArgs e)
        {
            CheckAnswer(3);
        }

        private void btn_antwort4_Click_1(object sender, EventArgs e)
        {
            CheckAnswer(4);
        }
    }
}
