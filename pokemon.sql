

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


--
-- Datenbank: `pokemon`
--
CREATE DATABASE IF NOT EXISTS `semester_pokemon` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `semester_pokemon`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_alola`
--

CREATE TABLE `tbl_alola` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `geschwindigkeit` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_alola`
--

INSERT INTO `tbl_alola` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `geschwindigkeit`, `generation_name`) VALUES
(722, 'Bauz', 'Pflanze', 'Flug', 55, 68, NULL, 'Alola'),
(723, 'Arboretoss', 'Pflanze', 'Flug', 75, 78, NULL, 'Alola'),
(724, 'Silvarro', 'Pflanze', 'Geist', 107, 78, NULL, 'Alola'),
(725, 'Flamiau', 'Feuer', NULL, 65, 45, NULL, 'Alola'),
(726, 'Miezunder', 'Feuer', NULL, 85, 50, NULL, 'Alola'),
(727, 'Fuegro', 'Feuer', 'Unlicht', 145, 60, NULL, 'Alola'),
(728, 'Robball', 'Wasser', NULL, 60, 50, NULL, 'Alola'),
(729, 'Marikeck', 'Wasser', NULL, 75, 80, NULL, 'Alola'),
(730, 'Primarene', 'Wasser', 'Fee', 126, 80, NULL, 'Alola'),
(731, 'Peppeck', 'Normal', 'Flug', 35, 45, NULL, 'Alola'),
(732, 'Trompeck', 'Normal', 'Flug', 85, 55, NULL, 'Alola'),
(733, 'Tukanon', 'Normal', 'Flug', 120, 80, NULL, 'Alola'),
(734, 'Mangunior', 'Normal', NULL, 50, 62, NULL, 'Alola'),
(735, 'Manguspektor', 'Normal', NULL, 75, 80, NULL, 'Alola'),
(736, 'Mabula', 'Käfer', NULL, 60, 45, NULL, 'Alola'),
(737, 'Akkup', 'Käfer', 'Elektro', 55, 50, NULL, 'Alola'),
(738, 'Donarion', 'Käfer', 'Elektro', 83, 70, NULL, 'Alola'),
(739, 'Krabbox', 'Kampf', NULL, 105, 47, NULL, 'Alola'),
(740, 'Krawell', 'Kampf', 'Eis', 125, 89, NULL, 'Alola'),
(741, 'Choreogel', 'Feuer', 'Flug', 65, 60, NULL, 'Alola'),
(742, 'Wommel', 'Käfer', 'Fee', 45, 40, NULL, 'Alola'),
(743, 'Bandelby', 'Käfer', 'Fee', 55, 60, NULL, 'Alola'),
(744, 'Wuffels', 'Gestein', NULL, 70, 75, NULL, 'Alola'),
(745, 'Wolwerock', 'Gestein', NULL, 115, 75, NULL, 'Alola'),
(746, 'Lusardin', 'Wasser', NULL, 72, 80, NULL, 'Alola'),
(747, 'Garstella', 'Gift', 'Wasser', 101, 103, NULL, 'Alola'),
(748, 'Aggrostella', 'Gift', 'Wasser', 107, 71, NULL, 'Alola'),
(749, 'Pampuli', 'Boden', NULL, 52, 55, NULL, 'Alola'),
(750, 'Pampross', 'Boden', NULL, 82, 85, NULL, 'Alola'),
(751, 'Araqua', 'Wasser', 'Käfer', 70, 68, NULL, 'Alola'),
(752, 'Aranestro', 'Wasser', 'Käfer', 120, 98, NULL, 'Alola'),
(753, 'Imantis', 'Pflanze', NULL, 70, 55, NULL, 'Alola'),
(754, 'Mantidea', 'Pflanze', NULL, 120, 95, NULL, 'Alola'),
(755, 'Bubungus', 'Pflanze', 'Fee', 54, 50, NULL, 'Alola'),
(756, 'Lamellux', 'Pflanze', 'Fee', 74, 90, NULL, 'Alola'),
(757, 'Molunk', 'Gift', 'Feuer', 94, 106, NULL, 'Alola'),
(758, 'Amfira', 'Gift', 'Feuer', 70, 70, NULL, 'Alola'),
(759, 'Velursi', 'Normal', 'Kampf', 75, 70, NULL, 'Alola'),
(760, 'Kosturso', 'Normal', 'Kampf', 130, 95, NULL, 'Alola'),
(761, 'Frubberl', 'Pflanze', NULL, 70, 70, NULL, 'Alola'),
(762, 'Frubaila', 'Pflanze', NULL, 110, 80, NULL, 'Alola'),
(763, 'Fruyal', 'Pflanze', NULL, 165, 120, NULL, 'Alola'),
(764, 'Curelei', 'Fee', NULL, 60, 50, NULL, 'Alola'),
(765, 'Kommandutan', 'Normal', 'Psycho', 120, 75, NULL, 'Alola'),
(766, 'Quartermak', 'Kampf', NULL, 160, 80, NULL, 'Alola'),
(767, 'Reißlaus', 'Käfer', 'Wasser', 125, 75, NULL, 'Alola'),
(768, 'Tectass', 'Käfer', 'Wasser', 185, 110, NULL, 'Alola'),
(769, 'Sankabuh', 'Geist', 'Boden', 55, 85, NULL, 'Alola'),
(770, 'Colossand', 'Geist', 'Boden', 107, 85, NULL, 'Alola'),
(771, 'Gufa', 'Wasser', NULL, 70, 70, NULL, 'Alola'),
(772, 'Typ:Null', 'Normal', NULL, 95, 95, NULL, 'Alola'),
(773, 'Amigento', 'Normal', NULL, 120, 95, NULL, 'Alola'),
(774, 'Meteno', 'Gestein', 'Flug', 55, 60, NULL, 'Alola'),
(775, 'Koalelu', 'Normal', NULL, 48, 45, NULL, 'Alola'),
(776, 'Tortunator', 'Feuer', 'Drache', 78, 140, NULL, 'Alola'),
(777, 'Togedemaru', 'Elektro', 'Stahl', 82, 65, NULL, 'Alola'),
(778, 'Mimigma', 'Geist', 'Fee', 90, 55, NULL, 'Alola'),
(779, 'Knirfish', 'Wasser', 'Psycho', 60, 65, NULL, 'Alola'),
(780, 'Sen-Long', 'Normal', 'Drache', 110, 75, NULL, 'Alola'),
(781, 'Moruda', 'Geist', 'Pflanze', 55, 60, NULL, 'Alola'),
(782, 'Miniras', 'Drache', NULL, 40, 45, NULL, 'Alola'),
(783, 'Mediras', 'Drache', 'Kampf', 50, 65, NULL, 'Alola'),
(784, 'Grandiras', 'Drache', 'Kampf', 100, 85, NULL, 'Alola'),
(785, 'Kapu-Riki', 'Elektro', 'Fee', 85, 70, NULL, 'Alola'),
(786, 'Kapu-Fala', 'Psycho', 'Fee', 130, 70, NULL, 'Alola'),
(787, 'Kapu-Toro', 'Pflanze', 'Fee', 115, 70, NULL, 'Alola'),
(788, 'Kapu-Kime', 'Wasser', 'Fee', 130, 70, NULL, 'Alola'),
(789, 'Cosmog', 'Psycho', NULL, 29, 43, NULL, 'Alola'),
(790, 'Cosmovum', 'Psycho', NULL, 131, 43, NULL, 'Alola'),
(791, 'Solgaleo', 'Psycho', 'Stahl', 137, 137, NULL, 'Alola'),
(792, 'Lunala', 'Psycho', 'Geist', 113, 137, NULL, 'Alola'),
(793, 'Anego', 'Gestein', 'Gift', 125, 95, NULL, 'Alola'),
(794, 'Masskito', 'Käfer', 'Kampf', 150, 95, NULL, 'Alola'),
(795, 'Schabelle', 'Käfer', 'Kampf', 80, 65, NULL, 'Alola'),
(796, 'Voltriant', 'Elektro', NULL, 55, 85, NULL, 'Alola'),
(797, 'Kaguron', 'Stahl', 'Flug', 132, 78, NULL, 'Alola'),
(798, 'Katagami', 'Pflanze', 'Stahl', 132, 78, NULL, 'Alola'),
(799, 'Schlingking', 'Unlicht', 'Drache', 131, 107, NULL, 'Alola'),
(800, 'Necrozma', 'Psycho', NULL, 137, 97, NULL, 'Alola'),
(801, 'Magearna', 'Stahl', 'Fee', 95, 80, NULL, 'Alola'),
(802, 'Marshadow', 'Kampf', 'Geist', 125, 90, NULL, 'Alola'),
(803, 'Venicro', 'Gift', NULL, 73, 67, NULL, 'Alola'),
(804, 'Agoyon', 'Gift', 'Drache', 101, 103, NULL, 'Alola'),
(805, 'Muramura', 'Gestein', 'Stahl', 161, 97, NULL, 'Alola'),
(806, 'Kopplosio', 'Feuer', 'Geist', 127, 77, NULL, 'Alola'),
(807, 'Zeraora', 'Elektro', NULL, 112, 88, NULL, 'Alola'),
(808, 'Meltan', 'Stahl', NULL, 65, 46, NULL, 'Alola'),
(809, 'Melmetal', 'Stahl', NULL, 143, 135, NULL, 'Alola');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_einall`
--

CREATE TABLE `tbl_einall` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_einall`
--

INSERT INTO `tbl_einall` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(495, 'Serpifeu', 'Pflanze', NULL, 45, 45, 'Einall'),
(496, 'Efoserp', 'Pflanze', NULL, 60, 60, 'Einall'),
(497, 'Serpiroyal', 'Pflanze', NULL, 75, 75, 'Einall'),
(498, 'Floink', 'Feuer', NULL, 63, 45, 'Einall'),
(499, 'Ferkokel', 'Feuer', 'Kampf', 93, 65, 'Einall'),
(500, 'Flambirex', 'Feuer', 'Kampf', 123, 90, 'Einall'),
(501, 'Ottaro', 'Wasser', NULL, 55, 55, 'Einall'),
(502, 'Zwottronin', 'Wasser', NULL, 75, 75, 'Einall'),
(503, 'Admurai', 'Wasser', NULL, 100, 85, 'Einall'),
(504, 'Nagelotz', 'Normal', NULL, 50, 38, 'Einall'),
(505, 'Kukmarda', 'Normal', NULL, 60, 45, 'Einall'),
(506, 'Yorkleff', 'Normal', NULL, 50, 50, 'Einall'),
(507, 'Terribark', 'Normal', NULL, 90, 80, 'Einall'),
(508, 'Bissbark', 'Normal', NULL, 110, 100, 'Einall'),
(509, 'Felilou', 'Unlicht', NULL, 50, 62, 'Einall'),
(510, 'Kleoparda', 'Unlicht', NULL, 88, 75, 'Einall'),
(511, 'Vegimak', 'Pflanze', NULL, 53, 70, 'Einall'),
(512, 'Vegichita', 'Pflanze', NULL, 98, 110, 'Einall'),
(513, 'Grillmak', 'Feuer', NULL, 53, 70, 'Einall'),
(514, 'Grillchita', 'Feuer', NULL, 98, 110, 'Einall'),
(515, 'Sodamak', 'Wasser', NULL, 53, 70, 'Einall'),
(516, 'Sodachita', 'Wasser', NULL, 98, 110, 'Einall'),
(517, 'Somniam', 'Psycho', NULL, 55, 45, 'Einall'),
(518, 'Somnivora', 'Psycho', NULL, 85, 70, 'Einall'),
(519, 'Dusselgur', 'Normal', 'Flug', 55, 45, 'Einall'),
(520, 'Navitaub', 'Normal', 'Flug', 55, 50, 'Einall'),
(521, 'Fasasnob', 'Normal', 'Flug', 87, 62, 'Einall'),
(522, 'Elezeba', 'Elektro', NULL, 63, 45, 'Einall'),
(523, 'Zebritz', 'Elektro', NULL, 100, 75, 'Einall'),
(524, 'Kiesling', 'Gestein', NULL, 50, 50, 'Einall'),
(525, 'Sedimantur', 'Gestein', NULL, 95, 70, 'Einall'),
(526, 'Brockoloss', 'Gestein', NULL, 135, 85, 'Einall'),
(527, 'Fleknoil', 'Psycho', 'Flug', 55, 50, 'Einall'),
(528, 'Fletiamo', 'Psycho', 'Flug', 80, 70, 'Einall'),
(529, 'Rotomurf', 'Boden', NULL, 60, 85, 'Einall'),
(530, 'Stalobor', 'Boden', 'Stahl', 135, 110, 'Einall'),
(531, 'Ohrdoch', 'Normal', NULL, 66, 85, 'Einall'),
(532, 'Praktibalk', 'Kampf', NULL, 105, 85, 'Einall'),
(533, 'Strepoli', 'Kampf', NULL, 60, 45, 'Einall'),
(534, 'Meistagrif', 'Kampf', NULL, 135, 95, 'Einall'),
(535, 'Schallquap', 'Wasser', NULL, 50, 50, 'Einall'),
(536, 'Mebrana', 'Wasser', 'Boden', 75, 105, 'Einall'),
(537, 'Branawarz', 'Wasser', 'Boden', 100, 150, 'Einall'),
(538, 'Jiutesto', 'Kampf', NULL, 70, 75, 'Einall'),
(539, 'Karadonis', 'Kampf', NULL, 105, 125, 'Einall'),
(540, 'Strawickl', 'Käfer', 'Pflanze', 45, 55, 'Einall'),
(541, 'Folikon', 'Käfer', 'Pflanze', 60, 75, 'Einall'),
(542, 'Matrifol', 'Käfer', 'Pflanze', 110, 115, 'Einall'),
(543, 'Toxiped', 'Käfer', 'Gift', 50, 40, 'Einall'),
(544, 'Rollum', 'Käfer', 'Gift', 60, 55, 'Einall'),
(545, 'Cerapendra', 'Käfer', 'Gift', 90, 70, 'Einall'),
(546, 'Waumboll', 'Pflanze', 'Fee', 67, 60, 'Einall'),
(547, 'Elfun', 'Pflanze', 'Fee', 110, 75, 'Einall'),
(548, 'Lilminip', 'Pflanze', NULL, 50, 45, 'Einall'),
(549, 'Dressella', 'Pflanze', NULL, 110, 70, 'Einall'),
(550, 'Barschuft', 'Wasser', NULL, 75, 65, 'Einall'),
(551, 'Ganovil', 'Boden', 'Unlicht', 85, 70, 'Einall'),
(552, 'Rokkaiman', 'Boden', 'Unlicht', 125, 95, 'Einall'),
(553, 'Rabigator', 'Boden', 'Unlicht', 155, 120, 'Einall'),
(554, 'Flampion', 'Feuer', NULL, 55, 55, 'Einall'),
(555, 'Flampivian', 'Feuer', NULL, 75, 85, 'Einall'),
(556, 'Maracamba', 'Pflanze', NULL, 60, 75, 'Einall'),
(557, 'Lithomit', 'Käfer', 'Gestein', 75, 85, 'Einall'),
(558, 'Castellith', 'Käfer', 'Gestein', 100, 110, 'Einall'),
(559, 'Zurrokex', 'Unlicht', 'Kampf', 105, 110, 'Einall'),
(560, 'Irokex', 'Unlicht', 'Kampf', 135, 110, 'Einall'),
(561, 'Symvolara', 'Psycho', 'Flug', 65, 60, 'Einall'),
(562, 'Makabaja', 'Geist', NULL, 50, 58, 'Einall'),
(563, 'Echnatol', 'Geist', NULL, 53, 50, 'Einall'),
(564, 'Galapaflos', 'Wasser', 'Gestein', 78, 72, 'Einall'),
(565, 'Karippas', 'Wasser', 'Gestein', 108, 103, 'Einall'),
(566, 'Flapteryx', 'Gestein', 'Flug', 140, 75, 'Einall'),
(567, 'Aeropteryx', 'Gestein', 'Flug', 164, 85, 'Einall'),
(568, 'Unratütox', 'Gift', NULL, 64, 103, 'Einall'),
(569, 'Deponitox', 'Gift', NULL, 91, 103, 'Einall'),
(570, 'Zorua', 'Unlicht', NULL, 65, 40, 'Einall'),
(571, 'Zoroak', 'Unlicht', NULL, 105, 60, 'Einall'),
(572, 'Picochilla', 'Normal', NULL, 45, 55, 'Einall'),
(573, 'Chillabell', 'Normal', NULL, 50, 60, 'Einall'),
(574, 'Mollimorba', 'Psycho', NULL, 65, 95, 'Einall'),
(575, 'Hypnomorba', 'Psycho', NULL, 105, 105, 'Einall'),
(576, 'Morbitesse', 'Psycho', NULL, 50, 75, 'Einall'),
(577, 'Monozyto', 'Psycho', NULL, 65, 95, 'Einall'),
(578, 'Mitodos', 'Psycho', NULL, 85, 125, 'Einall'),
(579, 'Zytomega', 'Psycho', NULL, 125, 120, 'Einall'),
(580, 'Piccolente', 'Wasser', 'Flug', 55, 45, 'Einall'),
(581, 'Swaroness', 'Wasser', 'Flug', 75, 60, 'Einall'),
(582, 'Gelatini', 'Eis', NULL, 50, 50, 'Einall'),
(583, 'Gelatroppo', 'Eis', NULL, 65, 60, 'Einall'),
(584, 'Gelatwino', 'Eis', NULL, 50, 50, 'Einall'),
(585, 'Sesokitz', 'Normal', 'Pflanze', 75, 60, 'Einall'),
(586, 'Kronjuwild', 'Normal', 'Pflanze', 100, 75, 'Einall'),
(587, 'Emolga', 'Elektro', 'Flug', 75, 55, 'Einall'),
(588, 'Laukaps', 'Käfer', NULL, 63, 45, 'Einall'),
(589, 'Cavalanzas', 'Käfer', 'Stahl', 100, 75, 'Einall'),
(590, 'Tarnpignon', 'Pflanze', 'Gift', 50, 65, 'Einall'),
(591, 'Hutsassa', 'Pflanze', 'Gift', 75, 70, 'Einall'),
(592, 'Quabbel', 'Wasser', 'Geist', 55, 45, 'Einall'),
(593, 'Apoquallyp', 'Wasser', 'Geist', 75, 65, 'Einall'),
(594, 'Mamolida', 'Wasser', NULL, 95, 85, 'Einall'),
(595, 'Wattzapf', 'Käfer', 'Elektro', 55, 50, 'Einall'),
(596, 'Voltula', 'Käfer', 'Elektro', 80, 70, 'Einall'),
(597, 'Kastadur', 'Pflanze', 'Stahl', 85, 60, 'Einall'),
(598, 'Tentantel', 'Pflaze', 'Stahl', 109, 109, 'Einall'),
(599, 'Klikk', 'Stahl', NULL, 50, 40, 'Einall'),
(600, 'Kliklak', 'Stahl', NULL, 75, 60, 'Einall'),
(601, 'Klikdiklak', 'Stahl', NULL, 120, 100, 'Einall'),
(602, 'Zapplardin', 'Elektro', NULL, 55, 60, 'Einall'),
(603, 'Zapplalek', 'Elektro', NULL, 85, 70, 'Einall'),
(604, 'Zapplarang', 'Elektro', NULL, 115, 80, 'Einall'),
(605, 'Pygraulon', 'Psycho', NULL, 55, 55, 'Einall'),
(606, 'Megalon', 'Psycho', NULL, 85, 75, 'Einall'),
(607, 'Lichtel', 'Geis', 'Feuer', 50, 45, 'Einall'),
(608, 'Laternecto', 'Gesit', 'Feuer', 60, 55, 'Einall'),
(609, 'Skelabra', 'Geist', 'Feuer', 80, 60, 'Einall'),
(610, 'Milza', 'Drache', NULL, 60, 60, 'Einall'),
(611, 'Sharfax', 'Drache', NULL, 90, 100, 'Einall'),
(612, 'Maxax', 'Drache', NULL, 128, 90, 'Einall'),
(613, 'Petznief', 'Eis', NULL, 60, 45, 'Einall'),
(614, 'Siberio', 'Eis', NULL, 95, 59, 'Einall'),
(615, 'Frigometri', 'Eis', NULL, 117, 75, 'Einall'),
(616, 'Schnuthelm', 'Käfer', NULL, 50, 50, 'Einall'),
(617, 'Hydragil', 'Käfer', NULL, 65, 60, 'Einall'),
(618, 'Flunschlik', 'Boden', 'Elektro', 95, 70, 'Einall'),
(619, 'Lin-Fu', 'Kampf', NULL, 80, 65, 'Einall'),
(620, 'Wie-Shu', 'Kampf', NULL, 105, 85, 'Einall'),
(621, 'Shardrago', 'Drache', NULL, 85, 90, 'Einall'),
(622, 'Golbit', 'Boden', 'Geist', 50, 58, 'Einall'),
(623, 'Golgantes', 'Boden', 'Geist', 100, 78, 'Einall'),
(624, 'Gladiantri', 'Unlicht', 'Stahl', 120, 98, 'Einall'),
(625, 'Caesurio', 'Unlicht', 'Stahl', 125, 80, 'Einall'),
(626, 'Bisofank', 'Normal', NULL, 110, 70, 'Einall'),
(627, 'Geronimatz', 'Normal', 'Flug', 145, 95, 'Einall'),
(628, 'Washakwil', 'Normal', 'Flug', 83, 70, 'Einall'),
(629, 'Skallyk', 'Unlicht', 'Flug', 85, 80, 'Einall'),
(630, 'Grypheldis', 'Unlicht', 'Flug', 135, 90, 'Einall'),
(631, 'Furnifraß', 'Feuer', NULL, 58, 54, 'Einall'),
(632, 'Fermicula', 'Käfer', 'Stahl', 98, 86, 'Einall'),
(633, 'Kapuno', 'Unlicht', 'Drache', 55, 50, 'Einall'),
(634, 'Doudino', 'Ulicht', 'Drache', 80, 68, 'Einall'),
(635, 'Trikephalo', 'Unlicht', 'Drache', 70, 75, 'Einall'),
(636, 'Ignivor', 'Käfer', 'Feuer', 105, 85, 'Einall'),
(637, 'Ramoth', 'Käfer', 'Feuer', 140, 100, 'Einall'),
(638, 'Kobalium', 'Stahl', 'Kampf', 90, 91, 'Einall'),
(639, 'Terrakium', 'Gestein', 'Kampf', 129, 91, 'Einall'),
(640, 'Viridium', 'Pflaze', 'Kampf', 90, 91, 'Einall'),
(641, 'Boreos', 'Flug', NULL, 115, 91, 'Einall'),
(642, 'Voltolos', 'Elektro', 'Flug', 115, 91, 'Einall'),
(643, 'Reshiram', 'Drache', 'Feuer', 120, 100, 'Einall'),
(644, 'Zekrom', 'Drache', 'Elektro', 150, 100, 'Einall'),
(645, 'Demeteros', 'Boden', 'Flug', 90, 89, 'Einall'),
(646, 'Kyurem', 'Drache', 'Eis', 130, 125, 'Einall'),
(647, 'Keldeo', 'Wasser', 'Kampf', 72, 91, 'Einall'),
(648, 'Meloetta', 'Normal', 'Psycho', 77, 100, 'Einall'),
(649, 'Genesect', 'Käfer', 'Stahl', 120, 71, 'Einall');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_galar`
--

CREATE TABLE `tbl_galar` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `geschwindigkeit` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_galar`
--

INSERT INTO `tbl_galar` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `geschwindigkeit`, `generation_name`) VALUES
(810, 'Chimpep', 'Pflanze', NULL, 65, 50, NULL, 'Galar'),
(811, 'Chimstix', 'Pflanze', NULL, 65, 65, NULL, 'Galar'),
(812, 'Gortrom', 'Pflanze', NULL, 95, 100, NULL, 'Galar'),
(813, 'Hopplo', 'Feuer', NULL, 65, 50, NULL, 'Galar'),
(814, 'Kickerlo', 'Feuer', NULL, 85, 65, NULL, 'Galar'),
(815, 'Liberlo', 'Feuer', NULL, 115, 100, NULL, 'Galar'),
(816, 'Memmeon', 'Wasser', NULL, 50, 50, NULL, 'Galar'),
(817, 'Phlegleon', 'Wasser', NULL, 65, 65, NULL, 'Galar'),
(818, 'Intelleon', 'Wasser', NULL, 125, 100, NULL, 'Galar'),
(819, 'Raffel', 'Normal', NULL, 20, 30, NULL, 'Galar'),
(820, 'Schlaraffel', 'Normal', NULL, 25, 40, NULL, 'Galar'),
(821, 'Meikro', 'Flug', NULL, 50, 60, NULL, 'Galar'),
(822, 'Kranoviz', 'Flug', NULL, 60, 70, NULL, 'Galar'),
(823, 'Kraromor', 'Flug', 'Stahl', 95, 100, NULL, 'Galar'),
(824, 'Sensect', 'Käfer', NULL, 10, 30, NULL, 'Galar'),
(825, 'Keradar', 'Käfer', 'Psycho', 80, 100, NULL, 'Galar'),
(826, 'Maritellit', 'Käfer', 'Psycho', 68, 72, NULL, 'Galar'),
(827, 'Kleptifux', 'Unlicht', NULL, 65, 70, NULL, 'Galar'),
(828, 'Gaunux', 'Unlicht', NULL, 95, 110, NULL, 'Galar'),
(829, 'Cottii', 'Pflanze', NULL, 40, 40, NULL, 'Galar'),
(830, 'Cottomi', 'Pflanze', NULL, 65, 60, NULL, 'Galar'),
(831, 'Wolly', 'Normal', NULL, 40, 45, NULL, 'Galar'),
(832, 'Zwollock', 'Normal', NULL, 60, 65, NULL, 'Galar'),
(833, 'Kamehaps', 'Wasser', NULL, 83, 80, NULL, 'Galar'),
(834, 'Kamalm', 'Wasser', 'Gestein', 125, 100, NULL, 'Galar'),
(835, 'Voldi', 'Elektro', NULL, 38, 45, NULL, 'Galar'),
(836, 'Bellektro', 'Elektro', NULL, 65, 60, NULL, 'Galar'),
(837, 'Klonkett', 'Gestein', NULL, 80, 75, NULL, 'Galar'),
(838, 'Wawgong', 'Gestein', 'Feuer', 110, 75, NULL, 'Galar'),
(839, 'Montecarbo', 'Gestein', 'Feuer', 90, 80, NULL, 'Galar'),
(840, 'Knapfel', 'Pflanze', 'Drache', 65, 60, NULL, 'Galar'),
(841, 'Drapfel', 'Pflanze', 'Drache', 100, 85, NULL, 'Galar'),
(842, 'Schlapfel', 'Pflanze', 'Drache', 150, 100, NULL, 'Galar'),
(843, 'Salanga', 'Boden', NULL, 70, 70, NULL, 'Galar'),
(844, 'Sanaconda', 'Boden', NULL, 110, 120, NULL, 'Galar'),
(845, 'Urgl', 'Flug', 'Wasser', 65, 58, NULL, 'Galar'),
(846, 'Pikuda', 'Wasser', NULL, 125, 98, NULL, 'Galar'),
(847, 'Barrakiefa', 'Wasser', NULL, 145, 118, NULL, 'Galar'),
(848, 'Toxel', 'Elektro', 'Gift', 40, 40, NULL, 'Galar'),
(849, 'Riffex', 'Elektro', 'Gift', 80, 75, NULL, 'Galar'),
(850, 'Thermopod', 'Feuer', 'Käfer', 120, 100, NULL, 'Galar'),
(851, 'Infernopod', 'Feuer', 'Käfer', 150, 130, NULL, 'Galar'),
(852, 'Klopptopus', 'Kampf', NULL, 75, 50, NULL, 'Galar'),
(853, 'Kaocto', 'Kampf', NULL, 125, 75, NULL, 'Galar'),
(854, 'Fatalitee', 'Geist', NULL, 65, 70, NULL, 'Galar'),
(855, 'Mortipot', 'Geist', NULL, 105, 90, NULL, 'Galar'),
(856, 'Brimova', 'Psycho', NULL, 55, 60, NULL, 'Galar'),
(857, 'Brimano', 'Psycho', NULL, 75, 80, NULL, 'Galar'),
(858, 'Silembrim', 'Psycho', 'Fee', 115, 100, NULL, 'Galar'),
(859, 'Bähmon', 'Unlicht', 'Fee', 60, 90, NULL, 'Galar'),
(860, 'Pelzebub', 'Unlicht', 'Fee', 95, 110, NULL, 'Galar'),
(861, 'Olangaar', 'Unlicht', 'Fee', 130, 110, NULL, 'Galar'),
(862, 'Barrikadax', 'Unlicht', 'Normal', 140, 125, NULL, 'Galar'),
(863, 'Mauzinger', 'Stahl', NULL, 90, 70, NULL, 'Galar'),
(864, 'Gorgasonn', 'Gestein', NULL, 115, 95, NULL, 'Galar'),
(865, 'Lauchzelot', 'Kampf', NULL, 85, 70, NULL, 'Galar'),
(866, 'Pantifrost', 'Eis', 'Psycho', 65, 70, NULL, 'Galar'),
(867, 'Oghnatoll', 'Boden', 'Geist', 145, 110, NULL, 'Galar'),
(868, 'Hokumil', 'Fee', NULL, 75, 70, NULL, 'Galar'),
(869, 'Pokusan', 'Fee', NULL, 100, 95, NULL, 'Galar'),
(870, 'Legios', 'Kampf', NULL, 105, 95, NULL, 'Galar'),
(871, 'Britzigel', 'Elektro', NULL, 65, 48, NULL, 'Galar'),
(872, 'Snomnom', 'Eis', 'Käfer', 30, 30, NULL, 'Galar'),
(873, 'Mottineva', 'Eis', 'Käfer', 125, 70, NULL, 'Galar'),
(874, 'Humanolith', 'Gestein', NULL, 77, 70, NULL, 'Galar'),
(875, 'Kubuin', 'Eis', NULL, 121, 100, NULL, 'Galar'),
(876, 'Servol', 'Psycho', 'Normal', 95, 70, NULL, 'Galar'),
(877, 'Morpeko', 'Elektro', 'Unlicht', 95, 58, NULL, 'Galar'),
(878, 'Kupfanti', 'Stahl', NULL, 60, 50, NULL, 'Galar'),
(879, 'Patinaraja', 'Stahl', NULL, 95, 80, NULL, 'Galar'),
(880, 'Lectragon', 'Elektro', 'Drache', 100, 75, NULL, 'Galar'),
(881, 'Lecryodon', 'Elektro', 'Eis', 130, 110, NULL, 'Galar'),
(882, 'Pescragon', 'Wasser', 'Drache', 60, 65, NULL, 'Galar'),
(883, 'Pescryodon', 'Wasser', 'Eis', 105, 95, NULL, 'Galar'),
(884, 'Duraludon', 'Stahl', 'Drache', 120, 70, NULL, 'Galar'),
(885, 'Grolldra', 'Drache', 'Geist', 65, 60, NULL, 'Galar'),
(886, 'Phandra', 'Drache', 'Geist', 100, 95, NULL, 'Galar'),
(887, 'Katapuldra', 'Drache', 'Geist', 125, 110, NULL, 'Galar'),
(888, 'Zacian', 'Fee', NULL, 170, 92, NULL, 'Galar'),
(889, 'Zamazenta', 'Kampf', NULL, 170, 92, NULL, 'Galar'),
(890, 'Endynalos', 'Gift', 'Drache', 85, 70, NULL, 'Galar'),
(891, 'Dakuma', 'Kampf', NULL, 105, 85, NULL, 'Galar'),
(892, 'Wulaosu', 'Kampf', 'Unlicht', 120, 95, NULL, 'Galar'),
(893, 'Zarude', 'Unlicht', 'Pflanze', 120, 105, NULL, 'Galar'),
(894, 'Regieleki', 'Elektro', NULL, 100, 80, NULL, 'Galar'),
(895, 'Regidrago', 'Drache', NULL, 100, 200, NULL, 'Galar'),
(896, 'Polaross', 'Eis', NULL, 80, 70, NULL, 'Galar'),
(897, 'Phantoross', 'Geist', NULL, 120, 100, NULL, 'Galar'),
(898, 'Coronospa', 'Psycho', 'Pflanze', 80, 80, NULL, 'Galar'),
(899, 'Damythir', 'Normal', 'Psycho', 120, 80, NULL, 'Galar'),
(900, 'Axantor', 'Käfer', 'Gestein', 90, 90, NULL, 'Galar'),
(901, 'Ursaluna', 'Boden', 'Normal', 100, 120, NULL, 'Galar'),
(902, 'Salmagnis', 'Wasser', 'Geist', 110, 110, NULL, 'Galar'),
(903, 'Snieboss', 'Kampf', 'Gift', 145, 95, NULL, 'Galar'),
(904, 'Myriador', 'Unlicht', 'Gift', 85, 100, NULL, 'Galar'),
(905, 'Cupidos', 'Fee', 'Flug', 90, 75, NULL, 'Galar');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_hoenn`
--

CREATE TABLE `tbl_hoenn` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_hoenn`
--

INSERT INTO `tbl_hoenn` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(252, 'Geckarbor', 'Pflanze', NULL, 40, 45, 'Hoenn'),
(253, 'Reptain', 'Pflanze', NULL, 60, 60, 'Hoenn'),
(254, 'Gewaldro', 'Pflanze', NULL, 110, 80, 'Hoenn'),
(255, 'Flemmli', 'Feuer', NULL, 45, 38, 'Hoenn'),
(256, 'Jungglut', 'Feuer', 'Kampf', 60, 58, 'Hoenn'),
(257, 'Lohgock', 'Feuer', 'Kampf', 120, 78, 'Hoenn'),
(258, 'Hydropi', 'Wasser', NULL, 40, 40, 'Hoenn'),
(259, 'Moorabbel', 'Wasser', 'Boden', 50, 50, 'Hoenn'),
(260, 'Sumpex', 'Wasser', 'Boden', 110, 70, 'Hoenn'),
(261, 'Fiffyen', 'Unlicht', NULL, 60, 45, 'Hoenn'),
(262, 'Magnayen', 'Unlicht', NULL, 85, 70, 'Hoenn'),
(263, 'Zigzachs', 'Normal', NULL, 30, 38, 'Hoenn'),
(264, 'Geradaks', 'Normal', NULL, 70, 78, 'Hoenn'),
(265, 'Waumpel', 'Käfer', NULL, 35, 45, 'Hoenn'),
(266, 'Schaloko', 'Käfer', NULL, 55, 50, 'Hoenn'),
(267, 'Papinella', 'Käfer', 'Flug', 45, 60, 'Hoenn'),
(268, 'Panekon', 'Käfer', NULL, 70, 70, 'Hoenn'),
(269, 'Pudox', 'Käfer', 'Gift', 60, 60, 'Hoenn'),
(270, 'Loturzel', 'Wasser', 'Pflanze', 40, 40, 'Hoenn'),
(271, 'Lombrero', 'Wasser', 'Pflanze', 50, 60, 'Hoenn'),
(272, 'Kappalores', 'Wasser', 'Pflanze', 70, 80, 'Hoenn'),
(273, 'Samurzel', 'Pflanze', NULL, 70, 50, 'Hoenn'),
(274, 'Blanas', 'Pflanze', 'Unlicht', 60, 70, 'Hoenn'),
(275, 'Tengulist', 'Pflanze', 'Unlicht', 90, 60, 'Hoenn'),
(276, 'Schwalbini', 'Normal', 'Flug', 60, 40, 'Hoenn'),
(277, 'Schwalboss', 'Normal', 'Flug', 85, 60, 'Hoenn'),
(278, 'Wingull', 'Wasser', 'Flug', 30, 40, 'Hoenn'),
(279, 'Pelipper', 'Wasser', 'Flug', 50, 60, 'Hoenn'),
(280, 'Trasla', 'Psycho', 'Fee', 35, 28, 'Hoenn'),
(281, 'Kirlia', 'Psycho', 'Fee', 65, 38, 'Hoenn'),
(282, 'Gardevoir', 'Psycho', 'Fee', 65, 68, 'Hoenn'),
(283, 'Geweiher', 'Käfer', 'Wasser', 60, 40, 'Hoenn'),
(284, 'Maskeregen', 'Käfer', 'Flug', 70, 60, 'Hoenn'),
(285, 'Knilz', 'Pflanze', NULL, 40, 45, 'Hoenn'),
(286, 'Kapilz', 'Pflanze', 'Kampf', 60, 60, 'Hoenn'),
(287, 'Bummelz', 'Normal', NULL, 60, 40, 'Hoenn'),
(288, 'Muntier', 'Normal', NULL, 60, 70, 'Hoenn'),
(289, 'Letarking', 'Normal', NULL, 160, 100, 'Hoenn'),
(290, 'Nincada', 'Käfer', 'Boden', 45, 31, 'Hoenn'),
(291, 'Ninjask', 'Käfer', 'Flug', 90, 61, 'Hoenn'),
(292, 'Ninjatom', 'Käfer', 'Geist', 90, 1, 'Hoenn'),
(293, 'Flurmel', 'Normal', NULL, 45, 40, 'Hoenn'),
(294, 'Krakeelo', 'Normal', NULL, 60, 70, 'Hoenn'),
(295, 'Krawumms', 'Normal', NULL, 80, 90, 'Hoenn'),
(296, 'Makuhita', 'Kampf', NULL, 60, 72, 'Hoenn'),
(297, 'Hariyama', 'Kampf', NULL, 120, 144, 'Hoenn'),
(298, 'Azurill', 'Normal', 'Fee', 20, 50, 'Hoenn'),
(299, 'Nasgnet', 'Gestein', NULL, 45, 60, 'Hoenn'),
(300, 'Eneco', 'Normal', NULL, 70, 60, 'Hoenn'),
(301, 'Enekoro', 'Normal', NULL, 105, 70, 'Hoenn'),
(302, 'Zobiris', 'Unlicht', 'Geist', 80, 60, 'Hoenn'),
(303, 'Flunkifer', 'Stahl', 'Fee', 160, 60, 'Hoenn'),
(304, 'Stollunior', 'Stahl', 'Gestein', 50, 50, 'Hoenn'),
(305, 'Stollrak', 'Stahl', 'Gestein', 70, 70, 'Hoenn'),
(306, 'Stolloss', 'Stahl', 'Gestein', 110, 100, 'Hoenn'),
(307, 'Meditie', 'Kampf', 'Psycho', 40, 30, 'Hoenn'),
(308, 'Meditalis', 'Kampf', 'Psycho', 60, 60, 'Hoenn'),
(309, 'Frizelbliz', 'Elektro', NULL, 45, 45, 'Hoenn'),
(310, 'Voltenso', 'Elektro', NULL, 75, 70, 'Hoenn'),
(311, 'Plusle', 'Elektro', NULL, 50, 60, 'Hoenn'),
(312, 'Minun', 'Elektro', NULL, 50, 60, 'Hoenn'),
(313, 'Volbeat', 'Käfer', NULL, 73, 65, 'Hoenn'),
(314, 'Illumise', 'Käfer', NULL, 53, 65, 'Hoenn'),
(315, 'Roselia', 'Pflanze', 'Gift', 60, 50, 'Hoenn'),
(316, 'Schluppuck', 'Gift', NULL, 43, 48, 'Hoenn'),
(317, 'Schlukwech', 'Gift', NULL, 73, 68, 'Hoenn'),
(318, 'Kanivanha', 'Wasser', 'Unlicht', 105, 45, 'Hoenn'),
(319, 'Tohaido', 'Wasser', 'Unlicht', 150, 70, 'Hoenn'),
(320, 'Wailmer', 'Wasser', NULL, 70, 130, 'Hoenn'),
(321, 'Wailord', 'Wasser', NULL, 90, 170, 'Hoenn'),
(322, 'Camaub', 'Feuer', 'Boden', 40, 50, 'Hoenn'),
(323, 'Camerupt', 'Feuer', 'Boden', 100, 70, 'Hoenn'),
(324, 'Qurtel', 'Feuer', NULL, 100, 70, 'Hoenn'),
(325, 'Spoink', 'Psycho', NULL, 25, 60, 'Hoenn'),
(326, 'Groink', 'Psycho', NULL, 45, 80, 'Hoenn'),
(327, 'Pandir', 'Normal', NULL, 60, 126, 'Hoenn'),
(328, 'Knacklion', 'Boden', NULL, 70, 50, 'Hoenn'),
(329, 'Vibrava', 'Boden', 'Drache', 70, 50, 'Hoenn'),
(330, 'Libelldra', 'Boden', 'Drache', 80, 80, 'Hoenn'),
(331, 'Tuska', 'Pflanze', NULL, 60, 40, 'Hoenn'),
(332, 'Noktuska', 'Pflanze', 'Unlicht', 80, 60, 'Hoenn'),
(333, 'Wablu', 'Normal', 'Flug', 40, 45, 'Hoenn'),
(334, 'Altaria', 'Drache', 'Flug', 70, 75, 'Hoenn'),
(335, 'Sengo', 'Normal', NULL, 50, 50, 'Hoenn'),
(336, 'Vipitis', 'Gift', NULL, 82, 80, 'Hoenn'),
(337, 'Lunastein', 'Gestein', 'Psycho', 90, 70, 'Hoenn'),
(338, 'Sonnfel', 'Gestein', 'Psycho', 90, 70, 'Hoenn'),
(339, 'Schmerbe', 'Wasser', 'Boden', 60, 30, 'Hoenn'),
(340, 'Welsar', 'Wasser', 'Boden', 80, 90, 'Hoenn'),
(341, 'Krebscorps', 'Wasser', NULL, 30, 30, 'Hoenn'),
(342, 'Krebutack', 'Wasser', 'Unlicht', 120, 60, 'Hoenn'),
(343, 'Puppance', 'Boden', 'Psycho', 80, 50, 'Hoenn'),
(344, 'Lepumentas', 'Boden', 'Psycho', 100, 70, 'Hoenn'),
(345, 'Liliep', 'Gestein', 'Pflanze', 64, 66, 'Hoenn'),
(346, 'Wielie', 'Gestein', 'Pflanze', 105, 121, 'Hoenn'),
(347, 'Anorith', 'Gestein', 'Käfer', 95, 45, 'Hoenn'),
(348, 'Armaldo', 'Gestein', 'Käfer', 125, 75, 'Hoenn'),
(349, 'Barschwa', 'Wasser', NULL, 20, 20, 'Hoenn'),
(350, 'Milotic', 'Wasser', NULL, 60, 95, 'Hoenn'),
(351, 'Formeo', 'Normal', NULL, 40, 70, 'Hoenn'),
(352, 'Kecleon', 'Normal', NULL, 90, 60, 'Hoenn'),
(353, 'Shuppet', 'Geist', NULL, 75, 44, 'Hoenn'),
(354, 'Banette', 'Geist', NULL, 115, 64, 'Hoenn'),
(355, 'Zwirrlicht', 'Geist', NULL, 30, 40, 'Hoenn'),
(356, 'Zwirrklop', 'Geist', NULL, 50, 60, 'Hoenn'),
(357, 'Tropius', 'Pflanze', 'Flug', 68, 99, 'Hoenn'),
(358, 'Palimpalim', 'Psycho', NULL, 50, 50, 'Hoenn'),
(359, 'Absol', 'Unlicht', NULL, 130, 65, 'Hoenn'),
(360, 'Isso', 'Psycho', NULL, 50, 50, 'Hoenn'),
(361, 'Schneppke', 'Eis', NULL, 60, 70, 'Hoenn'),
(362, 'Firnontor', 'Eis', NULL, 80, 80, 'Hoenn'),
(363, 'Seemops', 'Eis', 'Wasser', 60, 70, 'Hoenn'),
(364, 'Seejong', 'Eis', 'Wasser', 50, 70, 'Hoenn'),
(365, 'Walraisa', 'Eis', 'Wasser', 80, 90, 'Hoenn'),
(366, 'Perlu', 'Wasser', NULL, 64, 40, 'Hoenn'),
(367, 'Aalabyss', 'Wasser', NULL, 84, 55, 'Hoenn'),
(368, 'Saganabyss', 'Wasser', NULL, 104, 55, 'Hoenn'),
(369, 'Relicanth', 'Wasser', 'Gestein', 90, 100, 'Hoenn'),
(370, 'Liebiskus', 'Wasser', NULL, 30, 43, 'Hoenn'),
(371, 'Kindwurm', 'Drache', NULL, 60, 45, 'Hoenn'),
(372, 'Draschel', 'Drache', NULL, 70, 65, 'Hoenn'),
(373, 'Brutalanda', 'Drache', 'Flug', 100, 95, 'Hoenn'),
(374, 'Tanhel', 'Stahl', 'Psyycho', 40, 40, 'Hoenn'),
(375, 'Metang', 'Stahl', 'Psycho', 60, 60, 'Hoenn'),
(376, 'Metagross', 'Stahl', 'Psycho', 120, 80, 'Hoenn'),
(377, 'Regirock', 'Gestein', NULL, 100, 80, 'Hoenn'),
(378, 'Regice', 'Eis', NULL, 100, 80, 'Hoenn'),
(379, 'Registeel', 'Stahl', NULL, 75, 80, 'Hoenn'),
(380, 'Latias', 'Drache', 'Psycho', 80, 80, 'Hoenn'),
(381, 'Latios', 'Drache', 'Psycho', 90, 80, 'Hoenn'),
(382, 'Kyogre', 'Wasser', NULL, 150, 100, 'Hoenn'),
(383, 'Groudon', 'Boden', NULL, 150, 100, 'Hoenn'),
(384, 'Rayquaza', 'Drache', 'Flug', 150, 105, 'Hoenn'),
(385, 'Jirachi', 'Stahl', 'Psycho', 100, 100, 'Hoenn'),
(386, 'Deoxys', 'Psycho', NULL, 150, 50, 'Hoenn');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_johto`
--

CREATE TABLE `tbl_johto` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_johto`
--

INSERT INTO `tbl_johto` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(152, 'Endivie', 'Pflanze', NULL, 49, 45, 'Johto'),
(153, 'Lorblatt', 'Pflanze', NULL, 62, 60, 'Johto'),
(154, 'Meganie', 'Pflanze', NULL, 82, 80, 'Johto'),
(155, 'Feurigel', 'Feuer', NULL, 52, 39, 'Johto'),
(156, 'Igelavar', 'Feuer', NULL, 64, 58, 'Johto'),
(157, 'Tornupto', 'Feuer', NULL, 84, 78, 'Johto'),
(158, 'Karnimani', 'Wasser', NULL, 65, 50, 'Johto'),
(159, 'Tyracroc', 'Wasser', NULL, 80, 65, 'Johto'),
(160, 'Impergator', 'Wasser', NULL, 105, 85, 'Johto'),
(161, 'Wiesor', 'Normal', NULL, 30, 45, 'Johto'),
(162, 'Wiesenior', 'Normal', NULL, 70, 60, 'Johto'),
(163, 'Hoothoot', 'Normal', 'Flug', 30, 60, 'Johto'),
(164, 'Noctuh', 'Normal', 'Flug', 50, 100, 'Johto'),
(165, 'Ledyba', 'Käfer', 'Flug', 20, 40, 'Johto'),
(166, 'Ledian', 'Käfer', 'Flug', 35, 55, 'Johto'),
(167, 'Webarak', 'Käfer', 'Gift', 70, 60, 'Johto'),
(168, 'Ariados', 'Käfer', 'Gift', 90, 70, 'Johto'),
(169, 'Iksbat', 'Gift', 'Flug', 80, 85, 'Johto'),
(170, 'Lampi', 'Wasser', 'Elektro', 40, 45, 'Johto'),
(171, 'Lanturn', 'Wasser', 'Elektro', 58, 125, 'Johto'),
(172, 'Pichu', 'Elektro', NULL, 40, 20, 'Johto'),
(173, 'Pii', 'Fee', NULL, 25, 40, 'Johto'),
(174, 'Fluffeluff', 'Normal', 'Fee', 45, 50, 'Johto'),
(175, 'Togepi', 'Fee', NULL, 20, 35, 'Johto'),
(176, 'Togetic', 'Fee', 'Flug', 40, 55, 'Johto'),
(177, 'Natu', 'Psycho', 'Flug', 50, 40, 'Johto'),
(178, 'Xatu', 'Psycho', 'Flug', 75, 65, 'Johto'),
(179, 'Voltilamm', 'Elektro', NULL, 55, 55, 'Johto'),
(180, 'Waaty', 'Elektro', NULL, 70, 70, 'Johto'),
(181, 'Ampharos', 'Elektro', NULL, 75, 90, 'Johto'),
(182, 'Blubella', 'Pflanze', NULL, 75, 70, 'Johto'),
(183, 'Marill', 'Wasser', 'Fee', 20, 70, 'Johto'),
(184, 'Azumarill', 'Wasser', 'Fee', 50, 100, 'Johto'),
(185, 'Mogelbaum', 'Gestein', NULL, 55, 60, 'Johto'),
(186, 'Quaxo', 'Wasser', NULL, 65, 65, 'Johto'),
(187, 'Hoppspross', 'Pflanze', 'Flug', 40, 50, 'Johto'),
(188, 'Hubelupf', 'Pflanze', 'Flug', 55, 70, 'Johto'),
(189, 'Papungha', 'Pflanze', 'Flug', 75, 90, 'Johto'),
(190, 'Griffel', 'Normal', NULL, 60, 55, 'Johto'),
(191, 'Sonnkern', 'Pflanze', NULL, 30, 30, 'Johto'),
(192, 'Sonnflora', 'Pflanze', NULL, 75, 75, 'Johto'),
(193, 'Yanma', 'Käfer', 'Flug', 65, 65, 'Johto'),
(194, 'Felino', 'Wasser', 'Boden', 70, 50, 'Johto'),
(195, 'Morlord', 'Wasser', 'Boden', 90, 70, 'Johto'),
(196, 'Psiana', 'Psycho', NULL, 65, 65, 'Johto'),
(197, 'Nachtara', 'Unlicht', NULL, 65, 65, 'Johto'),
(198, 'Kramurx', 'Unlicht', 'Flug', 50, 43, 'Johto'),
(199, 'Laschoking', 'Wasser', 'Psycho', 65, 80, 'Johto'),
(200, 'Traunfugil', 'Geist', NULL, 40, 40, 'Johto'),
(201, 'Icognito', 'Psycho', NULL, 60, 60, 'Johto'),
(202, 'Woingenau', 'Psycho', NULL, 90, 70, 'Johto'),
(203, 'Girafarig', 'Normal', 'Psycho', 80, 70, 'Johto'),
(204, 'Tannza', 'Käfer', NULL, 80, 35, 'Johto'),
(205, 'Forstellka', 'Käfer', 'Stahl', 90, 75, 'Johto'),
(206, 'Dummisel', 'Normal', NULL, 50, 43, 'Johto'),
(207, 'Skorgla', 'Boden', 'Flug', 90, 70, 'Johto'),
(208, 'Stahlos', 'Stahl', 'Boden', 120, 70, 'Johto'),
(209, 'Snubbull', 'Fee', NULL, 80, 60, 'Johto'),
(210, 'Granbull', 'Fee', NULL, 120, 90, 'Johto'),
(211, 'Baldorfish', 'Wasser', 'Gift', 55, 45, 'Johto'),
(212, 'Scherox', 'Käfer', 'Stahl', 130, 70, 'Johto'),
(213, 'Pottrott', 'Käfer', 'Gestein', 110, 100, 'Johto'),
(214, 'Skaraborn', 'Käfer', 'Kampf', 125, 65, 'Johto'),
(215, 'Sniebel', 'Unlicht', 'Eis', 90, 70, 'Johto'),
(216, 'Teddiursa', 'Normal', NULL, 80, 60, 'Johto'),
(217, 'Ursaring', 'Normal', NULL, 130, 90, 'Johto'),
(218, 'Schneckmag', 'Feuer', NULL, 50, 40, 'Johto'),
(219, 'Magcargo', 'Feuer', 'Gestein', 50, 60, 'Johto'),
(220, 'Quiekel', 'Eis', 'Boden', 40, 50, 'Johto'),
(221, 'Keifel', 'Eis', 'Boden', 70, 100, 'Johto'),
(222, 'Corasonn', 'Wasser', 'Gestein', 64, 70, 'Johto'),
(223, 'Remoraid', 'Wasser', NULL, 65, 35, 'Johto'),
(224, 'Octillery', 'Wasser', NULL, 105, 75, 'Johto'),
(225, 'Botogel', 'Eis', 'Flug', 60, 50, 'Johto'),
(226, 'Mantax', 'Wasser', 'Flug', 70, 60, 'Johto'),
(227, 'Panzaeron', 'Stahl', 'Flug', 80, 100, 'Johto'),
(228, 'Hunduster', 'Unlicht', 'Feuer', 60, 50, 'Johto'),
(229, 'Hundemon', 'Unlicht', 'Feuer', 90, 70, 'Johto'),
(230, 'Seedraking', 'Wasser', 'Drache', 95, 85, 'Johto'),
(231, 'Phanpy', 'Boden', NULL, 60, 90, 'Johto'),
(232, 'Donphan', 'Boden', NULL, 120, 90, 'Johto'),
(233, 'Porygon2', 'Normal', NULL, 80, 85, 'Johto'),
(234, 'Damhirplex', 'Normal', NULL, 120, 95, 'Johto'),
(235, 'Farbeagle', 'Normal', NULL, 60, 65, 'Johto'),
(236, 'Rabauz', 'Kampf', NULL, 125, 64, 'Johto'),
(237, 'Kapoera', 'Kampf', NULL, 120, 95, 'Johto'),
(238, 'Kussilla', 'Eis', 'Psycho', 70, 70, 'Johto'),
(239, 'Elekid', 'Elektro', NULL, 63, 45, 'Johto'),
(240, 'Magby', 'Feuer', NULL, 75, 45, 'Johto'),
(241, 'Miltank', 'Normal', NULL, 80, 95, 'Johto'),
(242, 'Heiteira', 'Normal', NULL, 70, 255, 'Johto'),
(243, 'Raikou', 'Elektro', NULL, 115, 90, 'Johto'),
(244, 'Entei', 'Feuer', NULL, 115, 115, 'Johto'),
(245, 'Suicune', 'Wasser', NULL, 100, 100, 'Johto'),
(246, 'Larvitar', 'Gestein', 'Boden', 64, 50, 'Johto'),
(247, 'Pupitar', 'Gestein', 'Boden', 84, 70, 'Johto'),
(248, 'Despotar', 'Gestein', 'Unlicht', 134, 100, 'Johto'),
(249, 'Lugia', 'Psycho', 'Flug', 90, 154, 'Johto'),
(250, 'Ho-Oh', 'Feuer', 'Flug', 130, 154, 'Johto'),
(251, 'Celebi', 'Psycho', 'Pflanze', 100, 100, 'Johto');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kalos`
--

CREATE TABLE `tbl_kalos` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_kalos`
--

INSERT INTO `tbl_kalos` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(650, 'Igamaro', 'Pflanze', NULL, 49, 45, 'Kalos'),
(651, 'Igastarnish', 'Pflanze', NULL, 62, 60, 'Kalos'),
(652, 'Brigaron', 'Pflanze', 'Kampf', 100, 78, 'Kalos'),
(653, 'Fynx', 'Feuer', NULL, 45, 40, 'Kalos'),
(654, 'Rutena', 'Feuer', NULL, 59, 59, 'Kalos'),
(655, 'Fennexis', 'Feuer', 'Psycho', 69, 75, 'Kalos'),
(656, 'Froxy', 'Wasser', NULL, 56, 41, 'Kalos'),
(657, 'Amphizel', 'Wasser', NULL, 63, 54, 'Kalos'),
(658, 'Quajutsu', 'Wasser', 'Unlicht', 95, 72, 'Kalos'),
(659, 'Scoppel', 'Normal', NULL, 50, 45, 'Kalos'),
(660, 'Grebbit', 'Normal', 'Boden', 93, 74, 'Kalos'),
(661, 'Dartiri', 'Normal', 'Flug', 35, 41, 'Kalos'),
(662, 'Dartignis', 'Feuer', 'Flug', 50, 64, 'Kalos'),
(663, 'Fiaro', 'Feuer', 'Flug', 65, 69, 'Kalos'),
(664, 'Purmel', 'Käfer', NULL, 22, 38, 'Kalos'),
(665, 'Puponcho', 'Käfer', NULL, 52, 72, 'Kalos'),
(666, 'Vivillon', 'Käfer', 'Flug', 52, 80, 'Kalos'),
(667, 'Leufeo', 'Feuer', 'Normal', 43, 62, 'Kalos'),
(668, 'Pyroleo', 'Feuer', 'Normal', 68, 78, 'Kalos'),
(669, 'Flabébé', 'Fee', NULL, 38, 44, 'Kalos'),
(670, 'Floette', 'Fee', NULL, 45, 54, 'Kalos'),
(671, 'Florges', 'Fee', NULL, 65, 78, 'Kalos'),
(672, 'Mähikel', 'Pflanze', NULL, 36, 67, 'Kalos'),
(673, 'Chevrumm', 'Pflanze', NULL, 56, 95, 'Kalos'),
(674, 'Pam-Pam', 'Kampf', NULL, 125, 78, 'Kalos'),
(675, 'Pandagro', 'Kampf', 'Unlicht', 125, 95, 'Kalos'),
(676, 'Coiffwaff', 'Normal', NULL, 75, 101, 'Kalos'),
(677, 'Psiau', 'Psycho', NULL, 65, 62, 'Kalos'),
(678, 'Psiaugon', 'Psycho', NULL, 85, 82, 'Kalos'),
(679, 'Gramokles', 'Stahl', 'Geist', 89, 57, 'Kalos'),
(680, 'Doukles', 'Stahl', 'Geist', 110, 67, 'Kalos'),
(681, 'Durengard', 'Stahl', 'Geist', 150, 67, 'Kalos'),
(682, 'Parfi', 'Fee', NULL, 52, 62, 'Kalos'),
(683, 'Parfinesse', 'Fee', NULL, 72, 82, 'Kalos'),
(684, 'Flauschling', 'Fee', NULL, 45, 54, 'Kalos'),
(685, 'Sabbaione', 'Fee', NULL, 80, 86, 'Kalos'),
(686, 'Iscalar', 'Unlicht', 'Psycho', 53, 62, 'Kalos'),
(687, 'Calamanero', 'Unlicht', 'Psycho', 73, 82, 'Kalos'),
(688, 'Bithora', 'Gestein', 'Wasser', 73, 92, 'Kalos'),
(689, 'Thanathora', 'Gestein', 'Wasser', 92, 112, 'Kalos'),
(690, 'Algitt', 'Gift', 'Wasser', 53, 50, 'Kalos'),
(691, 'Tandrak', 'Gift', 'Drache', 73, 68, 'Kalos'),
(692, 'Scampisto', 'Wasser', NULL, 98, 88, 'Kalos'),
(693, 'Wummer', 'Wasser', NULL, 65, 85, 'Kalos'),
(694, 'Eguana', 'Elektro', 'Normal', 85, 105, 'Kalos'),
(695, 'Elezard', 'Elektro', 'Normal', 62, 58, 'Kalos'),
(696, 'Balgoras', 'Gestein', 'Drache', 78, 78, 'Kalos'),
(697, 'Monargoras', 'Gestein', 'Drache', 104, 78, 'Kalos'),
(698, 'Amarino', 'Gestein', 'Eis', 36, 44, 'Kalos'),
(699, 'Amagarga', 'Gestein', 'Eis', 52, 67, 'Kalos'),
(700, 'Feelinara', 'Fee', NULL, 45, 55, 'Kalos'),
(701, 'Resladero', 'Kampf', 'Flug', 65, 70, 'Kalos'),
(702, 'Dedenne', 'Elektro', 'Fee', 58, 67, 'Kalos'),
(703, 'Rocara', 'Gestein', 'Fee', 89, 65, 'Kalos'),
(704, 'Viscora', 'Drache', NULL, 50, 65, 'Kalos'),
(705, 'Viscargot', 'Drache', NULL, 65, 100, 'Kalos'),
(706, 'Viscogon', 'Drache', NULL, 105, 115, 'Kalos'),
(707, 'Clavion', 'Stahl', 'Fee', 80, 57, 'Kalos'),
(708, 'Paragoni', 'Geist', 'Pflanze', 110, 67, 'Kalos'),
(709, 'Trombork', 'Geist', 'Pflanze', 91, 95, 'Kalos'),
(710, 'Irrbis', 'Geist', 'Pflanze', 58, 44, 'Kalos'),
(711, 'Pumpdjinn', 'Geist', 'Pflanze', 80, 54, 'Kalos'),
(712, 'Arktip', 'Eis', NULL, 60, 58, 'Kalos'),
(713, 'Arktilas', 'Eis', NULL, 110, 72, 'Kalos'),
(714, 'eF-eM', 'Flug', 'Drache', 80, 122, 'Kalos'),
(715, 'UHaFnir', 'Flug', 'Drache', 90, 95, 'Kalos'),
(716, 'Xerneas', 'Fee', NULL, 131, 126, 'Kalos'),
(717, 'Yveltal', 'Unlicht', 'Flug', 131, 126, 'Kalos'),
(718, 'Zygarde', 'Drache', 'Boden', 100, 115, 'Kalos'),
(719, 'Diancie', 'Gestein', 'Fee', 100, 50, 'Kalos'),
(720, 'Hoopa', 'Psycho', 'Geist', 110, 80, 'Kalos'),
(721, 'Volcanion', 'Feuer', 'Wasser', 110, 80, 'Kalos');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kanto`
--

CREATE TABLE `tbl_kanto` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_kanto`
--

INSERT INTO `tbl_kanto` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(1, 'Bisasam', 'Pflanze', 'Gift', 49, 45, 'Kanto'),
(2, 'Bisaknosp', 'Pflanze', 'Gift', 62, 60, 'Kanto'),
(3, 'Bisaflor', 'Pflanze', 'Gift', 82, 80, 'Kanto'),
(4, 'Glumanda', 'Feuer', NULL, 52, 39, 'Kanto'),
(5, 'Glutexo', 'Feuer', NULL, 64, 58, 'Kanto'),
(6, 'Glurak', 'Feuer', 'Flug', 84, 78, 'Kanto'),
(7, 'Schiggy', 'Wasser', NULL, 48, 44, 'Kanto'),
(8, 'Schillok', 'Wasser', NULL, 63, 59, 'Kanto'),
(9, 'Turtok', 'Wasser', NULL, 83, 79, 'Kanto'),
(10, 'Raupy', 'Käfer', NULL, 30, 40, 'Kanto'),
(11, 'Safcon', 'Käfer', NULL, 55, 40, 'Kanto'),
(12, 'Smettbo', 'Käfer', 'Flug', 90, 60, 'Kanto'),
(13, 'Hornliu', 'Käfer', 'Gift', 35, 30, 'Kanto'),
(14, 'Kokuna', 'Käfer', 'Gift', 25, 50, 'Kanto'),
(15, 'Bibor', 'Käfer', 'Gift', 150, 65, 'Kanto'),
(16, 'Taubsi', 'Normal', 'Flug', 45, 40, 'Kanto'),
(17, 'Tauboga', 'Normal', 'Flug', 60, 55, 'Kanto'),
(18, 'Tauboss', 'Normal', 'Flug', 80, 80, 'Kanto'),
(19, 'Rattfratz', 'Normal', NULL, 56, 30, 'Kanto'),
(20, 'Rattikarl', 'Normal', NULL, 71, 55, 'Kanto'),
(21, 'Habitak', 'Normal', 'Flug', 60, 40, 'Kanto'),
(22, 'Ibitak', 'Normal', 'Flug', 65, 40, 'Kanto'),
(23, 'Rettan', 'Normal', NULL, 60, 55, 'Kanto'),
(24, 'Arbok', 'Gift', NULL, 85, 60, 'Kanto'),
(25, 'Pikachu', 'Elektro', NULL, 55, 35, 'Kanto'),
(26, 'Raichu', 'Elektro', NULL, 90, 60, 'Kanto'),
(27, 'Sandan', 'Boden', NULL, 75, 50, 'Kanto'),
(28, 'Sandamer', 'Boden', NULL, 100, 75, 'Kanto'),
(29, 'Nidoran♀', 'Gift', NULL, 47, 55, 'Kanto'),
(30, 'Nidorina', 'Gift', NULL, 62, 70, 'Kanto'),
(31, 'Nidoqueen', 'Gift', 'Boden', 92, 90, 'Kanto'),
(32, 'Nidoran♂', 'Gift', NULL, 57, 46, 'Kanto'),
(33, 'Nidorino', 'Gift', NULL, 72, 61, 'Kanto'),
(34, 'Nidoking', 'Gift', 'Boden', 102, 81, 'Kanto'),
(35, 'Piepi', 'Fee', NULL, 40, 35, 'Kanto'),
(36, 'Pixi', 'Fee', NULL, 70, 60, 'Kanto'),
(37, 'Vulpix', 'Feuer', NULL, 41, 38, 'Kanto'),
(38, 'Vulnona', 'Feuer', NULL, 76, 73, 'Kanto'),
(39, 'Pummeluff', 'Normal', 'Fee', 45, 140, 'Kanto'),
(40, 'Knuddeluff', 'Normal', 'Fee', 70, 185, 'Kanto'),
(41, 'Zubat', 'Gift', 'Flug', 45, 40, 'Kanto'),
(42, 'Golbat', 'Gift', 'Flug', 80, 75, 'Kanto'),
(43, 'Myrapla', 'Pflanze', 'Gift', 35, 45, 'Kanto'),
(44, 'Duflor', 'Pflanze', 'Gift', 65, 60, 'Kanto'),
(45, 'Giflor', 'Pflanze', 'Gift', 80, 75, 'Kanto'),
(46, 'Paras', 'Käfer', 'Pflanze', 70, 35, 'Kanto'),
(47, 'Parasek', 'Käfer', 'Pflanze', 95, 60, 'Kanto'),
(48, 'Bluzuk', 'Käfer', 'Gift', 55, 40, 'Kanto'),
(49, 'Omot', 'Käfer', 'Gift', 70, 60, 'Kanto'),
(50, 'Digda', 'Boden', NULL, 55, 10, 'Kanto'),
(51, 'Digdri', 'Boden', NULL, 80, 35, 'Kanto'),
(52, 'Mauzi', 'Normal', NULL, 45, 40, 'Kanto'),
(53, 'Snobilikat', 'Normal', NULL, 70, 65, 'Kanto'),
(54, 'Enton', 'Wasser', NULL, 50, 52, 'Kanto'),
(55, 'Entoron', 'Wasser', NULL, 80, 82, 'Kanto'),
(56, 'Menki', 'Kampf', NULL, 80, 40, 'Kanto'),
(57, 'Rasaff', 'Kampf', NULL, 105, 65, 'Kanto'),
(58, 'Fukano', 'Feuer', NULL, 70, 55, 'Kanto'),
(59, 'Arkani', 'Feuer', NULL, 110, 90, 'Kanto'),
(60, 'Quapsel', 'Wasser', NULL, 50, 65, 'Kanto'),
(61, 'Quaputzi', 'Wasser', NULL, 65, 80, 'Kanto'),
(62, 'Quappo', 'Wasser', 'Kampf', 65, 80, 'Kanto'),
(63, 'Abra', 'Psycho', NULL, 20, 25, 'Kanto'),
(64, 'Kadabra', 'Psycho', NULL, 35, 40, 'Kanto'),
(65, 'Simsala', 'Psycho', NULL, 50, 55, 'Kanto'),
(66, 'Machollo', 'Kampf', NULL, 80, 70, 'Kanto'),
(67, 'Maschock', 'Kampf', NULL, 100, 80, 'Kanto'),
(68, 'Machomei', 'Kampf', NULL, 130, 90, 'Kanto'),
(69, 'Knofensa', 'Pflanze', 'Gift', 75, 50, 'Kanto'),
(70, 'Ultrigaria', 'Pflanze', 'Gift', 90, 75, 'Kanto'),
(71, 'Sarzenia', 'Pflanze', 'Gift', 105, 80, 'Kanto'),
(72, 'Tentacha', 'Wasser', 'Gift', 40, 35, 'Kanto'),
(73, 'Tentoxa', 'Wasser', 'Gift', 70, 65, 'Kanto'),
(74, 'Kleinstein', 'Gestein', 'Boden', 80, 40, 'Kanto'),
(75, 'Georok', 'Gestein', 'Boden', 95, 55, 'Kanto'),
(76, 'Geowaz', 'Gestein', 'Boden', 120, 80, 'Kanto'),
(77, 'Ponita', 'Feuer', NULL, 85, 55, 'Kanto'),
(78, 'Gallopa', 'Feuer', NULL, 100, 70, 'Kanto'),
(79, 'Flegmon', 'Wasser', 'Psycho', 65, 44, 'Kanto'),
(80, 'Lahmus', 'Wasser', 'Psycho', 85, 59, 'Kanto'),
(81, 'Magnetilo', 'Elektro', 'Stahl', 35, 25, 'Kanto'),
(82, 'Magneton', 'Elektro', 'Stahl', 60, 50, 'Kanto'),
(83, 'Porenta', 'Normal', 'Flug', 55, 40, 'Kanto'),
(84, 'Dodu', 'Normal', 'Flug', 85, 45, 'Kanto'),
(85, 'Dodri', 'Normal', 'Flug', 110, 60, 'Kanto'),
(86, 'Jurob', 'Wasser', NULL, 65, 40, 'Kanto'),
(87, 'Jugong', 'Wasser', 'Eis', 80, 90, 'Kanto'),
(88, 'Sleima', 'Gift', NULL, 40, 40, 'Kanto'),
(89, 'Sleimok', 'Gift', NULL, 65, 65, 'Kanto'),
(90, 'Muschas', 'Wasser', NULL, 70, 80, 'Kanto'),
(91, 'Austos', 'Wasser', 'Eis', 67, 80, 'Kanto'),
(92, 'Nebulak', 'Geist', 'Gift', 45, 30, 'Kanto'),
(93, 'Alpollo', 'Geist', 'Gift', 60, 45, 'Kanto'),
(94, 'Gengar', 'Geist', 'Gift', 65, 60, 'Kanto'),
(95, 'Onix', 'Gestein', 'Boden', 45, 35, 'Kanto'),
(96, 'Traumato', 'Psycho', NULL, 48, 55, 'Kanto'),
(97, 'Hypno', 'Psycho', NULL, 73, 85, 'Kanto'),
(98, 'Krabby', 'Wasser', NULL, 105, 30, 'Kanto'),
(99, 'Kingler', 'Wasser', NULL, 130, 55, 'Kanto'),
(100, 'Voltobal', 'Elektro', NULL, 30, 40, 'Kanto'),
(101, 'Lektrobal', 'Elektro', NULL, 50, 60, 'Kanto'),
(102, 'Owei', 'Pflanze', 'Psycho', 40, 50, 'Kanto'),
(103, 'Kokowei', 'Pflanze', 'Psycho', 65, 80, 'Kanto'),
(104, 'Tragosso', 'Boden', NULL, 80, 90, 'Kanto'),
(105, 'Knogga', 'Boden', NULL, 105, 105, 'Kanto'),
(106, 'Kicklee', 'Kampf', NULL, 125, 50, 'Kanto'),
(107, 'Nockchan', 'Kampf', NULL, 105, 50, 'Kanto'),
(108, 'Schlurp', 'Normal', NULL, 65, 55, 'Kanto'),
(109, 'Smogon', 'Gift', NULL, 30, 40, 'Kanto'),
(110, 'Smogmog', 'Gift', NULL, 50, 65, 'Kanto'),
(111, 'Rihorn', 'Gestein', 'Boden', 85, 80, 'Kanto'),
(112, 'Rizeros', 'Gestein', 'Boden', 130, 105, 'Kanto'),
(113, 'Chaneira', 'Normal', NULL, 5, 250, 'Kanto'),
(114, 'Tangela', 'Pflanze', NULL, 55, 65, 'Kanto'),
(115, 'Kangama', 'Normal', NULL, 95, 105, 'Kanto'),
(116, 'Seeper', 'Wasser', NULL, 65, 55, 'Kanto'),
(117, 'Seemon', 'Wasser', NULL, 95, 80, 'Kanto'),
(118, 'Goldini', 'Wasser', NULL, 67, 40, 'Kanto'),
(119, 'Golking', 'Wasser', NULL, 130, 55, 'Kanto'),
(120, 'Sterndu', 'Wasser', NULL, 45, 30, 'Kanto'),
(121, 'Starmie', 'Wasser', 'Psycho', 75, 60, 'Kanto'),
(122, 'Pantimos', 'Psycho', 'Fee', 45, 50, 'Kanto'),
(123, 'Sichlor', 'Käfer', 'Flug', 110, 70, 'Kanto'),
(124, 'Rossana', 'Eis', 'Psycho', 65, 60, 'Kanto'),
(125, 'Elektek', 'Elektro', NULL, 123, 65, 'Kanto'),
(126, 'Magmar', 'Feuer', NULL, 95, 65, 'Kanto'),
(127, 'Pinsir', 'Käfer', NULL, 125, 65, 'Kanto'),
(128, 'Tauros', 'Normal', NULL, 100, 75, 'Kanto'),
(129, 'Karpador', 'Wasser', NULL, 10, 20, 'Kanto'),
(130, 'Garados', 'Wasser', 'Flug', 125, 95, 'Kanto'),
(131, 'Lapras', 'Wasser', 'Eis', 85, 130, 'Kanto'),
(132, 'Ditto', 'Normal', NULL, 48, 48, 'Kanto'),
(133, 'Evoli', 'Normal', NULL, 55, 55, 'Kanto'),
(134, 'Aquana', 'Wasser', NULL, 65, 65, 'Kanto'),
(135, 'Blitza', 'Elektro', NULL, 65, 65, 'Kanto'),
(136, 'Flamara', 'Feuer', NULL, 130, 65, 'Kanto'),
(137, 'Porygon', 'Normal', NULL, 60, 65, 'Kanto'),
(138, 'Ammonitas', 'Gestein', 'Wasser', 60, 35, 'Kanto'),
(139, 'Amoroso', 'Gestein', 'Wasser', 95, 70, 'Kanto'),
(140, 'Kabuto', 'Gestein', 'Wasser', 80, 30, 'Kanto'),
(141, 'Kabutops', 'Gestein', 'Wasser', 115, 60, 'Kanto'),
(142, 'Aerodactyl', 'Gestein', 'Flug', 105, 80, 'Kanto'),
(143, 'Relaxo', 'Normal', NULL, 110, 65, 'Kanto'),
(144, 'Arktos', 'Eis', 'Flug', 85, 90, 'Kanto'),
(145, 'Zapdos', 'Elektro', 'Flug', 90, 90, 'Kanto'),
(146, 'Lavados', 'Feuer', 'Flug', 100, 90, 'Kanto'),
(147, 'Dratini', 'Drache', NULL, 64, 41, 'Kanto'),
(148, 'Dragonir', 'Drache', NULL, 84, 61, 'Kanto'),
(149, 'Dragoran', 'Drache', 'Flug', 134, 91, 'Kanto'),
(150, 'Mewtu', 'Psycho', NULL, 110, 106, 'Kanto'),
(151, 'Mew', 'Psycho', NULL, 100, 100, 'Kanto');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_paldea`
--

CREATE TABLE `tbl_paldea` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `geschwindigkeit` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_paldea`
--

INSERT INTO `tbl_paldea` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `geschwindigkeit`, `generation_name`) VALUES
(906, 'Felori', 'Pflanze', NULL, 40, 30, NULL, 'Paldea'),
(907, 'Feliospa', 'Pflanze', NULL, 50, 40, NULL, 'Paldea'),
(908, 'Maskagato', 'Pflanze', 'Unlicht', 70, 50, NULL, 'Paldea'),
(909, 'Krokel', 'Feuer', NULL, 30, 40, NULL, 'Paldea'),
(910, 'Lokroko', 'Feuer', NULL, 50, 40, NULL, 'Paldea'),
(911, 'Skelokrok', 'Feuer', 'Geist', 70, 50, NULL, 'Paldea'),
(912, 'Kwaks', 'Wasser', NULL, 40, 40, NULL, 'Paldea'),
(913, 'Fuentente', 'Wasser', NULL, 50, 50, NULL, 'Paldea'),
(914, 'Bailonda', 'Wasser', 'Kampf', 80, 50, NULL, 'Paldea'),
(915, 'Ferkuli', 'Normal', NULL, 30, 40, NULL, 'Paldea'),
(916, 'Fragrunz', 'Normal', NULL, 60, 70, NULL, 'Paldea'),
(917, 'Tarundel', 'Käfer', NULL, 30, 30, NULL, 'Paldea'),
(918, 'Spinsidias', 'Käfer', NULL, 50, 40, NULL, 'Paldea'),
(919, 'Micrick', 'Käfer', NULL, 30, 20, NULL, 'Paldea'),
(920, 'Lextremo', 'Käfer', 'Ulicht', 60, 50, NULL, 'Paldea'),
(921, 'Pamo', 'Elektro', NULL, 30, 30, NULL, 'Paldea'),
(922, 'Pamamo', 'Elektro', 'Kampf', 50, 40, NULL, 'Paldea'),
(923, 'Pamomamo', 'Elektro', 'Kampf', 70, 50, NULL, 'Paldea'),
(924, 'Zwieps', 'Normal', NULL, 30, 30, NULL, 'Paldea'),
(925, 'Famieps', 'Normal', NULL, 50, 50, NULL, 'Paldea'),
(926, 'Hefel', 'Fee', NULL, 40, 30, NULL, 'Paldea'),
(927, 'Backel', 'Fee', NULL, 50, 40, NULL, 'Paldea'),
(928, 'Olini', 'Pflanze', 'Normal', 30, 30, NULL, 'Paldea'),
(929, 'Olivinio', 'Pflanze', 'Normal', 40, 40, NULL, 'Paldea'),
(930, 'Olithena', 'Pflanze', 'Normal', 50, 50, NULL, 'Paldea'),
(931, 'Krawalloro', 'Normal', 'Flug', 60, 50, NULL, 'Paldea'),
(932, 'Geosali', 'Gestein', NULL, 35, 30, NULL, 'Paldea'),
(933, 'Sedisal', 'Gestein', NULL, 45, 40, NULL, 'Paldea'),
(934, 'Saltigant', 'Gestein', NULL, 60, 55, NULL, 'Paldea'),
(935, 'Knarbon', 'Feuer', NULL, 30, 25, NULL, 'Paldea'),
(936, 'Crimanzo', 'Feuer', 'Psycho', 40, 50, NULL, 'Paldea'),
(937, 'Azugladis', 'Feuer', 'Geist', 80, 50, NULL, 'Paldea'),
(938, 'Blipp', 'Elektro', NULL, 20, 40, NULL, 'Paldea'),
(939, 'Wampitz', 'Elektro', NULL, 40, 70, NULL, 'Paldea'),
(940, 'Voltrel', 'Elektro', 'Flug', 30, 30, NULL, 'Paldea'),
(941, 'Voltrean', 'Elektro', 'Flug', 50, 50, NULL, 'Paldea'),
(942, 'Mobtiff', 'Unlicht', NULL, 50, 40, NULL, 'Paldea'),
(943, 'Mastifioso', 'Unlicht', NULL, 80, 50, NULL, 'Paldea'),
(944, 'Sproxi', 'Gift', 'Normal', 40, 30, NULL, 'Paldea'),
(945, 'Affiti', 'Gift', 'Normal', 60, 40, NULL, 'Paldea'),
(946, 'Weherba', 'Pflanze', 'Geist', 40, 30, NULL, 'Paldea'),
(947, 'Horrerba', 'Pflanze', 'Geist', 70, 40, NULL, 'Paldea'),
(948, 'Tentagra', 'Boden', 'Pflanze', 30, 25, NULL, 'Paldea'),
(949, 'Tenterra', 'Boden', 'Pflanze', 50, 45, NULL, 'Paldea'),
(950, 'Klibbe', 'Gestein', NULL, 60, 50, NULL, 'Paldea'),
(951, 'Chilingel', 'Pflanze', NULL, 40, 30, NULL, 'Paldea'),
(952, 'Halupenjo', 'Pflanze', 'Feuer', 70, 40, NULL, 'Paldea'),
(953, 'Relluk', 'Käfer', NULL, 30, 25, NULL, 'Paldea'),
(954, 'Skarabaks', 'Käfer', 'Psycho', 30, 50, NULL, 'Paldea'),
(955, 'Flattutu', 'Psycho', NULL, 30, 20, NULL, 'Paldea'),
(956, 'Psiopatra', 'Psycho', NULL, 40, 60, NULL, 'Paldea'),
(957, 'Forgita', 'Fee', 'Stahl', 30, 25, NULL, 'Paldea'),
(958, 'Tafforgita', 'Fee', 'Stahl', 40, 35, NULL, 'Paldea'),
(959, 'Granforgita', 'Fee', 'Stahl', 50, 45, NULL, 'Paldea'),
(960, 'Schligda', 'Wasser', NULL, 40, 10, NULL, 'Paldea'),
(961, 'Schligdri', 'Wasser', NULL, 60, 30, NULL, 'Paldea'),
(962, 'Adebom', 'Flug', 'Ulicht', 70, 50, NULL, 'Paldea'),
(963, 'Normifin', 'Wasser', NULL, 30, 50, NULL, 'Paldea'),
(964, 'Delfinator', 'Wasser', NULL, 50, 60, NULL, 'Paldea'),
(965, 'Knattox', 'Stahl', 'Gift', 50, 30, NULL, 'Paldea'),
(966, 'Knattatox', 'Stahl', 'Gift', 70, 50, NULL, 'Paldea'),
(967, 'Mopex', 'Drache', 'Normal', 60, 50, NULL, 'Paldea'),
(968, 'Schlurm', 'Stahl', NULL, 50, 50, NULL, 'Paldea'),
(969, 'Lumispross', 'Gestein', 'Gift', 30, 30, NULL, 'Paldea'),
(970, 'Lumiflora', 'Gestein', 'Gift', 40, 50, NULL, 'Paldea'),
(971, 'Gruff', 'Geist', NULL, 40, 35, NULL, 'Paldea'),
(972, 'Friedwuff', 'Geist', NULL, 60, 50, NULL, 'Paldea'),
(973, 'Flaminkno', 'Flug', 'Kampf', 70, 50, NULL, 'Paldea'),
(974, 'Flaniwal', 'Eis', NULL, 40, 70, NULL, 'Paldea'),
(975, 'Kolowal', 'Eis', NULL, 70, 100, NULL, 'Paldea'),
(976, 'Agiluza', 'Wasser', 'Psycho', 60, 60, NULL, 'Paldea'),
(977, 'Heerashai', 'Wasser', NULL, 60, 90, NULL, 'Paldea'),
(978, 'Nigiragi', 'Drache', 'Wasser', 30, 40, NULL, 'Paldea'),
(979, 'Epitaff', 'Kampf', 'Geist', 70, 65, NULL, 'Paldea'),
(980, 'Suelord', 'Gift', 'Boden', 50, 85, NULL, 'Paldea'),
(981, 'Farigiraf', 'Normal', 'Psycho', 60, 80, NULL, 'Paldea'),
(982, 'Dummimisel', 'Normal', NULL, 65, 80, NULL, 'Paldea'),
(983, 'Gladimperio', 'Unlicht', 'Stahl', 85, 60, NULL, 'Paldea'),
(984, 'Riesenzahn', 'Boden', 'Kampf', 80, 70, NULL, 'Paldea'),
(985, 'Brüllschweif', 'Fee', 'Psycho', 40, 70, NULL, 'Paldea'),
(986, 'Wutpilz', 'Pflanze', 'Unlicht', 80, 75, NULL, 'Paldea'),
(987, 'Flatterhaar', 'Geist', 'Fee', 40, 35, NULL, 'Paldea'),
(988, 'Kriechflügel', 'Käfer', 'Kampf', 80, 50, NULL, 'Paldea'),
(989, 'Sandfell', 'Elektro', 'Boden', 50, 45, NULL, 'Paldea'),
(990, 'Eisenrad', 'Boden', 'Stahl', 70, 60, NULL, 'Paldea'),
(991, 'Eisenbüdel', 'Eis', 'Wasser', 50, 40, NULL, 'Paldea'),
(992, 'Eisenhand', 'Kampf', 'Elektro', 90, 110, NULL, 'Paldea'),
(993, 'Eisenhals', 'Unlicht', 'Flug', 50, 60, NULL, 'Paldea'),
(994, 'Eisenfalter', 'Feuer', 'Gift', 50, 45, NULL, 'Paldea'),
(995, 'Eisendorn', 'Gestein', 'Elektro', 80, 60, NULL, 'Paldea'),
(996, 'Frospino', 'Drache', 'Eis', 50, 40, NULL, 'Paldea'),
(997, 'Cryospino', 'Drache', 'Eis', 60, 55, NULL, 'Paldea'),
(998, 'Espinodon', 'Drache', 'Eis', 90, 70, NULL, 'Paldea'),
(999, 'Gierspenst', 'Geist', NULL, 20, 30, NULL, 'Paldea'),
(1000, 'Monetigo', 'Stahl', 'Geist', 40, 60, NULL, 'Paldea'),
(1001, 'Chongjian', 'Unlicht', 'Pflanze', 50, 45, NULL, 'Paldea'),
(1002, 'Baojian', 'Unlicht', 'Eis', 80, 50, NULL, 'Paldea'),
(1003, 'Dinglu', 'Unlicht', 'Boden', 70, 100, NULL, 'Paldea'),
(1004, 'Yuyu', 'Unlicht', 'Feuer', 50, 40, NULL, 'Paldea'),
(1005, 'Donnersichel', 'Drache', 'Unlicht', 90, 70, NULL, 'Paldea'),
(1006, 'Eisenkrieger', 'Fee', 'Kampf', 80, 50, NULL, 'Paldea'),
(1007, 'Koraidon', 'Kampf', 'Drache', 80, 60, NULL, 'Paldea'),
(1008, 'Miraidon', 'Elektro', 'Drache', 50, 60, NULL, 'Paldea'),
(1009, 'Windewoge', 'Wasser', 'Drache', 45, 60, NULL, 'Paldea'),
(1010, 'Eisenblatt', 'Pflanze', 'Psycho', 80, 60, NULL, 'Paldea'),
(1011, 'Sirapfel', 'Pflanze', 'Drache', 50, 45, NULL, 'Paldea'),
(1012, 'Mortcha', 'Pflanze', 'Geist', 30, 25, NULL, 'Paldea'),
(1013, 'Fatalitcha', 'Pflanze', 'Geist', 40, 50, NULL, 'Paldea'),
(1014, 'Boninu', 'Gift', 'Kampf', 80, 65, NULL, 'Paldea'),
(1015, 'Benesaru', 'Gift', 'Psycho', 50, 60, NULL, 'Paldea'),
(1016, 'Beatori', 'Gift', 'Fee', 60, 55, NULL, 'Paldea'),
(1017, 'Ogerpon', 'Pflanze', NULL, 80, 50, NULL, 'Paldea'),
(1018, 'Briduradon', 'Stahl', 'Drache', 70, 60, NULL, 'Paldea'),
(1019, 'Hydrapfel', 'Pflanze', 'Drache', 55, 70, NULL, 'Paldea'),
(1020, 'Keilflamme', 'Feuer', 'Drache', 70, 65, NULL, 'Paldea'),
(1021, 'Furienblitz', 'Elektro', 'Drache', 50, 80, NULL, 'Paldea'),
(1022, 'Eisenfels', 'Gestein', 'Psycho', 80, 60, NULL, 'Paldea'),
(1023, 'Eisenhaupt', 'Stahl', 'Psycho', 55, 65, NULL, 'Paldea'),
(1024, 'Terapagos', 'Normal', NULL, 40, 60, NULL, 'Paldea'),
(1025, 'Infamomo', 'Gift', 'Geist', 65, 60, NULL, 'Paldea');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_sinnoh`
--

CREATE TABLE `tbl_sinnoh` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_sinnoh`
--

INSERT INTO `tbl_sinnoh` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(387, 'Chelast', 'Pflanze', NULL, 49, 45, 'Sinnoh'),
(388, 'Chelcarain', 'Pflanze', NULL, 62, 60, 'Sinnoh'),
(389, 'Chelterrar', 'Pflanze', 'Boden', 100, 80, 'Sinnoh'),
(390, 'Panflam', 'Feuer', NULL, 52, 44, 'Sinnoh'),
(391, 'Panpyro', 'Feuer', 'Kampf', 64, 58, 'Sinnoh'),
(392, 'Panferno', 'Feuer', 'Kampf', 104, 76, 'Sinnoh'),
(393, 'Plinfa', 'Wasser', NULL, 51, 53, 'Sinnoh'),
(394, 'Pliprin', 'Wasser', NULL, 66, 64, 'Sinnoh'),
(395, 'Impoleon', 'Wasser', 'Stahl', 86, 84, 'Sinnoh'),
(396, 'Staralili', 'Normal', 'Flug', 55, 40, 'Sinnoh'),
(397, 'Staravia', 'Normal', 'Flug', 75, 55, 'Sinnoh'),
(398, 'Staraptor', 'Normal', 'Flug', 120, 85, 'Sinnoh'),
(399, 'Bidiza', 'Normal', NULL, 55, 45, 'Sinnoh'),
(400, 'Bidifas', 'Normal', 'Wasser', 85, 60, 'Sinnoh'),
(401, 'Zirpurze', 'Käfer', NULL, 30, 25, 'Sinnoh'),
(402, 'Zirpeise', 'Käfer', NULL, 50, 35, 'Sinnoh'),
(403, 'Sheinux', 'Elektro', NULL, 49, 45, 'Sinnoh'),
(404, 'Luxio', 'Elektro', NULL, 85, 60, 'Sinnoh'),
(405, 'Luxtra', 'Elektro', 'Flug', 120, 80, 'Sinnoh'),
(406, 'Knospi', 'Pflanze', NULL, 50, 40, 'Sinnoh'),
(407, 'Roserade', 'Pflanze', 'Gift', 70, 60, 'Sinnoh'),
(408, 'Koknodon', 'Gestein', 'Stahl', 52, 67, 'Sinnoh'),
(409, 'Rameidon', 'Gestein', 'Stahl', 82, 107, 'Sinnoh'),
(410, 'Schilterus', 'Gestein', 'Stahl', 82, 77, 'Sinnoh'),
(411, 'Bollterus', 'Gestein', 'Stahl', 125, 100, 'Sinnoh'),
(412, 'Burmy', 'Käfer', NULL, 29, 40, 'Sinnoh'),
(413, 'Burmadame', 'Käfer', 'Pflanze', 59, 60, 'Sinnoh'),
(414, 'Moterpel', 'Käfer', 'Flug', 94, 70, 'Sinnoh'),
(415, 'Wadribie', 'Käfer', 'Flug', 70, 60, 'Sinnoh'),
(416, 'Honweisel', 'Käfer', 'Flug', 90, 70, 'Sinnoh'),
(417, 'Pachirisu', 'Elektro', NULL, 45, 60, 'Sinnoh'),
(418, 'Bamelin', 'Wasser', NULL, 49, 64, 'Sinnoh'),
(419, 'Bojelin', 'Wasser', NULL, 69, 84, 'Sinnoh'),
(420, 'Kikugi', 'Pflanze', NULL, 43, 36, 'Sinnoh'),
(421, 'Kinoso', 'Pflanze', NULL, 76, 55, 'Sinnoh'),
(422, 'Schalellos', 'Wasser', NULL, 48, 76, 'Sinnoh'),
(423, 'Gastrodon', 'Wasser', 'Boden', 83, 111, 'Sinnoh'),
(424, 'Ambidiffel', 'Normal', NULL, 50, 60, 'Sinnoh'),
(425, 'Driftlon', 'Geist', 'Flug', 40, 90, 'Sinnoh'),
(426, 'Drifzepeli', 'Geist', 'Flug', 70, 150, 'Sinnoh'),
(427, 'Haspiror', 'Normal', NULL, 70, 110, 'Sinnoh'),
(428, 'Schlapor', 'Normal', NULL, 60, 65, 'Sinnoh'),
(429, 'Traunmagil', 'Geist', NULL, 50, 50, 'Sinnoh'),
(430, 'Kramshef', 'Unlicht', 'Flug', 65, 60, 'Sinnoh'),
(431, 'Charmian', 'Normal', NULL, 45, 70, 'Sinnoh'),
(432, 'Schnurrgast', 'Normal', NULL, 59, 60, 'Sinnoh'),
(433, 'Klingplim', 'Psycho', NULL, 90, 70, 'Sinnoh'),
(434, 'Skunkapuh', 'Gift', 'Unlicht', 64, 103, 'Sinnoh'),
(435, 'Skuntank', 'Gift', 'Unlicht', 93, 103, 'Sinnoh'),
(436, 'Bronzel', 'Stahl', 'Psycho', 24, 57, 'Sinnoh'),
(437, 'Bronzong', 'Stahl', 'Psycho', 89, 67, 'Sinnoh'),
(438, 'Mobai', 'Gestein', NULL, 35, 60, 'Sinnoh'),
(439, 'Pantimimi', 'Psycho', 'Fee', 65, 75, 'Sinnoh'),
(440, 'Wonneira', 'Normal', NULL, 100, 255, 'Sinnoh'),
(441, 'Plaudagei', 'Normal', 'Flug', 65, 60, 'Sinnoh'),
(442, 'Kryppuk', 'Geist', 'Unlicht', 50, 45, 'Sinnoh'),
(443, 'Kaumalat', 'Drache', 'Boden', 40, 68, 'Sinnoh'),
(444, 'Knarksel', 'Drache', 'Boden', 70, 108, 'Sinnoh'),
(445, 'Knakrack', 'Drache', 'Boden', 115, 108, 'Sinnoh'),
(446, 'Mampfaxo', 'Normal', NULL, 40, 110, 'Sinnoh'),
(447, 'Riolu', 'Kampf', NULL, 70, 40, 'Sinnoh'),
(448, 'Lucario', 'Kampf', 'Stahl', 110, 70, 'Sinnoh'),
(449, 'Hippopotas', 'Boden', NULL, 72, 68, 'Sinnoh'),
(450, 'Hippoterus', 'Boden', NULL, 112, 108, 'Sinnoh'),
(451, 'Pionskora', 'Gift', 'Käfer', 65, 60, 'Sinnoh'),
(452, 'Piondragi', 'Gift', 'Unlicht', 125, 110, 'Sinnoh'),
(453, 'Glibunkel', 'Gift', 'Kampft', 64, 64, 'Sinnoh'),
(454, 'Toxiquak', 'Gift', 'Kampf', 106, 65, 'Sinnoh'),
(455, 'Venuflibis', 'Pflanze', NULL, 50, 76, 'Sinnoh'),
(456, 'Finneon', 'Wasser', NULL, 49, 49, 'Sinnoh'),
(457, 'Lumineon', 'Wasser', NULL, 69, 69, 'Sinnoh'),
(458, 'Mantirps', 'Wasser', 'Flug', 50, 58, 'Sinnoh'),
(459, 'Shnebedeck', 'Pflanze', 'Eis', 62, 70, 'Sinnoh'),
(460, 'Rexblisar', 'Pflanze', 'Eis', 130, 110, 'Sinnoh'),
(461, 'Snibunna', 'Unlicht', 'Eis', 120, 100, 'Sinnoh'),
(462, 'Magnezone', 'Elektro', 'Stahl', 70, 70, 'Sinnoh'),
(463, 'Schlurplek', 'Normal', NULL, 92, 83, 'Sinnoh'),
(464, 'Rihornior', 'Boden', 'Gestein', 140, 115, 'Sinnoh'),
(465, 'Tangoloss', 'Pflanze', NULL, 100, 105, 'Sinnoh'),
(466, 'Elevoltek', 'Elektro', NULL, 75, 75, 'Sinnoh'),
(467, 'Magbrant', 'Feuer', NULL, 120, 75, 'Sinnoh'),
(468, 'Togekiss', 'Fee', 'Flug', 120, 85, 'Sinnoh'),
(469, 'Yanmega', 'Käfer', 'Flug', 76, 86, 'Sinnoh'),
(470, 'Folipurba', 'Pflanze', NULL, 55, 75, 'Sinnoh'),
(471, 'Glaziola', 'Eis', NULL, 60, 65, 'Sinnoh'),
(472, 'Skorgro', 'Boden', 'Flug', 110, 110, 'Sinnoh'),
(473, 'Mamutel', 'Eis', 'Boden', 130, 110, 'Sinnoh'),
(474, 'Porygon-Z', 'Normal', NULL, 135, 85, 'Sinnoh'),
(475, 'Galagladi', 'Psycho', 'Kampf', 68, 68, 'Sinnoh'),
(476, 'Voluminas', 'Gestein', 'Stahl', 55, 70, 'Sinnoh'),
(477, 'Zwirrfinst', 'Geist', NULL, 50, 45, 'Sinnoh'),
(478, 'Frosdedje', 'Eis', 'Geist', 80, 70, 'Sinnoh'),
(479, 'Rotom', 'Elektro', 'Geist', 50, 50, 'Sinnoh'),
(480, 'Selfe', 'Psycho', NULL, 75, 70, 'Sinnoh'),
(481, 'Vesprit', 'Psycho', NULL, 105, 70, 'Sinnoh'),
(482, 'Tobutz', 'Psycho', NULL, 125, 70, 'Sinnoh'),
(483, 'Dialga', 'Stahl', 'Drache', 120, 100, 'Sinnoh'),
(484, 'Palkia', 'Wasser', 'Drache', 120, 90, 'Sinnoh'),
(485, 'Heatran', 'Feuer', 'Stahl', 90, 91, 'Sinnoh'),
(486, 'Regigigas', 'Normal', NULL, 160, 110, 'Sinnoh'),
(487, 'Giratina', 'Geist', 'Drache', 120, 150, 'Sinnoh'),
(488, 'Cresselia', 'Psycho', NULL, 70, 120, 'Sinnoh'),
(489, 'Phione', 'Wasser', NULL, 80, 80, 'Sinnoh'),
(490, 'Manaphy', 'Wasser', NULL, 100, 100, 'Sinnoh'),
(491, 'Darkrai', 'Unlicht', NULL, 140, 70, 'Sinnoh'),
(492, 'Shaymin', 'Pflanze', NULL, 100, 100, 'Sinnoh'),
(493, 'Arceus', 'Normal', NULL, 120, 120, 'Sinnoh'),
(494, 'Victini', 'Psycho', 'Feuer', 100, 100, 'Sinnoh');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_spectra`
--

CREATE TABLE `tbl_spectra` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typ1` varchar(255) DEFAULT NULL,
  `typ2` varchar(255) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `lebenspunkte` int(11) DEFAULT NULL,
  `generation_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Daten für Tabelle `tbl_spectra`
--

INSERT INTO `tbl_spectra` (`pokedex`, `name`, `typ1`, `typ2`, `attack`, `lebenspunkte`, `generation_name`) VALUES
(1026, 'Konnimoni', 'Psycho', 'Kampf', 80, 60, 'Spectra'),
(1027, 'Blitzara', 'Gift', 'Eis', 55, 37, 'Spectra');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_alola`
--
ALTER TABLE `tbl_alola`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_einall`
--
ALTER TABLE `tbl_einall`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_galar`
--
ALTER TABLE `tbl_galar`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_hoenn`
--
ALTER TABLE `tbl_hoenn`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_johto`
--
ALTER TABLE `tbl_johto`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_kalos`
--
ALTER TABLE `tbl_kalos`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_kanto`
--
ALTER TABLE `tbl_kanto`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_paldea`
--
ALTER TABLE `tbl_paldea`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_sinnoh`
--
ALTER TABLE `tbl_sinnoh`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indizes für die Tabelle `tbl_spectra`
--
ALTER TABLE `tbl_spectra`
  ADD PRIMARY KEY (`pokedex`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_alola`
--
ALTER TABLE `tbl_alola`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=810;

--
-- AUTO_INCREMENT für Tabelle `tbl_einall`
--
ALTER TABLE `tbl_einall`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=650;

--
-- AUTO_INCREMENT für Tabelle `tbl_galar`
--
ALTER TABLE `tbl_galar`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=906;

--
-- AUTO_INCREMENT für Tabelle `tbl_hoenn`
--
ALTER TABLE `tbl_hoenn`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT für Tabelle `tbl_johto`
--
ALTER TABLE `tbl_johto`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT für Tabelle `tbl_kalos`
--
ALTER TABLE `tbl_kalos`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=722;

--
-- AUTO_INCREMENT für Tabelle `tbl_kanto`
--
ALTER TABLE `tbl_kanto`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT für Tabelle `tbl_paldea`
--
ALTER TABLE `tbl_paldea`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1026;

--
-- AUTO_INCREMENT für Tabelle `tbl_sinnoh`
--
ALTER TABLE `tbl_sinnoh`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=495;

--
-- AUTO_INCREMENT für Tabelle `tbl_spectra`
--
ALTER TABLE `tbl_spectra`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1028;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
